-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2016 at 06:41 AM
-- Server version: 5.6.28
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pdjaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_cost`
--

CREATE TABLE `additional_cost` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(50) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `additional_cost`
--

INSERT INTO `additional_cost` (`id`, `ref_id`, `desc`, `price`, `created_at`, `updated_at`) VALUES
(1, '20160802001', 'additional1', 50000, '2016-08-19 04:18:44', '2016-08-02 01:33:58'),
(2, '20160802002', 'Ongkos Supir', 300000, '2016-08-19 04:19:02', '2016-08-02 02:13:33'),
(3, 'SOCA20160819001', 'gOJEK', 15000, '2016-08-19 03:01:36', '2016-08-19 03:01:36'),
(4, 'SOCA20160909001', 'add', 4000, '2016-09-09 03:21:38', '2016-09-09 03:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'test', 'phone', 'test', '2016-09-13 04:07:54', '2016-09-12 21:07:54'),
(2, 'test2', 'test', 'address', '2016-09-12 20:36:35', '2016-09-12 20:36:35');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order`
--

CREATE TABLE `delivery_order` (
  `id` int(11) UNSIGNED NOT NULL,
  `delivery_id` varchar(50) DEFAULT NULL,
  `origin` varchar(50) DEFAULT NULL,
  `destination` varchar(50) DEFAULT NULL,
  `meta_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_order`
--

INSERT INTO `delivery_order` (`id`, `delivery_id`, `origin`, `destination`, `meta_id`, `quantity`, `created_at`, `updated_at`) VALUES
(49, '20160608022', '100 A', '105 A', 29, 1, '2016-06-08 03:24:56', '2016-06-08 03:24:56'),
(50, '20160608023', '112 B', '100 A', 60, 4, '2016-06-08 03:40:24', '2016-06-08 03:40:24'),
(51, '20160623001', '100 A', '105 A', 62, 5, '2016-06-23 01:28:56', '2016-06-23 01:28:56'),
(52, '20160623001', '100 A', '105 A', 63, 5, '2016-06-23 01:28:56', '2016-06-23 01:28:56'),
(53, '20160623001', '100 A', '105 A', 64, 5, '2016-06-23 01:28:56', '2016-06-23 01:28:56'),
(54, '20160628001', '100 A', '105 A', 71, 2, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(55, '20160628001', '100 A', '105 A', 72, 38, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(56, '20160628001', '100 A', '105 A', 73, 150, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(57, '20160628001', '100 A', '105 A', 74, 1, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(58, '20160628001', '100 A', '105 A', 75, 1, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(59, '20160628002', '100 A', '112 B', 83, 2, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(60, '20160628002', '100 A', '112 B', 84, 1, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(61, '20160628002', '100 A', '112 B', 85, 2, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(62, '20160628002', '100 A', '112 B', 86, 5, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(63, '20160628002', '100 A', '112 B', 87, 5, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(64, '20160628002', '100 A', '112 B', 88, 1, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(65, '20160628002', '100 A', '112 B', 89, 1, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(66, '20160628002', '100 A', '112 B', 90, 1, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(67, '20160628002', '100 A', '112 B', 91, 1, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(68, '20160628002', '100 A', '112 B', 93, 1, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(69, '20160628002', '100 A', '112 B', 62, 5, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(70, '20160628002', '100 A', '112 B', 63, 10, '2016-06-27 21:24:43', '2016-06-27 21:24:43');

-- --------------------------------------------------------

--
-- Table structure for table `detil_produk`
--

CREATE TABLE `detil_produk` (
  `id` int(11) NOT NULL,
  `meta_id` int(11) NOT NULL,
  `no_faktur` varchar(50) NOT NULL DEFAULT '',
  `kode_produk` varchar(50) NOT NULL DEFAULT '',
  `quantity` int(11) NOT NULL,
  `quantity_faktur` int(11) DEFAULT NULL,
  `quantity_sisa` int(11) NOT NULL,
  `harga_faktur` int(11) NOT NULL,
  `harga_asli` int(11) NOT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detil_produk`
--

INSERT INTO `detil_produk` (`id`, `meta_id`, `no_faktur`, `kode_produk`, `quantity`, `quantity_faktur`, `quantity_sisa`, `harga_faktur`, `harga_asli`, `gudang_id`, `created_at`, `updated_at`) VALUES
(29, 24, '2016000001', 'PLA16E0001', 50, 50, 60, 15000, 15000, 2, '2016-08-23 04:27:00', '2016-08-22 21:27:00'),
(58, 24, '2016000001', 'PLA16E0001', 50, 50, 0, 15000, 15000, 3, '2016-06-08 10:10:48', '2016-06-08 03:10:48'),
(59, 24, '2016000001', 'PLA16E0001', 50, 50, 10, 15000, 15000, 1, '2016-06-24 07:50:49', '2016-06-24 00:50:49'),
(60, 24, '2016000001', 'PLA16E0001', 50, 50, 5, 15000, 15000, 4, '2016-06-08 10:40:24', '2016-06-08 03:40:24'),
(61, 25, '2016000001', 'PLA16E0001', 50, 50, 40, 15000, 15000, 2, '2016-08-23 04:27:00', '2016-08-22 21:27:00'),
(62, 26, '2016000002', 'KPK16F0001', 10, 10, 0, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(63, 27, '2016000002', 'KPK16F0001', 20, 20, 5, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(65, 26, '2016000002', 'KPK16F0001', 10, 10, 5, 0, 0, 1, '2016-06-23 01:28:56', '2016-06-23 01:28:56'),
(66, 27, '2016000002', 'KPK16F0001', 20, 20, 5, 0, 0, 1, '2016-06-23 01:28:56', '2016-06-23 01:28:56'),
(68, 1, '0000', 'PJY16F0001', 10, 10, 1, 130273, 130273, 4, '2016-08-19 10:13:27', '2016-08-19 03:13:27'),
(69, 2, '0000', 'PJY16F0001', 20, 20, 10, 130273, 130273, 4, '2016-08-19 10:13:27', '2016-08-19 03:13:27'),
(70, 3, '0000', 'PJY16F0001', 20, 20, 20, 130273, 130273, 4, '2016-06-24 00:50:49', '2016-06-24 00:50:49'),
(71, 29, '2016000003', 'SMD16F0001', 32, 32, 30, 0, 0, 2, '2016-06-28 04:09:25', '2016-06-27 21:09:25'),
(72, 30, '2016000003', 'SMD16F0002', 38, 38, 0, 0, 0, 2, '2016-06-28 04:09:25', '2016-06-27 21:09:25'),
(73, 31, '2016000003', 'SMD16F0003', 159, 159, 9, 0, 0, 2, '2016-06-28 04:09:25', '2016-06-27 21:09:25'),
(74, 32, '2016000003', 'SMD16F0004', 2, 2, 1, 0, 0, 2, '2016-06-28 04:09:25', '2016-06-27 21:09:25'),
(75, 33, '2016000003', 'SMD16F0004', 1, 1, 0, 0, 0, 2, '2016-06-28 04:09:25', '2016-06-27 21:09:25'),
(76, 34, '2016000003', 'SMD16F0005', 30, 30, 30, 0, 0, 2, '2016-06-27 20:58:11', '2016-06-27 20:58:11'),
(77, 35, '2016000003', 'SMD16F0005', 9, 9, 9, 0, 0, 2, '2016-06-27 20:58:48', '2016-06-27 20:58:48'),
(78, 29, '2016000003', 'SMD16F0001', 32, 32, 0, 0, 0, 1, '2016-09-09 10:21:38', '2016-09-09 03:21:38'),
(79, 30, '2016000003', 'SMD16F0002', 38, 38, 38, 0, 0, 1, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(80, 31, '2016000003', 'SMD16F0003', 159, 159, 150, 0, 0, 1, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(81, 32, '2016000003', 'SMD16F0004', 2, 2, 1, 0, 0, 1, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(82, 33, '2016000003', 'SMD16F0004', 1, 1, 1, 0, 0, 1, '2016-06-27 21:09:25', '2016-06-27 21:09:25'),
(83, 36, '2016000004', 'IHS16F0001', 3, 3, 1, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(84, 37, '2016000004', 'IHS16F0001', 3, 3, 2, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(85, 38, '2016000004', 'IHS16F0001', 3, 3, 1, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(86, 39, '2016000004', 'IHS16F0001', 8, 8, 3, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(87, 40, '2016000004', 'IHS16F0001', 5, 5, 0, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(88, 41, '2016000004', 'IHS16F0001', 1, 1, 0, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(89, 42, '2016000004', 'IHS16F0001', 1, 1, 0, 0, 0, 2, '2016-06-28 04:24:43', '2016-06-27 21:24:43'),
(94, 36, '2016000004', 'IHS16F0001', 3, 3, 2, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(95, 37, '2016000004', 'IHS16F0001', 3, 3, 1, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(96, 38, '2016000004', 'IHS16F0001', 3, 3, 2, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(97, 39, '2016000004', 'IHS16F0001', 8, 8, 5, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(98, 40, '2016000004', 'IHS16F0001', 5, 5, 5, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(99, 41, '2016000004', 'IHS16F0001', 1, 1, 0, 0, 0, 4, '2016-08-19 10:01:36', '2016-08-19 03:01:36'),
(100, 42, '2016000004', 'IHS16F0001', 1, 1, 0, 0, 0, 4, '2016-08-19 10:01:36', '2016-08-19 03:01:36'),
(104, 26, '2016000002', 'KPK16F0001', 10, 10, 5, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(105, 27, '2016000002', 'KPK16F0001', 20, 20, 10, 0, 0, 4, '2016-06-27 21:24:43', '2016-06-27 21:24:43'),
(106, 43, '19900501', 'IHS16I0002', 1, 1, 1, 1000, 1000, 1, '2016-09-08 00:16:57', '2016-09-08 00:16:57'),
(107, 44, '2016000005', 'PLA16I0002', 40, 40, 40, 4000, 4000, 1, '2016-09-08 00:19:04', '2016-09-08 00:19:04'),
(108, 45, '2016000005', 'PLA16I0002', 30, 30, 30, 3000, 3000, 1, '2016-09-08 00:19:04', '2016-09-08 00:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `discount_rate` double NOT NULL,
  `keterangan` longtext NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `discount_rate`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
(4, 10, 'Test', 'Active', '2016-02-01 02:48:45', '2016-02-01 02:48:45'),
(5, 20, '20% Obral', 'Active', '2016-02-06 10:36:21', '2016-02-06 03:36:21'),
(6, 50, 'cuci gudang', 'Active', '2016-02-01 03:05:46', '2016-02-01 03:05:46'),
(7, 40, 'diskon akhir tahun', 'Active', '2016-02-02 02:34:39', '2016-02-02 02:34:39'),
(8, 60, 'test purpose only                    ', 'Active', '2016-04-09 22:04:27', '2016-04-09 22:04:27'),
(9, 0.8, '-         ', 'Active', '2016-05-31 20:21:13', '2016-05-31 20:21:13');

-- --------------------------------------------------------

--
-- Table structure for table `faktur`
--

CREATE TABLE `faktur` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(50) NOT NULL DEFAULT '',
  `tanggal_faktur` date NOT NULL,
  `tanggal_tiba` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `invoice_status` enum('Not Paid','Paid') NOT NULL DEFAULT 'Not Paid',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faktur`
--

INSERT INTO `faktur` (`id`, `no_faktur`, `tanggal_faktur`, `tanggal_tiba`, `supplier_id`, `invoice_status`, `created_at`, `updated_at`) VALUES
(17, '2016000001', '2016-05-25', '2016-05-27', 1, 'Not Paid', '2016-07-22 05:47:56', '2016-05-17 02:25:32'),
(19, '2016000002', '2016-06-18', '2016-06-18', 6, 'Not Paid', '2016-06-17 22:36:07', '2016-06-17 22:36:07'),
(26, '2016000003', '2016-06-28', '2016-06-28', 7, 'Paid', '2016-08-02 09:13:33', '2016-08-02 02:13:33'),
(27, '2016000004', '2016-06-28', '2016-06-28', 8, 'Not Paid', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(28, '19900501', '2015-01-01', '2015-01-01', 8, 'Paid', '2016-09-13 04:19:33', '2016-09-12 21:19:33'),
(29, '2016000005', '2015-01-01', '2015-01-01', 1, 'Not Paid', '2016-09-08 00:19:04', '2016-09-08 00:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id` int(11) NOT NULL,
  `no_gudang` varchar(50) NOT NULL DEFAULT '',
  `kamar_gudang` varchar(50) NOT NULL DEFAULT '',
  `pic_gudang` varchar(150) NOT NULL DEFAULT '',
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id`, `no_gudang`, `kamar_gudang`, `pic_gudang`, `status`, `created_at`, `updated_at`) VALUES
(1, '000', 'toko', 'toko', 'Active', '2016-08-23 10:20:30', '0000-00-00 00:00:00'),
(2, '100', 'A', 'Jordy', 'Active', '2016-04-10 04:44:00', '2016-04-09 21:44:00'),
(3, '111', 'A', 'Saleman', 'Active', '2016-02-15 07:07:09', '0000-00-00 00:00:00'),
(4, '112', 'B', 'QWRTY', 'Active', '2016-02-15 04:17:30', '2016-02-15 04:17:30');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id` int(11) NOT NULL,
  `kode_kategori_barang` varchar(25) NOT NULL,
  `kategori_barang` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_barang`
--

INSERT INTO `kategori_barang` (`id`, `kode_kategori_barang`, `kategori_barang`, `status`, `created_at`, `updated_at`) VALUES
(2, 'TMN', 'Ikan Gabus', 'Active', '2016-02-15 11:18:31', '2016-02-15 04:18:31'),
(3, 'TBS', 'Ikan Sepat', 'Active', '2016-02-15 04:18:42', '2016-02-15 04:18:42'),
(4, 'ATS', 'Udang', 'Active', '2016-03-04 21:44:48', '2016-03-04 21:44:48'),
(5, 'HR XXSI', 'Gabus', 'Active', '2016-06-27 20:51:21', '2016-06-27 20:51:21'),
(6, 'HR XXS', 'Gabus', 'Active', '2016-06-27 20:51:28', '2016-06-27 20:51:28'),
(7, 'SR XXS', 'Sepat Rawa', 'Active', '2016-06-27 20:51:33', '2016-06-27 20:51:33'),
(8, 'SR XX', 'Sepat Rawa', 'Active', '2016-06-27 20:51:39', '2016-06-27 20:51:39'),
(9, 'SR K', 'Sepat Rawa', 'Active', '2016-06-27 20:51:49', '2016-06-27 20:51:49'),
(10, 'SR KX', 'Sepat Rawa', 'Active', '2016-06-27 20:51:57', '2016-06-27 20:51:57'),
(11, 'SR KL', 'Sepat Rawa', 'Active', '2016-06-27 20:52:02', '2016-06-27 20:52:02'),
(12, 'BWPA', 'Tembang', 'Active', '2016-06-27 20:52:10', '2016-06-27 20:52:10'),
(13, 'HR KK', 'Gabus', 'Active', '2016-06-27 20:52:16', '2016-06-27 20:52:16'),
(14, 'BS HR', 'Gabus', 'Active', '2016-06-27 20:52:24', '2016-06-27 20:52:24'),
(15, 'BINTANG', 'Sepat Rawa', 'Active', '2016-06-27 21:13:24', '2016-06-27 21:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_packing`
--

CREATE TABLE `kategori_packing` (
  `id` int(11) NOT NULL,
  `packing` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_packing`
--

INSERT INTO `kategori_packing` (`id`, `packing`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Box', 'Active', '2016-02-15 10:55:07', '2016-02-15 03:55:07'),
(2, 'Karung', 'Active', '2016-02-15 03:02:15', '2016-02-15 03:02:15'),
(3, 'Loa', 'Active', '2016-02-15 03:02:20', '2016-02-15 03:02:20'),
(4, 'Keranjang', 'Active', '2016-02-15 03:02:26', '2016-02-15 03:02:26'),
(5, 'Packing Kayu', 'Active', '2016-02-15 04:19:13', '2016-02-15 04:19:13');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) UNSIGNED NOT NULL,
  `package_id` varchar(50) DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `packing_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_id`, `netto`, `packing_id`, `created_at`, `updated_at`) VALUES
(1, 'PJY16F0001', 5, 1, '2016-06-24 00:50:49', '2016-06-24 00:50:49'),
(2, 'PJY16F0001', 10, 1, '2016-06-24 00:50:49', '2016-06-24 00:50:49'),
(3, 'PJY16F0001', 15, 1, '2016-06-24 00:50:49', '2016-06-24 00:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `product_category_name`
--

CREATE TABLE `product_category_name` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category_name`
--

INSERT INTO `product_category_name` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ikan Layar', 'active', '2016-06-29 10:10:40', '2016-06-29 03:10:40'),
(2, 'Teri Nasi', 'active', '2016-06-23 01:19:53', '2016-06-23 01:19:53'),
(3, 'Hebi', 'active', '2016-06-23 01:20:00', '2016-06-23 01:20:00'),
(4, 'Gabus', 'active', '2016-06-27 20:40:44', '2016-06-27 20:40:44'),
(5, 'Sepat Rawa', 'active', '2016-06-27 20:40:52', '2016-06-27 20:40:52'),
(7, 'Pehu', 'active', '2016-06-27 20:41:05', '2016-06-27 20:41:05'),
(9, 'Siro', 'active', '2016-06-27 20:42:51', '2016-06-27 20:42:51'),
(10, 'Tembang', 'active', '2016-06-27 20:42:57', '2016-06-27 20:42:57'),
(11, 'Jengki', 'active', '2016-06-27 20:43:02', '2016-06-27 20:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `prodid` int(11) NOT NULL,
  `kode_produk` varchar(50) NOT NULL DEFAULT '',
  `nama_produk` varchar(100) NOT NULL DEFAULT '',
  `kategori_packing` varchar(100) NOT NULL DEFAULT '',
  `bruto_faktur` double NOT NULL,
  `bruto_asli` double NOT NULL,
  `tara_faktur` double NOT NULL,
  `tara_asli` double NOT NULL,
  `total_bruto_faktur` double NOT NULL,
  `total_bruto_asli` double NOT NULL,
  `total_tara_faktur` double NOT NULL,
  `total_tara_asli` double NOT NULL,
  `penyusutan` double NOT NULL,
  `penambahan` double NOT NULL,
  `netto_faktur` double NOT NULL,
  `netto_asli` double NOT NULL,
  `gudang` int(11) DEFAULT NULL,
  `keterangan` longtext,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`prodid`, `kode_produk`, `nama_produk`, `kategori_packing`, `bruto_faktur`, `bruto_asli`, `tara_faktur`, `tara_asli`, `total_bruto_faktur`, `total_bruto_asli`, `total_tara_faktur`, `total_tara_asli`, `penyusutan`, `penambahan`, `netto_faktur`, `netto_asli`, `gudang`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
(24, 'PLA16E0001', 'ATS', 'Packing Kayu', 20, 20, 1, 1, 1000, 1000, 50, 50, 0, 0, 950, 950, 2, '', 'Active', '2016-06-08 11:23:13', '2016-06-08 04:18:44'),
(25, 'PLA16E0001', 'ATS', 'Packing Kayu', 10, 10, 1, 1, 500, 500, 50, 50, 0, 0, 450, 450, 2, '', 'Active', '2016-06-16 08:55:11', '2016-06-16 01:55:11'),
(26, 'KPK16F0001', 'TMN', 'Box', 100, 100, 0, 0, 1000, 1000, 0, 0, 0, 0, 1000, 1000, 2, '', 'Active', '2016-06-17 22:29:44', '2016-06-17 22:29:44'),
(27, 'KPK16F0001', 'TMN', 'Box', 150, 150, 0, 0, 3000, 3000, 0, 0, 0, 0, 3000, 3000, 2, '', 'Active', '2016-06-17 22:29:44', '2016-06-17 22:29:44'),
(29, 'SMD16F0001', 'HR XXSI', 'Box', 24.5, 24.5, 0, 0, 784, 784, 0, 0, 0, 0, 784, 784, 2, '', 'Active', '2016-06-27 20:54:12', '2016-06-27 20:54:12'),
(30, 'SMD16F0002', 'HR XXS', 'Box', 24.5, 24.5, 0, 0, 931, 931, 0, 0, 0, 0, 931, 931, 2, '', 'Active', '2016-06-27 20:54:51', '2016-06-27 20:54:51'),
(31, 'SMD16F0003', 'SR XXS', 'Box', 24.5, 24.5, 0, 0, 3895.5, 3895.5, 0, 0, 0, 0, 3895.5, 3895.5, 2, '', 'Active', '2016-06-27 20:55:25', '2016-06-27 20:55:25'),
(32, 'SMD16F0004', 'SR XX', 'Box', 24.5, 24.5, 0, 0, 49, 49, 0, 0, 0, 0, 49, 49, 2, '', 'Active', '2016-06-28 03:56:48', '2016-06-27 20:56:48'),
(33, 'SMD16F0004', 'SR XX', 'Box', 27.5, 27.5, 0, 0, 27.5, 27.5, 0, 0, 0, 0, 27.5, 27.5, 2, '', 'Active', '2016-06-27 20:57:23', '2016-06-27 20:57:23'),
(34, 'SMD16F0005', 'SR K', 'Box', 19.5, 19.5, 1, 1, 585, 585, 30, 30, 0, 0, 555, 555, 2, '', 'Active', '2016-06-27 20:58:11', '2016-06-27 20:58:11'),
(35, 'SMD16F0005', 'SR K', 'Box', 19.5, 19.5, 0.5, 0.5, 175.5, 175.5, 4.5, 4.5, 0, 0, 171, 171, 2, '', 'Active', '2016-06-27 20:58:48', '2016-06-27 20:58:48'),
(36, 'IHS16F0001', 'BINTANG', 'Box', 34, 34, 1, 1, 102, 102, 3, 3, 0, 0, 99, 99, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(37, 'IHS16F0001', 'BINTANG', 'Box', 37.5, 37.5, 1, 1, 112.5, 112.5, 3, 3, 0, 0, 109.5, 109.5, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(38, 'IHS16F0001', 'BINTANG', 'Box', 36.5, 36.5, 1, 1, 109.5, 109.5, 3, 3, 0, 0, 106.5, 106.5, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(39, 'IHS16F0001', 'BINTANG', 'Box', 35, 35, 1, 1, 280, 280, 8, 8, 0, 0, 272, 272, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(40, 'IHS16F0001', 'BINTANG', 'Box', 35.5, 35.5, 1, 1, 177.5, 177.5, 5, 5, 0, 0, 172.5, 172.5, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(41, 'IHS16F0001', 'BINTANG', 'Box', 33, 33, 1, 1, 33, 33, 1, 1, 0, 0, 32, 32, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(42, 'IHS16F0001', 'BINTANG', 'Box', 33.5, 33.5, 1, 1, 33.5, 33.5, 1, 1, 0, 0, 32.5, 32.5, 2, '', 'Active', '2016-06-27 21:19:36', '2016-06-27 21:19:36'),
(43, 'IHS16I0002', 'HR XXSI', 'Box', 20, 20, 1, 1, 20, 20, 1, 1, 0, 0, 19, 19, 1, 'TEST', 'Active', '2016-09-08 00:16:57', '2016-09-08 00:16:57'),
(44, 'PLA16I0002', 'BINTANG', 'Box', 40, 40, 4, 4, 1600, 1600, 160, 160, 0, 0, 1440, 1440, 1, '', 'Active', '2016-09-08 00:19:04', '2016-09-08 00:19:04'),
(45, 'PLA16I0002', 'BINTANG', 'Box', 30, 30, 4, 4, 900, 900, 120, 120, 0, 0, 780, 780, 1, '', 'Active', '2016-09-08 00:19:04', '2016-09-08 00:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_id` varchar(50) NOT NULL,
  `no_faktur` varchar(50) NOT NULL,
  `namasupplier` varchar(50) NOT NULL,
  `notes` longtext NOT NULL,
  `total` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_id`, `no_faktur`, `namasupplier`, `notes`, `total`, `created_at`, `updated_at`) VALUES
(1, '20160802001', '2016000005', 'Taufik', 'notes', '72000', '2016-08-02 01:33:58', '2016-08-02 01:33:58'),
(2, '20160802002', '2016000003', 'Samat', '', '77177250', '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(3, '20160913001', '19900501', 'Ikhsan', '', '19000', '2016-09-12 21:19:33', '2016-09-12 21:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_item`
--

CREATE TABLE `purchase_order_item` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `weight` int(11) NOT NULL,
  `tara` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_item`
--

INSERT INTO `purchase_order_item` (`id`, `purchase_order_id`, `kode_produk`, `weight`, `tara`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 'KPK16F0001', 15, 0, 10, '2016-08-02 01:33:58', '2016-08-02 01:33:58'),
(2, 1, 'KPK16F0001', 14, 0, 5, '2016-08-02 01:33:58', '2016-08-02 01:33:58'),
(3, 2, 'SMD16F0001', 25, 0, 32, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(4, 2, 'SMD16F0002', 25, 0, 38, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(5, 2, 'SMD16F0003', 25, 0, 159, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(6, 2, 'SMD16F0004', 25, 0, 2, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(7, 2, 'SMD16F0004', 28, 0, 1, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(8, 2, 'SMD16F0005', 20, 1, 30, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(9, 2, 'SMD16F0005', 20, 1, 9, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(10, 3, 'IHS16I0002', 20, 1, 1, '2016-09-12 21:19:33', '2016-09-12 21:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_item_detail`
--

CREATE TABLE `purchase_order_item_detail` (
  `id` int(11) NOT NULL,
  `purchase_order_item_id` int(11) NOT NULL,
  `netto` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_item_detail`
--

INSERT INTO `purchase_order_item_detail` (`id`, `purchase_order_item_id`, `netto`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 15, 10, 100, '2016-08-02 01:33:58', '2016-08-02 01:33:58'),
(2, 2, 14, 5, 100, '2016-08-02 01:33:58', '2016-08-02 01:33:58'),
(3, 3, 25, 30, 30000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(4, 3, 25, 2, 20000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(5, 4, 25, 38, 5000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(6, 5, 25, 150, 10000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(7, 5, 25, 9, 9000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(8, 6, 25, 2, 7000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(9, 7, 28, 1, 20000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(10, 8, 20, 30, 15000, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(11, 9, 20, 9, 4500, '2016-08-02 02:13:33', '2016-08-02 02:13:33'),
(12, 10, 19, 1, 1000, '2016-09-12 21:19:33', '2016-09-12 21:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `id` int(11) NOT NULL,
  `so_id` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `payment_method` enum('Cash','Credit') NOT NULL,
  `subtotal` varchar(50) NOT NULL,
  `discount` varchar(50) NOT NULL,
  `total` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order`
--

INSERT INTO `sales_order` (`id`, `so_id`, `customer_id`, `payment_method`, `subtotal`, `discount`, `total`, `created_at`, `updated_at`) VALUES
(1, 'SOCA20160819001', 1, 'Cash', '181000', '10', '162900', '2016-09-09 09:51:52', '2016-08-19 03:01:36'),
(2, 'SOCR20160819001', 1, 'Credit', '790000', '0', '790000', '2016-09-09 09:51:56', '2016-08-19 03:13:27'),
(3, 'SOCA20160909001', 1, 'Cash', '53000', '50', '26500', '2016-09-09 03:21:38', '2016-09-09 03:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_item`
--

CREATE TABLE `sales_order_item` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order_item`
--

INSERT INTO `sales_order_item` (`id`, `sales_order_id`, `product_code`, `created_at`, `updated_at`) VALUES
(1, 1, 'PJY16F0001', '2016-08-19 10:04:20', '2016-08-19 03:01:36'),
(2, 1, 'IHS16F0001', '2016-08-19 10:04:26', '2016-08-19 03:01:36'),
(3, 2, 'PJY16F0001', '2016-08-19 03:13:27', '2016-08-19 03:13:27'),
(4, 3, 'SMD16F0001', '2016-09-09 03:21:38', '2016-09-09 03:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_item_detail`
--

CREATE TABLE `sales_order_item_detail` (
  `id` int(11) NOT NULL,
  `sales_order_item_id` int(11) NOT NULL,
  `bruto` varchar(50) NOT NULL,
  `tara` varchar(50) NOT NULL,
  `quantity` varchar(50) NOT NULL,
  `netto` varchar(50) NOT NULL,
  `price` varchar(50) NOT NULL,
  `total` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order_item_detail`
--

INSERT INTO `sales_order_item_detail` (`id`, `sales_order_item_id`, `bruto`, `tara`, `quantity`, `netto`, `price`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, '5', '0', '4', '20', '1000', '20000', '2016-08-19 03:01:36', '2016-08-19 03:01:36'),
(2, 1, '10', '0', '5', '50', '1000', '50000', '2016-08-19 03:01:36', '2016-08-19 03:01:36'),
(3, 2, '33', '1', '1', '32', '1000', '32000', '2016-08-19 03:01:36', '2016-08-19 03:01:36'),
(4, 2, '33.5', '1', '1', '32.5', '2000', '65000', '2016-08-19 03:01:36', '2016-08-19 03:01:36'),
(5, 3, '5', '0', '5', '25', '11000', '275000', '2016-08-19 03:13:27', '2016-08-19 03:13:27'),
(6, 3, '10', '0', '5', '50', '10000', '500000', '2016-08-19 03:13:27', '2016-08-19 03:13:27'),
(7, 4, '24.5', '0', '2', '49', '1000', '49000', '2016-09-09 03:21:38', '2016-09-09 03:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kodesupplier` varchar(50) NOT NULL DEFAULT '',
  `namasupplier` varchar(100) NOT NULL DEFAULT '',
  `alamat` longtext NOT NULL,
  `no_rek` longtext NOT NULL,
  `notelepon` varchar(50) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kodesupplier`, `namasupplier`, `alamat`, `no_rek`, `notelepon`, `fax_no`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PLA', 'Pala Lampu', '', '', '', '', 'Active', '2016-05-17 09:13:38', '2016-02-02 04:55:31'),
(2, 'PKA', 'Pintu Kamar', 'Citra ', '', '021000', '', 'Active', '2016-05-17 09:13:47', '2016-04-09 21:55:54'),
(3, 'PLM', 'Pintu Lama', '', '', '', '', 'Active', '2016-05-17 09:13:55', '2016-02-02 04:56:01'),
(4, 'TES', 'TEST', 'TEST          ', '', '123', '213', 'Active', '2016-05-17 09:13:21', '2016-05-17 01:38:24'),
(5, 'KPK', 'Taufik', 'jl. 1         ', '', '0000', '0000', 'Active', '2016-05-31 20:34:21', '2016-05-31 20:34:21'),
(6, 'KPK', 'Samidi', 'Jl. 2         ', '', '0001', '0001', 'Active', '2016-05-31 20:34:39', '2016-05-31 20:34:39'),
(7, 'SMD', 'Samat', '-  ', '', '-', '-', 'Active', '2016-06-27 20:50:50', '2016-06-27 20:50:50'),
(8, 'IHS', 'Ikhsan', '-', '', '-', '-', 'Active', '2016-06-27 21:12:58', '2016-06-27 21:12:58'),
(9, 'TST', 'Test2', '   ', '11112', '1111', '111', 'Active', '2016-08-22 06:09:42', '2016-08-21 23:09:42'),
(10, 'TTT', 'Test3', '', '', '', '', 'Active', '2016-08-22 02:25:26', '2016-08-22 02:25:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `userfirstname` varchar(50) DEFAULT NULL,
  `userlastname` varchar(50) DEFAULT NULL,
  `userphone` varchar(20) DEFAULT NULL,
  `useremail` varchar(150) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userfirstname`, `userlastname`, `userphone`, `useremail`, `username`, `password`, `role`, `created_at`, `updated_at`, `remember_token`, `status`) VALUES
(1, 'Jordy', 'Jonatan', '081289256242', 'jorjonna@gmail.com', 'dev', '$2y$10$Hx0RXDQaSsop9cIZ4v45KeR7iip7ZianrvxRyc4jMzF39jvufDTpC', 'Developer', '2016-04-09 06:52:13', '2016-07-27 02:30:51', 'ipcSMJVubg2ZeVymgay9z2EHaHGH9J56xGEmlfQsU4nb8dtikFzogSNVfwUd', 'ACTIVE');

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewinventorystock`
-- (See below for the actual view)
--
CREATE TABLE `viewinventorystock` (
`kode_produk` varchar(50)
,`bruto_asli` double
,`tara_asli` varchar(22)
,`quantity_sisa` int(11)
,`netto_asli` double
,`kodesupplier` varchar(50)
,`namasupplier` varchar(100)
,`gudang` varchar(101)
,`created_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewpurchaseorderreport`
-- (See below for the actual view)
--
CREATE TABLE `viewpurchaseorderreport` (
`year` int(4)
,`month` varchar(9)
,`total` double
,`created_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewsalesorderreport`
-- (See below for the actual view)
--
CREATE TABLE `viewsalesorderreport` (
`year` int(4)
,`month` varchar(9)
,`salestotal` double
,`created_at` timestamp
,`salescash` double
,`salescredit` double
);

-- --------------------------------------------------------

--
-- Structure for view `viewinventorystock`
--
DROP TABLE IF EXISTS `viewinventorystock`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewinventorystock`  AS  select `dp`.`kode_produk` AS `kode_produk`,`pr`.`bruto_asli` AS `bruto_asli`,`pr`.`tara_asli` AS `tara_asli`,`dp`.`quantity_sisa` AS `quantity_sisa`,`pr`.`netto_asli` AS `netto_asli`,`su`.`kodesupplier` AS `kodesupplier`,`su`.`namasupplier` AS `namasupplier`,concat(`gd`.`no_gudang`,' ',`gd`.`kamar_gudang`) AS `gudang`,`dp`.`created_at` AS `created_at` from ((((`produk` `pr` join `detil_produk` `dp` on((`dp`.`meta_id` = `pr`.`prodid`))) join `gudang` `gd` on((`gd`.`id` = `dp`.`gudang_id`))) join `faktur` `fa` on((`fa`.`no_faktur` = `dp`.`no_faktur`))) join `supplier` `su` on((`su`.`id` = `fa`.`supplier_id`))) union select `dp`.`kode_produk` AS `kode_produk`,`pk`.`netto` AS `bruto_asli`,'0' AS `tara_asli`,`dp`.`quantity_sisa` AS `quantity_sisa`,`pk`.`netto` AS `netto_asli`,'PJY' AS `kodesupplier`,'pdjaya' AS `namasupplier`,concat(`gd`.`no_gudang`,' ',`gd`.`kamar_gudang`) AS `gudang`,`dp`.`created_at` AS `created_at` from ((`detil_produk` `dp` join `packages` `pk` on((`pk`.`id` = `dp`.`meta_id`))) join `gudang` `gd` on((`gd`.`id` = `dp`.`gudang_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `viewpurchaseorderreport`
--
DROP TABLE IF EXISTS `viewpurchaseorderreport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewpurchaseorderreport`  AS  select year(`purchase_order`.`created_at`) AS `year`,monthname(`purchase_order`.`created_at`) AS `month`,sum(`purchase_order`.`total`) AS `total`,`purchase_order`.`created_at` AS `created_at` from `purchase_order` group by 2 order by `purchase_order`.`created_at` ;

-- --------------------------------------------------------

--
-- Structure for view `viewsalesorderreport`
--
DROP TABLE IF EXISTS `viewsalesorderreport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewsalesorderreport`  AS  select year(`so`.`created_at`) AS `year`,monthname(`so`.`created_at`) AS `month`,sum(`so`.`total`) AS `salestotal`,`so`.`created_at` AS `created_at`,coalesce((select sum(`so2`.`total`) from `sales_order` `so2` where ((`so2`.`payment_method` = 'CASH') and (year(`so2`.`created_at`) = year(`so`.`created_at`)) and (monthname(`so2`.`created_at`) = monthname(`so`.`created_at`))) group by year(`so2`.`created_at`),monthname(`so2`.`created_at`)),0) AS `salescash`,coalesce((select sum(`so2`.`total`) from `sales_order` `so2` where ((`so2`.`payment_method` = 'CREDIT') and (year(`so2`.`created_at`) = year(`so`.`created_at`)) and (monthname(`so2`.`created_at`) = monthname(`so`.`created_at`))) group by year(`so2`.`created_at`),monthname(`so2`.`created_at`)),0) AS `salescredit` from `sales_order` `so` group by 1,2 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_cost`
--
ALTER TABLE `additional_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_order`
--
ALTER TABLE `delivery_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detil_produk`
--
ALTER TABLE `detil_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faktur`
--
ALTER TABLE `faktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_packing`
--
ALTER TABLE `kategori_packing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category_name`
--
ALTER TABLE `product_category_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`prodid`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_item`
--
ALTER TABLE `purchase_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_item_detail`
--
ALTER TABLE `purchase_order_item_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_item`
--
ALTER TABLE `sales_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_item_detail`
--
ALTER TABLE `sales_order_item_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_cost`
--
ALTER TABLE `additional_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `delivery_order`
--
ALTER TABLE `delivery_order`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `detil_produk`
--
ALTER TABLE `detil_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `faktur`
--
ALTER TABLE `faktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `kategori_packing`
--
ALTER TABLE `kategori_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_category_name`
--
ALTER TABLE `product_category_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `prodid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `purchase_order_item`
--
ALTER TABLE `purchase_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `purchase_order_item_detail`
--
ALTER TABLE `purchase_order_item_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sales_order_item`
--
ALTER TABLE `sales_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sales_order_item_detail`
--
ALTER TABLE `sales_order_item_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;