<?php  
class Supplier extends Eloquent{
	protected $table= 'supplier';
	protected $fillable= ['kodesupplier','namasupplier','status'];

	public function faktur()
	{
		return $this->belongsTo('faktur');
	}
}