<?php
class detilProduk extends Eloquent{
	protected $table = 'detil_produk';

	public function produk()
	{
		return $this->hasMany('produk');
	}

	public function faktur()
	{
		return $this->hasMany('faktur');
	}
}