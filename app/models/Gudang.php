<?php
class Gudang extends Eloquent{
	protected $table = 'gudang';
	protected $fillable= ['no_gudang','kamar_gudang','pic_gudang','status'];

	public function faktur()
	{
		return $this->belongsTo('faktur');
	}
}