<?php
class Faktur extends Eloquent{
	protected $table = 'faktur';

	public function gudang()
	{
		return $this->hasMany('gudang');
	}

	public function supplier()
	{
		return $this->hasMany('supplier');
	}
}