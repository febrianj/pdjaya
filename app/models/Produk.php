<?php
class Produk extends Eloquent{
	protected $table = 'produk';

	public function detilProduk()
	{
		return $this->belongsTo('detilProduk');
	}
}