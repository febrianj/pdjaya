<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') | Inventory System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('packages/dist/css/AdminLTE.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('packages/dist/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('packages/dist/css/skins/_all-skins.min.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('packages/plugins/morris/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('packages/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('packages/plugins/datepicker/datepicker3.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('packages/plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('packages/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link href="{{ asset('packages/font-lato.css') }}" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('packages/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/select2.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{ asset('packages/dist/css/custom.css') }}">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- ================================= HEADER ============================================= -->
      <header class="main-header">
        <!-- Logo -->
        <a href="{{ URL::to('/dashboard') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>I</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Inventory</b>System</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('packages/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('packages/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('packages/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('packages/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('packages/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ asset('packages/dist/img/user_ico.png') }}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ Session::get('user_name') }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ asset('packages/dist/img/user_ico.png') }}" class="img-circle" alt="User Image">
                    <p>
                      {{ Session::get('user_name') }} - {{ Session::get('user_role') }}
                      <small>Last login on {{ Session::get('user_lastlogin')->toFormattedDateString() }}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ URL::to('/profile') }}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ URL::to('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- ======================================== END HEADER =========================================== -->
      
      <!-- ======================================== MENU SAMPING KIRI ==================================== -->
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('packages/dist/img/user_ico.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Session::get('user_name') }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">NAVIGATION MENU</li>
            <li class="active treeview">
              <a href="{{ URL::to('/dashboard') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
              </a>
              
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL::route('getCustomer') }}"><i class="fa fa-circle-o"></i> Customer</a></li>
                <li><a href="{{ URL::to('/master/discount') }}"><i class="fa fa-circle-o"></i> Discount</a></li>
                <li><a href="{{ URL::to('/master/packing-category') }}"><i class="fa fa-circle-o"></i> Packing Category</a></li>
                <li><a href="{{ URL::to('/master/product-category-name') }}"><i class="fa fa-circle-o"></i> Product Category Name</a></li>
                <li><a href="{{ URL::to('/master/product-category') }}"><i class="fa fa-circle-o"></i> Product Category</a></li>
                <li><a href="{{ URL::to('/master/product') }}"><i class="fa fa-circle-o"></i> Product</a></li>
                <li><a href="{{ URL::to('/master/storage') }}"><i class="fa fa-circle-o"></i> Storage</a></li>
                <li><a href="{{ URL::to('/master/supplier') }}"><i class="fa fa-circle-o"></i> Supplier</a></li>
                <li><a href="{{ URL::to('/master/packages') }}"><i class="fa fa-circle-o"></i> Packages</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-th"></i> <span>Activities</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL::to('/activities/stockmanagement') }}"><i class="fa fa-circle-o"></i>Delivery Order</a></li>
                <li><a href="{{ URL::to('/activities/purchaseorder') }}"><i class="fa fa-circle-o"></i>Purchase Order</a></li>
                <li><a href="{{ URL::to('/activities/salesorder') }}"><i class="fa fa-circle-o"></i>Sales Order</a></li>
                <li><a href="{{ URL::to('/activities/adjustment') }}"><i class="fa fa-circle-o"></i>Adjustment</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Reporting</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL::to('/reporting/inventorystock') }}"><i class="fa fa-circle-o"></i> Inventory Stock</a></li>
                <li><a href="{{ URL::to('/reporting/purchasereport') }}"><i class="fa fa-circle-o"></i> Purchasing</a></li>
                <li><a href="{{ URL::to('/reporting/salesreport') }}"><i class="fa fa-circle-o"></i> Sales</a></li>
              </ul>
            </li>
     
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- ====================================== END MENU SAMPING KIRI =============================== -->

      <!-- ==================================== START CONTENT ======================================== -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <!-- CONTENT USING PARTIALS VIEWS -->
          @yield('content')
      </div>
      <!-- /.content-wrapper -->
      <!-- ===================================== END CONTENT ========================================= -->
      
      <!-- ===================================== FOOTER ============================================== -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2016.</strong> All rights reserved.
	    </footer>
      <!-- ======================================== END FOOTER ====================================== -->
      
      <!-- ================================== MENU SAMPING KANAN ICON GEAR ========================== -->
      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-user bg-yellow"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                    <p>New phone +1(800)555-1234</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                    <p>nora@example.com</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-file-code-o bg-green"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                    <p>Execution time 5 seconds</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Update Resume
                    <span class="label label-success pull-right">95%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Laravel Integration
                    <span class="label label-warning pull-right">50%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Back End Framework
                    <span class="label label-primary pull-right">68%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Allow mail redirect
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Other sets of options are available
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Expose author name in posts
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Allow the user to show his name in blog posts
                </p>
              </div><!-- /.form-group -->

              <h3 class="control-sidebar-heading">Chat Settings</h3>

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Show me as online
                  <input type="checkbox" class="pull-right" checked>
                </label>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Turn off notifications
                  <input type="checkbox" class="pull-right">
                </label>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Delete chat history
                  <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                </label>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- ================================== END MENU SAMPING KANAN ICON GEAR ========================== -->
      
      <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

<!-- =========================================SCRIPT AREA================================================= -->
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('packages/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('packages/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);

      $(function () {
	        $('#timepicker').datetimepicker({
	            format: 'LT'
	        });

          //------------------------------------------------------------------------------------------//
          //add new
          $('#tambah-row-add-new').click(function(){
            $('#detail-item-add-new').append('<tr class="table-row-add-new"><td><input type="text" name="bruto[]" class="form-control"/></td><td><input type="text" name="qty[]" class="form-control" value="1"></td><td><input type="text" name="harga[]" class="form-control" value="0"></td></tr>');
          });

          $('#hapus-row-add-new').click(function(){
            $('.table-row-add-new:last').remove();
          });

          //add existing
          $('#tambah-row-add-existing').click(function(){
            $('#detail-item-add-existing').append('<tr class="table-row-add-existing"><td><input type="text" name="bruto[]" class="form-control"/></td><td><input type="text" name="qty[]" class="form-control" value="1"></td><td><input type="text" name="harga[]" class="form-control" value="0"></td></tr>');
          });

          $('#hapus-row-add-existing').click(function(){
            $('.table-row-add-existing:last').remove();
          });
	    });

      //------------------------------------------------------------------------------------------//

      // Product Page
      invoiceNo = $('input[name="invoice_no"]').val();
      $('#dataState').on('change', function() {

        switch(parseInt($(this).val())){
          case 1:
            $('#invoiceNo').val(invoiceNo);
            $('#productCode').prop('readonly', true);
            $('#invoiceNo').prop('readonly', true);
            break;
          case 2:
            $('#invoiceNo').val('');
            $('#productCode').prop('readonly', false);
            $('#invoiceNo').prop('readonly', false);
            break;
        }
      });
      $('#productSearch').on('change', function() {
        if($(this).val() == 'faktur.tanggal_tiba') {
          $('#searchField').html('<input type="date" name="from" class="form-control"><input type="date" name="to" class="form-control">');
        } else {          
          $('#searchField').html('<input type="text" name="keyword" placeholder="Keyword" required>');
        }
      });

      $('#supplier').on('change', function() {
          var key = $('#supplier').val().split('|');
          jQuery.get("../api/v1/getProductCode?keyword="+key[1], function(data) {
            $('#productCode').val(key[1]+data.id);
          });
      });

      //Delivery Order
      var picked = [];
      $('.product').on('change', function() {

        //validasi pilihan tdk boleh sama
        var idx = $(this).attr('index');

        if($.inArray($(this).val(), picked) > -1){
          alert('Product already chosen');
          $(this).val(picked[idx]);
        } else {
          picked[idx] = $(this).val();
          localStorage.setItem('selected', picked);
          $('.weight[index='+idx+']').html("");
          $('.qty[index='+idx+']').html("");
          
          jQuery.post('../api/v1/getProductDetail',{'keyword': $(this).val(),'origin': $('#origin').val() }, function(response){
            response.data.forEach(function(data){
              $('.weight[index='+idx+']').append('<p><input type="checkbox" name="weight['+idx+']['+data.prodid+']" value="'+data.prodid+'">'+data.bruto_asli+' kg - <span class="stock" index="'+data.prodid+'">'+ data.quantity_sisa +'</span></p>');
              $('.qty[index='+idx+']').append('<p><input class="inputQty" type="text" name="qty['+idx+']['+data.prodid+']" index="'+data.prodid+'"><h6 class="error" index="'+data.prodid+'"></h6></p>');
            });

            $('input.inputQty').on('input', function() {
              var k = $(this).attr('index');
              
              if(isNaN($(this).val())) {
                $('.error[index='+k+']').text('Input must be numeric.');
                $(this).css('border','1px solid red');
                $('.error[index='+k+']').css('color','red');
              }
              else if(parseInt($('.stock[index='+k+']').text()) < $(this).val()){
                $('.error[index='+k+']').text('Input must be lower than stock quantity ('+$('.stock[index='+k+']').text()+').');
                $(this).css('border','1px solid red');
                $('.error[index='+k+']').css('color','red');
              } else {
                $(this).css('border','1px solid #ddd');
                $('.error[index='+k+']').html("");
              }
            });

          });//jQuery POST
        }
      });//on change function

      //PACKAGE
      $('#calculateNetto').on('click', function() {
        totalNetto = 0;
        for(i = 0; i < 5; i++) {
          totalNetto += $('input[name="netto[]"][index="'+i+'"]').val() * $('input[name="qty[]"][index="'+i+'"]').val();
        }
        if(isNaN(totalNetto)) {
          alert('Please check your Input');
        } else {
          $('#totalNetto').val(totalNetto);
          $('input[type="submit"]').removeAttr("disabled", true);
        }
      });

      //DELIVERY ORDER
      $('#origin').on('change', function() {
        if($('#destination').val() == $(this).val() || $('#destination').val() == '' || $(this).val() == '') {
          $("#submit").attr("disabled", true);
        } else {
          $("#submit").removeAttr("disabled", true);
        }
      });
      $('#destination').on('change', function() {
        if($('#origin').val() == $(this).val() || $('#origin').val() == '' || $(this).val() == '') {
          $("#submit").attr("disabled", true);
        } else {
          $("#submit").removeAttr("disabled", true);
        }
      });

      //------------------------------------------------------------------------------------------//

      //PURCHASE ORDER
      $('#supplier').on('change',function(){
          jQuery.get('../api/v1/invoiceList?supplier_id='+$(this).val(), function(res){
            if (res.data.length == 0) {
              $('#invoice_list').html('<div class="panel panel-default"><div class="panel-body">No result found</div></div>');
            }
            else {
              $('#invoice_list').html('<div class="table-responsive"><table class="table table-striped" id="invoice_res"><tr><th></th><th>Invoice No.</th><th>Invoice Date</th></tr></table></div>');
              for(i = 0; i < res.data.length; i++) {
                $('#invoice_res').append('<tr><td><input type="radio" name="invoice_id" value="'+res.data[i].no_faktur+'"></td><td>'+res.data[i].no_faktur+'</td><td>'+res.data[i].tanggal_faktur+'</td></tr>');
              }
            }
            if (res.paid.length == 0) {
              $('#invoice_paid').html('<div class="panel panel-default"><div class="panel-body">No result found</div></div>');
            } else {
              for (i = 0; i < res.paid.length; i++){
                $('#invoice_paid').html('<div class="table-responsive"><table class="table table-striped" id="invoice_res_paid"><tr><th>Invoice No.</th><th>Invoice Date</th><th>Action</th></tr></table></div>');
                  $('#invoice_res_paid').append('<tr><td>'+res.paid[i].no_faktur+'</td><td>'+res.paid[i].tanggal_faktur+'</td><td><form><button>button</button></form></td></tr>');
              }
            }
          });
      });

      var totalArr = $('#totalArr').val();
      var arr = [];
      for(i = 0; i < totalArr; i++) {
        arr[i] = 0;
      }
      $('.priceRow_add').click(function() {
        var idx = $(this).attr('idx');
        var netto = $('input[name="weight[]"][idx="'+idx+'"]').val() - $('input[name="tara[]"][idx="'+idx+'"]').val();
        $('.priceRow[idx="'+idx+'"]').append('<div class="rowWrapper" idx="'+idx+'" arr="'+arr[idx]+'"><input type="text" size="5" value="'+netto+'" readonly> x <input class="qtyInput" idx="'+idx+'" arr="'+arr[idx]+'" type="text" name="qty_input['+idx+'][]" size="5" value=""> x <input idx="'+idx+'" arr="'+arr[idx]+'" type="text" name="price['+idx+'][]" size="25"/></div>');
        arr[idx]++;
      });

      $('.priceRow_remove').click(function() {
        var idx = $(this).attr('idx');
        $('.rowWrapper[idx="'+idx+'"][arr="'+arr[idx]+'"]:last').remove();
        if(arr[idx] != 0)
          arr[idx]--;
      });
      
      function validateQuantity() {

        for(i = 0; i < totalArr; i++) {
          var sum = 0;
          for(j = 0; j < arr[i]; j++) {
            if(isNaN($('.qtyInput[idx="'+i+'"][arr="'+j+'"]').val())) {
              alert('Quantity input must be numeric.')
              return false;
            } else if(parseInt($('.qtyInput[idx="'+i+'"][arr="'+j+'"]').val()) == 0) {
              alert('Quantity input must be greater than 0');
              return false;
            }
            sum += parseInt($('.qtyInput[idx="'+i+'"][arr="'+j+'"]').val());
          }
          if(sum != 0 && (sum > parseInt($('.max[idx="'+i+'"]').val()) || sum < parseInt($('.max[idx="'+i+'"]').val()))) {
            alert('Invalid Quantity Input. Please re-check.');
            return false;
          }
        }
        return true;
      }

      var x = 0;
      $('.additional_add').click(function (){
        $('.additional').append('<tr class="additional_row"><td><input type="text" class="form-control additional_desc" index="'+x+'" name="additional_desc[]" value=""></td><td><input type="text" class="form-control additional_price" index="'+x+'" name="additional_price[]" value="0"></td></tr>');
        x++;
      });
      
      $('.additional_remove').click(function (){
        $('.additional_row:last').remove();
        if (x > 0) {
          x--;
        }
      });

      function validateAll() {
        var totalPriceQty = 0;
        if(validateQuantity()) {
          for(i = 0; i < totalArr; i++) {
            for(j = 0; j < arr[i]; j++) {
              totalPriceQty += parseFloat($('input[name="weight[]"][idx="'+i+'"]').val() - $('input[name="tara[]"][idx="'+i+'"]').val()) * parseInt($('.qtyInput[idx="'+i+'"][arr="'+j+'"]').val()) * parseInt($('input[name="price['+i+'][]"][idx="'+i+'"][arr="'+j+'"]').val());
            }
          }
          for(k = 0; k < x; k++) {
            totalPriceQty += parseInt($('.additional_price[index="'+k+'"]').val());
          }
          $('#totalCost').val(totalPriceQty);
          $('button[type="submit"]').removeAttr('disabled');
        }
      }
      //------------------------------------------------------------------------------------------//
      // INVENTORY STOCK MANAGEMENT
      $(document).ready( function() {
        $('#inventoryFilter').on('change', function() {          
          switch(parseInt($(this).val())) {
            case 1:            
            case 3:
            case 4:             
              $('.filterField').html('<input type="text" name="value" class="form-control">');
            break;
            case 2:
              $('.filterField').html('<input type="date" name="from" class="form-control"><input type="date" name="to" class="form-control">');
            break;
            default:
              $('.filterField').html('');
          }
        })
      });

      //------------------------------------------------------------------------------------------//
      // SALES ORDER
      var items = [{}];
      $(document).ready(function() {
        
        var SalesItem = function(prodId) {
          this.prodId = prodId;
          this.itemDetails = [{}];
          this.dtlCount = 0;
        }
        var SalesItemDetail = function(bruto, tara, quantity, orderQty, price, storage) {          
          this.bruto    = bruto;
          this.tara     = tara;
          this.quantity = quantity;
          this.orderQty = orderQty;
          this.price    = price;
          this.storage  = storage;
        }

        var renderTable = function() {
          $('.salesItems').html('<tr><th>Product Code</th><th>Bruto</th><th>Tara</th><th>Order Qty</th><th>Netto</th><th>Price/kg</th><th>Total</th><th></th></tr>');
          
          for (i in items) {
            if(i != 0) {
              $('.salesItems').append('<tr><td><input type="text" class="form-control" name="prod_code['+i+']" value="'+items[i].prodId+'" readonly></td><td class="bruto '+i+'"></td><td class="tara '+i+'"></td><td class="qty '+i+'"></td><td class="netto '+i+'"></td><td class="price '+i+'"></td><td class="total '+i+'"></td><td class="removeCol '+i+'"></td></tr>');
              
              for (j in items[i].itemDetails) {
                if(j != 0) {
                  netto = (items[i].itemDetails[j].bruto * items[i].itemDetails[j].orderQty) -
                          (items[i].itemDetails[j].tara * items[i].itemDetails[j].orderQty);
                  total = netto * items[i].itemDetails[j].price;
                  $('.bruto.'+i).append('<span id="bruto-'+i+'-'+j+'"><input type="text" class="form-control" name="bruto['+i+'][]" value="'+items[i].itemDetails[j].bruto+'" size="3" readonly></span>');
                  $('.tara.'+i).append('<span id="tara-'+i+'-'+j+'"><input type="text" class="form-control" name="tara['+i+'][]" value="'+items[i].itemDetails[j].tara+'" size="3" readonly></span>');
                  $('.qty.'+i).append('<input id="qty-'+i+'-'+j+'" type="text" class="form-control" name="qty['+i+'][]" value="'+items[i].itemDetails[j].orderQty+'" size="3" readonly>');
                  $('.netto.'+i).append('<input type="text" id="netto-'+i+'-'+j+'" name="netto['+i+'][]" value="'+netto+'" class="form-control" size="3" readonly>');
                  $('.price.'+i).append('<input id="price-'+i+'-'+j+'" type="text" name="price['+i+'][]" value="'+items[i].itemDetails[j].price+'" class="form-control" size="3" readonly>');
                  $('.total.'+i).append('<input type="text" id="total-'+i+'-'+j+'" class="form-control" name="total['+i+'][]" value="'+total+'" size="3" readonly>');
                  $('.removeCol.'+i).append('<a class="btnRemove" item="'+i+'" dtl="'+j+'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><br><input type="hidden" name="dtlid['+i+'][]" value="'+j+'"></a>');
                }
              }
            }
          }

          $('.btnRemove').on('click', function() {
            var key = $(this).attr('dtl');
            var itemIndex = $(this).attr('item');
            items[itemIndex].itemDetails.splice(key, 1);
            items[itemIndex].dtlCount--;
            if(items[itemIndex].dtlCount == 0) {              
              items.splice(itemIndex, 1);
            }
            renderTable();
          });
        }

        $('#payment').on('change', function() {
          if($(this).val() != '') {            
            jQuery.get('../api/v1/getSalesOrderId?payment='+$(this).val(), function(response) {              
              $('#soId').val(response.id);
            });
          }
        });

        $('#storageProduct').on('change', function() {
          jQuery.post('../api/v1/getSalesProduct', {'productCode': $('#salesProduct option[value="'+$("#salesProduct").val()+'"]').text()}, function(response) {
            $('#itemList').html('<tr><th></th><th>Bruto</th><th>Tara</th><th>Quantity</th><th>Order Qty</th><th>Price</th><th>Storage</th></tr>');
            response.data.forEach(function(data) {
              $('#itemList').append('<tr><td><input type="checkbox" class="prod-Id" value="'+data.id+'"></td><td class="bruto-'+data.id+'">'+data.bruto_asli+'</td><td class="tara-'+data.id+'">'+data.tara_asli+'</td><td class="quantity-'+data.id+'">'+data.quantity_sisa+'</td><td><input type="text" class="form-control" id="order-qty-'+data.id+'" size="1"></td><td><input type="text" class="form-control" id="price-'+data.id+'" size="1"></td><td class="storage-'+data.id+'">'+data.no_gudang+' '+data.kamar_gudang+'</td></tr>');
            });
          });
        });

        $('#btnAddSalesProduk').on('click', function() {
          flag = 0;

          $('.prod-Id:checked').each(function(item) {
            var detailProdId = $(this).val();
            var quantity     = $('.quantity-'+detailProdId).html();
            var orderQty     = $('#order-qty-'+detailProdId).val();
            var price        = $('#price-'+detailProdId).val();
            if( $.trim(orderQty).length == 0 || $.trim(price).length == 0 || isNaN(orderQty) || isNaN(price) || parseInt(orderQty) > quantity ) {
              flag = 1;
              return false;
            }
          });

          if(flag == 1) {
            alert('Please check your input!');
          } else {
            var id     = $('#salesProduct').val();
            var prodId = $('#salesProduct option[value="'+$("#salesProduct").val()+'"]').text();
            items[id]  = new SalesItem(prodId);

            $('.prod-Id:checked').each(function(item) {
              items[id].dtlCount++;
              var detailProdId = $(this).val();
              var bruto        = $('.bruto-'+detailProdId).html();
              var tara         = $('.tara-'+detailProdId).html();
              var quantity     = $('.quantity-'+detailProdId).html();
              var orderQty     = $('#order-qty-'+detailProdId).val();
              var price        = $('#price-'+detailProdId).val();
              var storage      = $('.storage-'+detailProdId).html();
              items[id].itemDetails[detailProdId] = new SalesItemDetail(bruto, tara, quantity, orderQty, price, storage);
            });
          }
          renderTable();          
        });
      });

      var validateSalesOrder = function() {
        var subTotal = 0;
        for (i in items) {
          if (i != 0) {
            for (j in items[i].itemDetails) {
              if(j != 0){
                netto = (items[i].itemDetails[j].bruto * items[i].itemDetails[j].orderQty) -
                        (items[i].itemDetails[j].tara * items[i].itemDetails[j].orderQty);
                total = parseInt(netto) * parseInt(items[i].itemDetails[j].price);                
                subTotal += total;
              }
            }
          }
        }

        for(k = 0; k < x; k++) {
          subTotal += parseInt($('.additional_price[index="'+k+'"]').val());
        }
        discount = $('#discountRate').val() / 100;
        total = subTotal - (discount * subTotal);

        $('#subTotal').val(subTotal);
        $('#total').val(total);
        $('button[type="submit"]').prop('disabled', false); 
      }
      //------------------------------------------------------------------------------------------//
    </script>
    <!-- Bootstrap 3.3.5 -->
    
    <script src="{{ asset('packages/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="{{ asset('packages/raphael-min.js') }}"></script>
    <script src="{{ asset('packages/plugins/morris/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('packages/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('packages/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('packages/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('packages/plugins/knob/jquery.knob.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('packages/moment.js') }}"></script>
    <script src="{{ asset('packages/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('packages/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('packages/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('packages/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('packages/plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('packages/dist/js/app.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('packages/dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('packages/dist/js/demo.js') }}"></script>
    <script src="{{ asset('packages/bootstrap-datetimepicker.js') }}">
    </script>
    <script src="{{ asset('packages/select2.js') }}"></script>
<!-- ============================================ END SCRIPT ============================================ -->
  </body>
</html>