@section('title')
	{{ $title }}
@stop 

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Product Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Product Category</li>
      </ol>
    </section>

    <section class="content">
	@if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
    <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Add</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="add">
&nbsp;    
    	<form method="post" action="{{URL::to('/master/product-category/add')}}">
			{{ Form::token() }}
			<div class="form-group">
				<label>Product Category Code</label>
				<input type="text" name="Product_Category_Code" class="form-control" value="{{ Input::old('Product_Category_Code') }}">
			</div>
			<div class="form-group">
				<label>Product Category Name</label>
				<select name="Product_Category_Name" class="form-control">
					<?php 
						$productCatName = productCategoryName::where('status','=','active')->orderBy('name','ASC')->get();
					?>
					<option>Choose Here</option>
					@foreach ($productCatName as $data)
						<option value="{{ $data->name }}">{{ $data->name }}</option>
					@endforeach
				</select>
			</div>
	      	<div class="form-group">
	        <input type="submit" class="btn btn-default" name="submit" value="Submit">
	      	</div>
	      </form>	
    </div>

    <div role="tabpanel" class="tab-pane active" id="view">
&nbsp;
    	<div class="table-responsive">
			<table class="table table-striped">
				<tr>
			<th>Product Category Code</th>
			<th>Product Category Name</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
		@foreach($kategoriBarang as $data)
		<tr>
			<td>{{ $data->kode_kategori_barang }}</td>
			<td>{{ $data->kategori_barang }}</td>
			<td>{{ $data->status }}</td>
			<td><a href="{{  URL::route('editKategoriBarang',$data->id) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
		</tr>
		@endforeach
		</table>
		{{ $kategoriBarang->links() }}
		</div>
    </div>
  </div>
	</section>
@stop