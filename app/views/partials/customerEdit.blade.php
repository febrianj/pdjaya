@section('title')
	{{$title}}
@stop

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Customer
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customer > Edit</li>
      </ol>
    </section>
    <section class="content">
    	@if($errors->any())
    		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    	@endif
		<form action="{{ URL::route('updateCustomer', $customer->id) }}" method="POST">
			{{ Form::token() }}
			<div class="form-group">
				<label>Name</label>
				<input type="text" name="name" class="form-control" value="{{ $customer->name }}">
			</div>
			<div class="form-group">
				<label>Phone No</label>
				<input type="text" name="phone" class="form-control" value="{{ $customer->phone }}">
			</div>
			<div class="form-group">
				<label for="">Address</label>
				<textarea name="address" id="" cols="30" rows="10" class="form-control">{{ $customer->address }}</textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-default" value="Update">
			</div>
		</form>
	</section>
@stop