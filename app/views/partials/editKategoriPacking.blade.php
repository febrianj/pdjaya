@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Packing Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Packing Category > Edit</li>
      </ol>
    </section>

    <section class="content">
	@if($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
	<form method="post" action="{{ URL::route('updateKategoriPacking',$kategoriPacking->id) }}">
		{{ Form::token() }}
		<div class="form-group">
			<label>Packing Category</label>
			<input type="text" class="form-control" name="Packing_Category" value="{{ $kategoriPacking->packing }}">
		</div>
		<div class="form-group">
			<label>Status</label>
			<select name="status" class='form-control'>
			@if($kategoriPacking->status == 'Active')
				<option value="Active">Active</option>
				<option value="Inactive">Inactive</option>
			@elseif($kategoriPacking->status == 'Inactive')
				<option value="Inactive">Inactive</option>
				<option value="Active">Active</option>
			@endif
			</select>
		</div>
		<div class="form-group"><input type="submit" name="submit" class="btn btn-default" value="Update"></div>
	</form>
	</section>
@stop