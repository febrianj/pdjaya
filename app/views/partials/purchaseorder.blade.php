@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Purchase Order
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Activities > Purchase Order</li>
      </ol>
    </section>
    <section class="content">
    @if($errors->any())
	<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@elseif(Session::has('message'))
	<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
    @endif
	<form action="{{ URL::to('/activities/purchaseorder') }}" method="POST">
	{{ Form::token() }}
	<div class="form-group">
		<label for="">Supplier</label>

		<?php $suppliers = Supplier::where('status','=','Active')->orderBy('kodesupplier','ASC')->get(); ?>

		<select name="supplier" id="supplier" class="form-control">
			<option value="">Choose Here</option>
			@foreach($suppliers as $supplier)
			<option value="{{ $supplier->id }}">{{ $supplier->kodesupplier }} - {{ $supplier->namasupplier }}</option>
			@endforeach
		</select>
	</div>
	<div style="border:1px solid #ddd;"></div>
	<h4>Invoice List (Not Paid)</h4>
	<div id="invoice_list"></div>
	<h4>Invoice List (Paid)</h4>
	<div id="invoice_paid"></div>
	<div class="text-right"><br><button type="submit" class="btn btn-default">Submit</button></div>
	</div>
	</form>
	</section>
@stop