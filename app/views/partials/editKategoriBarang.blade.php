@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Product Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product Category > Edit</li>
      </ol>
    </section>

    <section class="content">
	@if($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
	<form method="post" action="{{ URL::route('updateKategoriBarang',$kategoriBarang->id) }}">
		{{ Form::token() }}
		<div class="form-group">
			<label>Product Category Code</label>
			<input type="text" class="form-control" name="Product_Category_Code" value="{{ $kategoriBarang->kode_kategori_barang }}">
		</div>
		<div class="form-group">
			<label>Product Category Name</label>
			<select name="Product_Category_Name" id="" class="form-control">
				<optgroup label="Selected">
					<option value="{{ $kategoriBarang->kategori_barang }}">{{ $kategoriBarang->kategori_barang }}</option>
				</optgroup>
				<optgroup label="Options">
				@foreach( $productCategoryNames as $productCategoryName )
					<option value="{{ $productCategoryName->name }}">{{ $productCategoryName->name }}</option>
				@endforeach
				</optgroup>
			</select>
		</div>
		<div class="form-group">
			<label>Status</label>
			<select name="status" class='form-control'>
				<optgroup label="Selected">
					<option value="{{ $kategoriBarang->status }}">{{ $kategoriBarang->status }}</option>
				</optgroup>
				<optgroup label="Options">
					<option value="Active">Active</option>
					<option value="Inactive">Inactive</option>
				</optgroup>
			</select>
		</div>
		<div class="form-group"><input type="submit" name="submit" class="btn btn-default" value="Update"></div>
	</form>
	</section>
@stop