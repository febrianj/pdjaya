@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Order Detail
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sales Order > Detail</li>
      </ol>
    </section>
    <section class="content">
    	<div class="table-responsive">
	    	<table class="table table-striped">
				@foreach($salesOrderItem as $item)
				<tr>
					<th class="text-center" colspan="6">{{ $item->product_code }}</th>
				</tr>
				<tr>
					<th class="col-xs-2">Bruto</th>
					<th class="col-xs-2">Tara</th>
					<th class="col-xs-2">Quantity</th>
					<th class="col-xs-2">Netto</th>
					<th class="col-xs-2">Price</th>
					<th class="col-xs-2">Total</th>
				</tr>
					@foreach($salesOrderItemDtl[$item->id] as $dtl)
					<tr>
						<td>{{ $dtl->bruto }}</td>
						<td>{{ $dtl->tara }}</td>
						<td>{{ $dtl->quantity }}</td>
						<td>{{ $dtl->netto }}</td>
						<td>Rp {{ number_format($dtl->price) }}</td>
						<td>Rp {{ number_format($dtl->total) }}</td>
					</tr>
					@endforeach
				@endforeach
			</table>
		</div>
	</section>
@stop