@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Purchase Report Detail
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Reporting > Purchase > Detail</li>
	</ol>
</section>
<section class="content">
   	<div class="table-responsive">
    	<table class="table table-striped">
	        <tr>
				<th>Purchase Order Id</th>
				<th>No Faktur</th>
				<th>Supplier</th>
				<th>Total</th>
				<th>Detail</th>
	        </tr>
	        @foreach($purchaseOrder as $item)
	      	<tr>
	      		<td>{{ $item->po_id }}</td>
	      		<td>{{ $item->no_faktur }}</td>
	      		<td>{{ $item->namasupplier }}</td>
	      		<td>Rp {{ number_format($item->total) }}</td>
	      		<td><a href="{{ URL::route('getPurchaseOrderDetail', $item->id) }}">View</a></td>
	      	</tr>
	        @endforeach
    	</table>
	</div>
</section>
@stop