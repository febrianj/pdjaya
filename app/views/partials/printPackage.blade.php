<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Print Packages</title>
  <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
  <style>
    body{margin: 30px; border:1px solid #ddd; padding:15px;}
  </style>
</head>
<body>
  <?php 
    $packages = packages::Join('detil_produk','packages.id', '=','detil_produk.meta_id')
    ->Join('gudang', 'detil_produk.gudang_id', '=', 'gudang.id')
    ->Join('kategori_packing', 'packages.packing_id', '=', 'kategori_packing.id')
    ->where('detil_produk.kode_produk','=',$package_id)
    ->get();
  ?>

  <h3>{{ $packages[0]->created_at->toFormattedDateString() }}</h3>
  <div class="table-responsive">
    <table class="table table-striped">
      <tr>
        <th>Package ID</th>
        <th>Netto</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Storage</th>
      </tr>
       @foreach($packages as $package)
       <tr>
        <td>{{ $package->package_id }} </td>
        <td>{{ $package->netto }} kg </td>
        <td>{{ $package->quantity_sisa }} {{ $package->packing }}</td>
        <td>Rp{{ number_format($package->harga_asli) }} </td>
        <td>{{ $package->no_gudang }} {{ $package->kamar_gudang }} </td>
       </tr>
      @endforeach
    </table>
  </div>
   
</body>
<!-- jQuery 2.1.4 -->
<script src="{{ asset('packages/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script src="{{ asset('packages/bootstrap/js/bootstrap.min.js') }}"></script>
</html>