<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Account</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="container">
	<div class="well">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
		@endif
		@if(Session::has('message'))
			<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
		@endif
		<form action="{{ URL::to('/account/add') }}" method="POST">
			{{ Form::token() }}
			<div class="form-group">
				<label>Firstname *</label>
				<input type="text" name="firstname" class="form-control">
			</div>
			<div class="form-group">
				<label>Lastname</label>
				<input type="text" name="lastname" class="form-control">
			</div>
			<div class="form-group">
				<label>Phone</label>
				<input type="tel" name="phone" class="form-control">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label>Username *</label>
				<input type="text" name="username" class="form-control">
			</div>
			<div class="form-group">
				<label>Password *</label>
				<input type="password" name="password" class="form-control">
			</div>
			<div class="form-group">
				<label>Role *</label>
				<select name="role" class="form-control">
					<option value="">Choose Here</option>
					<option value="Developer">Developer</option>
					<option value="Front Store">Front Store</option>
					<option value="Finance">Finance</option>
					<option value="Cashier">Cashier</option>
					<option value="Storage">Storage</option>
					<option value="Owner">Owner</option>
				</select>
			</div>
			<p>* required</p>
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-default">
			</div>
		</form>
	</div>
	<hr>
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th>Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Username</th>
				<th>Role</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
			@foreach($users as $user)
			<tr>
				<td>{{ $user->userfirstname }} {{ $user->userlastname }}</td>
				<td>{{ $user->userphone }}</td>
				<td>{{ $user->useremail }}</td>
				<td>{{ $user->username }}</td>
				<td>{{ $user->role }}</td>
				<td>{{ $user->status }}</td>
				<td><a href="{{ URL::route('editAccount',$user->id) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
			</tr>
			@endforeach
		</table>
		{{ $users->links() }}
	</div>
	</div>
</body>
<!-- jQuery 2.1.4 -->
<script src="{{ asset('packages/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('packages/bootstrap/js/bootstrap.min.js') }}"></script>
</html>