@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Storage
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Storage</li>
      </ol>
    </section>

    <section class="content">
    @if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
    <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Add</a></li>
  </ul>

   <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="add">
&nbsp;    
    	<form method="post" action="{{URL::to('/master/storage/add')}}">
			{{ Form::token() }}
			<div class="form-group">
				<label>Storage No.</label>
				<input type="text" name="Storage_Number" class="form-control" value="{{ Input::old('Storage_Number') }}">
			</div>
			<div class="form-group">
				<label>Storage Room</label>
				<input type="text" name="Storage_Room" class="form-control" value="{{ Input::old('Storage_Room') }}">
			</div>
			<div class="form-group">
				<label>PIC</label>
				<input type="text" name="Storage_PIC" class="form-control" value="{{ Input::old('Storage_PIC') }}">
			</div>				
	      <div class="form-group">
	        <input type="submit" class="btn btn-default" name="submit" value="Submit">
	      </div>
	    </form>
    </div>

    <div role="tabpanel" class="tab-pane active" id="view">
&nbsp;
    	<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>Storage No.</th>
					<th>Storage Room</th>
					<th>PIC</th>
					<th>Status</th>
					<th>Action</th>

				</tr>
				@foreach($gudang as $data)
				<tr>
					<td>{{ $data->no_gudang }}</td>
					<td>{{ $data->kamar_gudang }}</td>
					<td>{{ $data->pic_gudang }}</td>
					<td>{{ $data->status }}</td>
					<td><a href="{{  URL::route('editGudang',$data->id) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
				
				</tr>
				@endforeach
			</table>
			{{ $gudang->links() }}
		</div>
    </div>
  </div>
</section>
@stop
