@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Purchase Report
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Reporting > Purchase</li>
    </ol>
  </section>
  <section class="content">
	  <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Year</th>
          <th>Month</th>
          <th>Purchase Total</th>
          <th>Detail</th>
        </tr>
        @foreach($report as $item)
        <tr>
          <td>{{ $item->year }}</td>
          <td>{{ $item->month }}</td>
          <td>Rp {{ number_format($item->total) }}</td>
          <td><a href="{{ URL::route('getPurchaseReportDetail', array($item->year, Carbon\Carbon::parse($item->created_at)->format('m'))) }}" class="button btn-sm">View</a>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
	</section>
@stop