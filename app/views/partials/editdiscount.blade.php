@section('title')
	{{$title}}
@stop

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Discount
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Discount > Edit</li>
      </ol>
    </section>

    <section class="content">
	@if($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
	<form method="POST" action="{{ URL::route('updateDiscount',$discount->id)}}">
	{{ Form::token() }}
	<div class="form-group">
		<label>Discount Rate(%)</label>
		<input type="text" name="Discount_Rate" class="form-control" value="{{ $discount->discount_rate }}">
	</div>
	<div class="form-group">
		<label>Description</label>
		<textarea rows="5" name= "Description" class="form-control">{{ strip_tags($discount->keterangan) }}</textarea>
	</div>
	<div class="form-group">
		<label>Status</label>
		<select name="status" class='form-control'>
		@if($discount->status == 'Active')
			<option value="Active">Active</option>
			<option value="Inactive">Inactive</option>
		@elseif($discount->status == 'Inactive')
			<option value="Inactive">Inactive</option>
			<option value="Active">Active</option>
		@endif
		</select>
	</div>
	<div class="form-group"><input type="submit" name="submit" class="btn btn-default" value="Update"></div>
	</form>
	</section>
@stop