@section('title')
	{{ $title }}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Search Result
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Product</li>
      </ol>
    </section>

    <section class="content">
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th>Invoice No</th>
				<th>Supplier</th>
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Qty</th>
				<th>Price</th>
				<th>Packing</th>
				<th>Storage</th>
				<th>Bruto (kg)</th>
				<th>Tara (kg)</th>
				<th>Netto (kg)</th>
				<th>Invoice Date</th>
				<th>Arrival Date</th>
				<th>Notes</th>
				<th>Action</th>
			</tr>
			@if(count($items) < 1)
          		<center><h3>No search result found.</h3></center>
          	@else
			@foreach($items as $data)
			<tr>
				<td>{{ $data->no_faktur }}</td>
				<td>{{ $data->namasupplier }}</td>
				<td>{{ $data->kode_produk }}</td>
				<td>{{ $data->nama_produk }}</td>
				<td>{{ $data->quantity }}</td>
				<td>{{ $data->harga_asli }}</td>
				<td>{{ $data->kategori_packing }}</td>
				<td>{{ $data->no_gudang }} {{ $data->kamar_gudang }}</td>
				<td>{{ $data->bruto_asli }}</td>
				<td>{{ $data->tara_asli }}</td>
				<td>{{ $data->netto_asli }}</td>
				<td>{{ Carbon\Carbon::parse($data->tanggal_faktur)->format('d-m-Y') }}</td>
				<td>{{ Carbon\Carbon::parse($data->tanggal_tiba)->format('d-m-Y') }}</td>
				<td>{{ $data->keterangan }}</td>
				<td><a href="{{ URL::route('editProduk',$data->prodid) }}" class="btn btn-primary"><i class="fa fa-pencil">&nbsp;Edit</i></a><br><a href="{{ URL::route('deleteProduk',$data->prodid) }}" class="btn btn-danger" href=""><i class="fa fa-trash"></i>&nbsp; Delete</a></td>
			</tr>
			@endforeach
			@endif
		</table>
		</div>
		</section>
@stop