<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $title }}</title>
  <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
  <style>
    body{margin: 30px; border:1px solid #ddd; padding:15px;}
  </style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-3">
				<label>Sales Order No.</label>
				<p>{{ $salesOrder->so_id }}</p>
			</div>
			<div class="col-xs-2">
				<label>Payment</label>
				<p>{{ $salesOrder->payment_method }}</p>
			</div>
			<div class="col-xs-2">
				<label>Customer Name</label>
				<p>{{ $salesOrder->name }}</p>
			</div>
			<div class="col-xs-3">
				<label>Address</label>
				<p>{{ $salesOrder->address }}</p>
			</div>
			<div class="col-xs-2">
				<label>Order Date</label>
				<p>{{ $salesOrder->created_at->toFormattedDateString() }}</p>
			</div>
		</div>
		<div class="row">
			<table class="table table-striped">
				@foreach($salesOrderItem as $item)
				<tr>
					<th class="text-center" colspan="6">{{ $item->product_code }}</th>
				</tr>
				<tr>
					<th class="col-xs-2">Bruto</th>
					<th class="col-xs-2">Tara</th>
					<th class="col-xs-2">Quantity</th>
					<th class="col-xs-2">Netto</th>
					<th class="col-xs-2">Price</th>
					<th class="col-xs-2">Total</th>
				</tr>
					@foreach($salesOrderItemDtl[$item->id] as $dtl)
					<tr>
						<td>{{ $dtl->bruto }}</td>
						<td>{{ $dtl->tara }}</td>
						<td>{{ $dtl->quantity }}</td>
						<td>{{ $dtl->netto }}</td>
						<td>Rp {{ number_format($dtl->price) }}</td>
						<td>Rp {{ number_format($dtl->total) }}</td>
					</tr>
					@endforeach
				@endforeach
			</table>
			@if(!$additionalCost->isEmpty())
			<div class="col-xs-4 col-xs-offset-8">
				<table class="table table-striped">
					<tr>
						<th class="text-center" colspan="2">Additional Cost</th>
					</tr>
					<tr>
						<th class="col-xs-6">Description</th>
						<th class="col-xs-6">Price</th>
					</tr>
					@foreach($additionalCost as $cost)
					<tr>
						<td>{{ $cost->desc }}</td>
						<td>Rp {{ number_format($cost->price) }}</td>
					</tr>
					@endforeach
				</table>
			</div>
			@endif
			<div class="col-xs-4 col-xs-offset-8">
				<table class="table table-striped">		
					<tr>					
						<th class="col-xs-6">Subtotal</th>
						<td class="col-xs-6">Rp {{ number_format($salesOrder->subtotal) }}</td>
					</tr>
					<tr>					
						<th>Discount</th>
						<td>{{ $salesOrder->discount }} %</td>
					</tr>				
					<tr>					
						<th>Total</th>
						<td>Rp {{ number_format($salesOrder->total) }}</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>