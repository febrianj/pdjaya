@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Supplier
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Supplier</li>
      </ol>
    </section>
    <section class="content">
    @if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
    <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
  	<li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Add</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="add">
&nbsp;    
    	<form method="post" action="{{URL::to('/master/supplier/add')}}">
			{{ Form::token() }}
				<div class="form-group">
					<label>Supplier Code</label>
					<input type="text" name="Supplier_Code" class="form-control" value="{{ Input::old('Supplier_Code') }}">
				</div>
				<div class="form-group">
					<label>Supplier Name</label>
					<input type="text" name="Supplier_Name" class="form-control" value="{{ Input::old('Supplier_Name') }}">
				</div>
				<div class="form-group">
					<label>Supplier Address</label>
					<textarea rows="5" name= "Supplier_Address" class="form-control">{{ Input::old('Supplier_Address') }}</textarea>
				</div>
				<div class="form-group">
					<label>Bank Account</label>
					<textarea rows="5" name= "no_rek" class="form-control">{{ Input::old('no_rek') }}</textarea>
				</div>				
				<div class="form-group">
					<label>Fax No</label>
					<input type="text" name="Supplier_Fax" class="form-control" value="{{ Input::old('Supplier_Fax') }}">
				</div>
				<div class="form-group">
					<label>Supplier Phone</label>
					<input type="text" name="Supplier_Phone" class="form-control" value="{{ Input::old('Supplier_Phone') }}">
				</div>	
		      <div class="form-group">
		        <input type="submit" class="btn btn-default" name="submit" value="Submit">
		      </div>
	      </form>	
    </div>

    <div role="tabpanel" class="tab-pane active" id="view">
&nbsp;
		<form class="form-inline" role="search" action="{{ URL::route('searchSupplier') }}" method="POST">
			<div class="form-group">
				<select name="search">
					<option value="">Choose Option</option>
					<option value="kodesupplier">Supplier Code</option>
					<option value="namasupplier">Supplier Name</option>
				</select>
				<input type="text" name="keyword" placeholder="Keyword" required>
				<input type="submit" class="btn btn-sm btn-danger" value="Search">
			</div>
		</form>
    	<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>Supplier Code</th>
					<th>Supplier Name</th>
					<th>Supplier Address</th>
					<th>Supplier Bank Acc</th>
					<th>Supplier Fax No</th>
					<th>Supplier Phone</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
				@foreach($supplier as $data)
				<tr>
					<td>{{ $data->kodesupplier }}</td>
					<td>{{ $data->namasupplier }}</td>
					<td>{{ $data->alamat }}</td>
					<td>{{ $data->no_rek }}</td>
					<td>{{ $data->fax_no }}</td>
					<td>{{ $data->notelepon }}</td>
					<td>{{ $data->status }}</td>
					<td><a href="{{  URL::route('editSupplier',$data->id) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
				</tr>
				@endforeach
			</table>
			{{ $supplier->links() }}
		</div>
    </div>
  </div>
	</section>
@stop
