@section('title')
  {{ $title }}
@stop

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Adjustment
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Product</li>
      </ol>
    </section>

    <section class="content">
  @if(Session::has('message'))
    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
  @elseif($errors->any())
    <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
  @endif
    <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
      <a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a>
    </li>
    <li class="presentation">
      <a href="#recent" aria-controls="recent" role="tab" data-toggle="tab">Recent Data</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="view">
&nbsp;
      <form class="form-inline" role="search" action="{{URL::route('search','adjustment')}}" method="POST">
        <div class="form-group">
          <select name="search">
            <option value="">Choose Option</option>
            <option value="gudang.no_gudang">Storage</option>
            <option value="produk.kode_produk">Product Code</option>
            <option value="detil_produk.no_faktur">Invoice No.</option>
            <option value="supplier.kodesupplier">Supplier Code</option>
          </select>
          <input type="text" name="keyword" placeholder="Keyword" required>
          <input type="submit" class="btn btn-sm btn-danger" value="Search">
        </div>
      </form>
      <div class="row"><br></div>
      <div class="table-responsive">
        <table class="table table-striped">
          <tr>
            <th>Invoice No</th>
            <th>Supplier</th>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Packing</th>
            <th>Storage</th>
            <th>Bruto (kg)</th>
            <th>Tara (kg)</th>
            <th>Total Bruto (kg)</th>
            <th>Total Tara (kg)</th>
            <th>- (kg)</th>
            <th>+ (kg)</th>
            <th>Netto (kg)</th>
            <th>Invoice Date</th>
            <th>Arrival Date</th>
            <th>Notes</th>
            <th>Action</th>
          </tr>
          @foreach($items as $data)
          <tr>
            <td>{{ $data->no_faktur }}</td>
            <td>{{ $data->namasupplier }}</td>
            <td>{{ $data->kode_produk }}</td>
            <td>{{ $data->nama_produk }}</td>
            <td>{{ $data->quantity }}</td>
            <td>{{ $data->harga_faktur }}</td>
            <td>{{ $data->kategori_packing }}</td>
            <td>{{ $data->no_gudang }} {{ $data->kamar_gudang }}</td>
            <td>{{ $data->bruto_faktur }}</td>
            <td>{{ $data->tara_faktur }}</td>
            <td>{{ $data->total_bruto_faktur }}</td>
            <td>{{ $data->total_tara_faktur }}</td>
            <td>{{ $data->penyusutan }}</td>
            <td>{{ $data->penambahan }}</td>
            <td>{{ $data->netto_faktur }}</td>
            <td>{{ Carbon\Carbon::parse($data->tanggal_faktur)->format('d-m-Y') }}</td>
            <td>{{ Carbon\Carbon::parse($data->tanggal_tiba)->format('d-m-Y') }}</td>
            <td>{{ $data->keterangan }}</td>
            <td><a href="{{  URL::route('editAdjustment',$data->prodid) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
          </tr>
          @endforeach
        </table>
        {{ $items->links() }}
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="recent">
&nbsp;
      <div class="row"><br></div>
      <div class="table-responsive">
        <table class="table table-striped">
          <tr>
            <th>Invoice No</th>
            <th>Supplier</th>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Packing</th>
            <th>Storage</th>
            <th>Bruto (kg)</th>
            <th>Tara (kg)</th>
            <th>Total Bruto (kg)</th>
            <th>Total Tara (kg)</th>
            <th>- (kg)</th>
            <th>+ (kg)</th>
            <th>Netto (kg)</th>
            <th>Invoice Date</th>
            <th>Arrival Date</th>
            <th>Notes</th>
            <th>Action</th>
          </tr>
          @foreach($recents as $data)
          <tr>
            <td>{{ $data->no_faktur }}</td>
            <td>{{ $data->namasupplier }}</td>
            <td>{{ $data->kode_produk }}</td>
            <td>{{ $data->nama_produk }}</td>
            <td>{{ $data->quantity }}</td>
            <td>{{ $data->harga_faktur }}</td>
            <td>{{ $data->kategori_packing }}</td>
            <td>{{ $data->no_gudang }} {{ $data->kamar_gudang }}</td>
            <td>{{ $data->bruto_faktur }}</td>
            <td>{{ $data->tara_faktur }}</td>
            <td>{{ $data->total_bruto_faktur }}</td>
            <td>{{ $data->total_tara_faktur }}</td>
            <td>{{ $data->penyusutan }}</td>
            <td>{{ $data->penambahan }}</td>
            <td>{{ $data->netto_faktur }}</td>
            <td>{{ Carbon\Carbon::parse($data->tanggal_faktur)->format('d-m-Y') }}</td>
            <td>{{ Carbon\Carbon::parse($data->tanggal_tiba)->format('d-m-Y') }}</td>
            <td>{{ $data->keterangan }}</td>
            <td><a href="{{  URL::route('editAdjustment',$data->prodid) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
          </tr>
          @endforeach
        </table>
        {{ $recents->links() }}
      </div>
    </div>
  </div>
  </section>
@stop