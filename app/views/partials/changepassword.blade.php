@section('title')
	{{$title}}
@stop

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile > Change Password</li>
      </ol>
    </section>
    <section class="content">
    	@if($errors->any())
    		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    	@endif
    	<div class="panel panel-default">
    		<div class="panel-body">
    			<form action="{{ URL::to('/profile/change-password') }}" method="POST">
    				{{ Form::token() }}
    				<div class="form-group">
    					<label>Old Password</label>
    					<input type="password" name="old_password" class="form-control" value="{{ $user->password }}" disabled="true">
    				</div>
    				<div class="form-group">
    					<label>New Password</label>
    					<input type="password" name="new_password" class="form-control">
    				</div>
    				<div class="form-group">
    					<label>Confirm Password</label>
    					<input type="password" name="confirm_password" class="form-control">
    				</div>
    				<div class="form-group">
    					<input type="submit" name="submit" class="btn btn-default" value="Update">
    				</div>
    			</form>
    		</div>
    	</div>
	</section>
@stop