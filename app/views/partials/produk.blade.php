@section('title')
	{{ $title }}
@stop

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Product
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Product</li>
      </ol>
    </section>

    <section class="content">
	@if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
    <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
  	<li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
  	<li role="presentation"><a href="#inserted" aria-controls="inserted" role="tab" data-toggle="tab">Recent Data</a></li>
    <li role="presentation"><a href="#addnew" aria-controls="addnew" role="tab" data-toggle="tab">Add New</a></li>
    <li role="presentation"><a href="#addexisting" aria-controls="addexisting" role="tab" data-toggle="tab">Add Existing</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="addnew">
	
	<?php
		$faktur = Faktur::orderBy('no_faktur','DESC')->first();
		if ($faktur != null) {
			$digit_year = substr($faktur->no_faktur,0,4);
			$digit_code = substr($faktur->no_faktur,4);
			if ($digit_year == date('Y')) {
				$new_digit_code = (int)$digit_code + 1;

				if (strlen($new_digit_code) == 1) {
					$new_no_faktur = $digit_year."00000".(string)$new_digit_code;
				}
				else if(strlen($new_digit_code) == 2) {
					$new_no_faktur = $digit_year."0000".(string)$new_digit_code;
				}
				else if(strlen($new_digit_code) == 3) {
					$new_no_faktur = $digit_year."000".(string)$new_digit_code;
				}
				else if(strlen($new_digit_code) == 4) {
					$new_no_faktur = $digit_year."00".(string)$new_digit_code;
				}
				else if(strlen($new_digit_code) == 5) {
					$new_no_faktur = $digit_year."0".(string)$new_digit_code;
				}
				else {
					$new_no_faktur = $digit_year.(string)$new_digit_code;
				}
			}
			else {
				$new_no_faktur = date('Y')."000001";
			}
		}
		else {
			$new_no_faktur = date('Y')."000001";
		}
	?>
    	<form method="post" action="{{URL::to('/master/product/add-new')}}">
			{{ Form::token() }}
			<h4><strong>Data Item</strong></h4>
			<div class="form-group">
				<select id="dataState">
					<option value="1">New Product</option>
					<option value="2">Old Existing Data</option>
				</select>
			</div>
			<div class="form-group">
				<label>Invoice No.</label>
				<input type="text" class="form-control" id="invoiceNo" name="invoice_no" value="{{ $new_no_faktur }}" readonly>
			</div>

			<div class="form-group">
				<label>Supplier</label>
				<select name="supplier" class="form-control" id='supplier'>
				<option value="">Choose Here</option>
					
					@foreach($supplier as $item)
						<option value="{{ $item->id }}|{{ $item->kodesupplier }}">{{ $item->kodesupplier }} - {{ $item->namasupplier }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Product Code</label>
				<input type="text" class="form-control" name="product_code" id='productCode'>
			</div>

			<div class="form-group">
				<label>Product Name</label>
				<select name="product_name" class="form-control">
					<option value="">Choose Here</option>
					@foreach($kategoriBarang as $item)
					<option value="{{ $item->kode_kategori_barang }}">{{ $item->kode_kategori_barang }} - {{ $item->kategori_barang }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Packing</label>
				<select name="packing" class="form-control">					
					@foreach($kategoriPacking as $item)
					<option value="{{ $item->packing }}">{{ $item->packing }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Storage</label>
				<select name="storage" class="form-control">				
					@foreach($gudang as $item)
						<option value="{{ $item->id }}">{{ $item->no_gudang }} {{ $item->kamar_gudang }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Invoice Date</label>
				<input type="date" class="form-control" name="invoice_date">
			</div>

			<div class="form-group">
				<label>Arrival Date</label>
				<input type="date" class="form-control" name="arrival_date">
			</div>

			<div class="form-group">
				<label>Tara (kg)</label>
				<input type="text" class="form-control"	name="tara">
			</div>
			<br>
			<h4><strong>Detail Item</strong></h4>
			<div class="table-responsive">
			<table id="detail-item-add-new" class="table table-striped">
			<tr>
				<th>Bruto (kg)</th>
				<th>Qty</th>
				<th>Price</th>
			</tr>
			<tr class="table-row-add-new">
				<td><input type="text" name="bruto[]" class="form-control"/></td>
				<td><input type="text" name="qty[]" class="form-control" value="1"></td>
				<td><input type="text" name="harga[]" class="form-control" value="0"></td>
			</tr>
			</table>
			</div>

			<div class="text-right" style="margin-top:10px;clear:both;">
			  <button type="button" class="btn btn-default" id="tambah-row-add-new">+</button>
		      <button type="button" class="btn btn-default" id="hapus-row-add-new">-</button>
		    </div>

		    <br>
			<h4><strong>Notes</strong></h4>
			<div class="form-group">
				<label>Notes (if exists)</label>
				<textarea name="notes" class="form-control" cols="30" rows="10"></textarea>
			</div>

	      <div class="form-group">
	        <input type="submit" class="btn btn-default" name="submit" value="Submit">
	      </div>
	      </form>
    </div>

    <div role="tabpanel" class="tab-pane" id="inserted">
		<div class="row"><br></div>
		<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th>Invoice No</th>
				<th>Supplier</th>
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Qty</th>
				<th>Price</th>
				<th>Packing</th>
				<th>Storage</th>
				<th>Bruto (kg)</th>
				<th>Tara (kg)</th>
				<th>Netto (kg)</th>
				<th>Invoice Date</th>
				<th>Arrival Date</th>
				<th>Notes</th>
				<th>Action</th>
			</tr>
			@foreach($recents as $data)
			<tr>
				<td>{{ $data->no_faktur }}</td>
				<td>{{ $data->namasupplier }}</td>
				<td>{{ $data->kode_produk }}</td>
				<td>{{ $data->nama_produk }}</td>
				<td>{{ $data->quantity }}</td>
				<td>{{ $data->harga_asli }}</td>
				<td>{{ $data->kategori_packing }}</td>
				<td>{{ $data->no_gudang }} {{ $data->kamar_gudang }}</td>
				<td>{{ $data->bruto_asli }}</td>
				<td>{{ $data->tara_asli }}</td>
				<td>{{ $data->netto_asli }}</td>
				<td>{{ Carbon\Carbon::parse($data->tanggal_faktur)->format('d-m-Y') }}</td>
				<td>{{ Carbon\Carbon::parse($data->tanggal_tiba)->format('d-m-Y') }}</td>
				<td>{{ $data->keterangan }}</td>
				<td><a href="{{ URL::route('editProduk',$data->prodid) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil">&nbsp;Edit</i></a><br><a href="{{ URL::route('deleteProduk',$data->prodid) }}" class="btn btn-sm btn-danger" href=""><i class="fa fa-trash"></i>&nbsp; Delete</a></td>
			</tr>
			@endforeach
		</table>
			{{ $recents->links() }}
		</div>
    </div>
    <div role="tabpanel" class="tab-pane active" id="view">
&nbsp;
    	<form class="form-inline" role="search" action="{{URL::route('search','product')}}" method="POST">
		<div class="form-group">
			<select id="productSearch" name="search">
				<option value="">Choose Option</option>
				<option value="faktur.tanggal_tiba">Arrival Date</option>
				<option value="gudang.no_gudang">Storage</option>
				<option value="produk.kode_produk">Product Code</option>
				<option value="detil_produk.no_faktur">Invoice No.</option>
				<option value="supplier.kodesupplier">Supplier Code</option>
			</select>
			<span id="searchField"><input type="text" name="keyword" placeholder="Keyword" required></span>
			<input type="submit" class="btn btn-sm btn-danger" value="Search">
		</div>
		</form>
		<div class="row"><br></div>
		<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th>Invoice No</th>
				<th>Supplier</th>
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Qty</th>
				<th>Price</th>
				<th>Packing</th>
				<th>Storage</th>
				<th>Bruto (kg)</th>
				<th>Tara (kg)</th>
				<th>Netto (kg)</th>
				<th>Invoice Date</th>
				<th>Arrival Date</th>
				<th>Notes</th>
				<th>Action</th>
			</tr>
			@foreach($items as $data)
			<tr>
				<td>{{ $data->no_faktur }}</td>
				<td>{{ $data->namasupplier }}</td>
				<td>{{ $data->kode_produk }}</td>
				<td>{{ $data->nama_produk }}</td>
				<td>{{ $data->quantity }}</td>
				<td>{{ $data->harga_asli }}</td>
				<td>{{ $data->kategori_packing }}</td>
				<td>{{ $data->no_gudang }} {{ $data->kamar_gudang }}</td>
				<td>{{ $data->bruto_asli }}</td>
				<td>{{ $data->tara_asli }}</td>
				<td>{{ $data->netto_asli }}</td>
				<td>{{ Carbon\Carbon::parse($data->tanggal_faktur)->format('d-m-Y') }}</td>
				<td>{{ Carbon\Carbon::parse($data->tanggal_tiba)->format('d-m-Y') }}</td>
				<td>{{ $data->keterangan }}</td>
				<td><a href="{{ URL::route('editProduk',$data->prodid) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil">&nbsp;Edit</i></a><br><a href="{{ URL::route('deleteProduk',$data->prodid) }}" class="btn btn-sm btn-danger" href=""><i class="fa fa-trash"></i>&nbsp; Delete</a></td>
			</tr>
			@endforeach
		</table>
			{{ $items->links() }}
		</div>
    </div>
	<?php
		$data = Faktur::all();
	 ?>
	@if (count($data)) 
	<div role="tabpanel" class="tab-pane" id="addexisting">
    	<form method="post" action="{{URL::to('/master/product/add-existing')}}">
			{{ Form::token() }}
			<h4><strong>Data Item</strong></h4>

			<div class="form-group">
				<label>Invoice No</label>
				<?php 
					$f = Faktur::orderBy('no_faktur','DESC')->first();
				?>
				<input type="text" name="invoice_no" class="form-control" value="{{ $f->no_faktur }}">
			</div>

			<div class="form-group">
				<label>Supplier</label>
				<?php 
					$f = Faktur::orderBy('no_faktur','DESC')->first();
					$data = Faktur::Join('supplier','supplier.id','=','faktur.supplier_id')
					->where('faktur.no_faktur','=',$f->no_faktur)
					->first();
				?>
				<select name="supplier" class="form-control">
					<option value="{{ $data->id }}">{{ $data->kodesupplier }} - {{ $data->namasupplier }}</option>
				</select>
			</div>

			<div class="form-group">
				<?php 
					$p = Produk::orderBy('prodid','DESC')
					->where('status','=','Active')
					->first();

					if ($p != null) {
						$kode_depan = substr($p->kode_produk,0,6);
						$digit_code = substr($p->kode_produk, 6);
						$new_digit_code = (int)$digit_code + 1;

						if (strlen($new_digit_code) == 1) {
							$new_kode_produk = $kode_depan."000".(string)$new_digit_code;
						}
						else if(strlen($new_digit_code) == 2) {
							$new_kode_produk = $kode_depan."00".(string)$new_digit_code;
						}
						else if(strlen($new_digit_code) == 3) {
							$new_kode_produk = $kode_depan."0".(string)$new_digit_code;
						}
						else {
							$new_kode_produk = $kode_depan.(string)$new_digit_code;
						}
					}
				?>
				<label>Product Code</label>
				<input type="text" class="form-control" name="product_code" value="{{ $new_kode_produk }}">
			</div>

			<div class="form-group">
				<label>Product Name</label>
				<select name="product_name" class="form-control">
					<option value="">Choose Here</option>
					@foreach($kategoriBarang as $item)
					<option value="{{ $item->kode_kategori_barang }}">{{ $item->kode_kategori_barang }} - {{ $item->kategori_barang }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Packing</label>
				<select name="packing" class="form-control">
					<option value="">Choose Here</option>
					@foreach($kategoriPacking as $item)
					<option value="{{ $item->packing }}">{{ $item->packing }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<?php 
					$f = Faktur::orderBy('no_faktur','DESC')->first();
					$data = detilProduk::Join('gudang','gudang.id','=','detil_produk.gudang_id')
					->where('detil_produk.no_faktur','=',$f->no_faktur)
					->first();
				?>
				<label>Storage</label>
				<select name="storage" class="form-control">
					<option value="{{ $data->id }}">{{ $data->no_gudang }} {{ $data->kamar_gudang }}
				</option>
				</select>
			</div>

			<div class="form-group">
				<?php 
					$f = Faktur::orderBy('no_faktur','DESC')->first();
				?>
				<label>Invoice Date</label>
				<input type="date" class="form-control" name="invoice_date" value="{{ $f->tanggal_faktur }}">
			</div>

			<div class="form-group">
				<?php 
					$f = Faktur::orderBy('no_faktur','DESC')->first();
				?>
				<label>Arrival Date</label>
				<input type="date" class="form-control" name="arrival_date" value="{{ $f->tanggal_tiba }}">
			</div>

			<div class="form-group">
				<?php 
					$p = Produk::orderBy('prodid','DESC')->first();
				?>
				<label>Tara (kg)</label>
				<input type="text" class="form-control" name="tara" value="{{ $p->tara_asli }}">
			</div>

			<br>
		
			<h4><strong>Detail Item</strong></h4>
			<div class="table-responsive">
			<table id="detail-item-add-existing" class="table table-striped">
			<tr>
				<th>Bruto (kg)</th>
				<th>Qty</th>
				<th>Price</th>
			</tr>
			<tr class="table-row-add-existing">
				<td><input type="text" name="bruto[]" class="form-control"/></td>
				<td><input type="text" name="qty[]" class="form-control" value="1"></td>
				<td><input type="text" name="harga[]" class="form-control" value="0"></td>
			</tr>
			</table>
			</div>

			<div class="text-right" style="margin-top:10px;clear:both;">
			  <button type="button" class="btn btn-default" id="tambah-row-add-existing">+</button>
		      <button type="button" class="btn btn-default" id="hapus-row-add-existing">-</button>
		    </div>

		    <br>
			<h4><strong>Notes</strong></h4>
			<div class="form-group">
				<label>Notes (if exists)</label>
				<textarea name="notes" class="form-control" cols="30" rows="10"></textarea>
			</div>

	      <div class="form-group">
	        <input type="submit" class="btn btn-default" name="submit" value="Submit">
	      </div>
	      </form>
    </div>
    @else
    <div role="tabpanel" class="tab-pane" id="addexisting">
    no data
    </div>
	@endif
  </div>
	</section>
@stop