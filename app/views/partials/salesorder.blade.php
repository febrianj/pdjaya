@section('title')
  {{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Sales Order
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Activities > Sales Order</li>
    </ol>
  </section>

  <section class="content">
    @if($errors->any())
    <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    @elseif(Session::has('message'))
    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
    @endif

  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#addnew" aria-controls="addnew" role="tab" data-toggle="tab">Add New</a></li>
  </ul>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="view">
      <div class="table-responsive">
        <table class="table table-striped">
          <tr>
            <th>SO ID</th>
            <th>Payment Method</th>
            <th>Order Date</th>
            <th>Action</th>
          </tr>
          @foreach ($salesOrder as $data)
          <tr>
            <td>{{ $data->so_id }}</td>
            <td>{{ $data->payment_method }}</td>
            <td>{{ Carbon\Carbon::parse($data->created_at)->format('d-m-Y') }}</td>
            <td><a href="{{ URL::route('printSalesOrderDetail',$data->id) }}/Invoice" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-print"></i>&nbsp;Invoice</a> <a href="{{ URL::route('printSalesOrderDetail',$data->id) }}/Letter" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-print"></i>&nbsp;Order</a></td>
          </tr>
          @endforeach
        </table>
        {{ $salesOrder->links() }}
      </div>  
    </div>

    <div role="tabpanel" class="tab-pane" id="addnew">
      <br>  
      <form class="table-responsive" action="{{ URL::to('/activities/salesorder') }}" method="post">
        {{ Form::token() }}
        <div class="form-horizontal">
          <div class="form-group">
            <div class="col-sm-2 control-label"><label>No SO</label></div>
            <div class="col-sm-10"><input type="text" id="soId" name="no_so" readonly value=""></div>
          </div>        
          <div class="form-group">
            <div class="col-sm-2 control-label"><label>Date (yyyy-mm-dd)</label></div>
            <div class="col-sm-10"><input type="text" readonly value="{{ Carbon\Carbon::now()->toDateString() }}"></div>
          </div>
          <div class="form-group">
            <div class="col-sm-2 control-label"><label>Payment Method</label></div>
            <div class="col-sm-2">
              <select class="form-control" name="payment" id="payment">
                <option></option>
                <option value="CA">Cash</option>
                <option value="CR">Credit</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2 control-label"><label>Name</label></div>
            <div class="col-sm-2">
              <select name="cust_id" class="form-control">
                <option></option>
                @foreach ($customers as $customer)
                  <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <span>
          <button type="button" id="add-sales-item" data-toggle="modal" data-target="#myModal">Add Item</button>
        </span>
          <table class="salesItems table table-striped">
            
          </table>
          <div class="form-group">
            <h4>
              Additional Cost
            <button type="button" class="additional_remove">-</button>
            <button type="button" class="additional_add">+</button>
            </h4>
          </div>
          <div class="form-group">
            <div class="table-responsive">
              <table border="1" width="100%" class="additional">
                <tr>
                  <th>Additional</th>
                  <th>Price (Rp) (e.g 50000)</th>
                </tr>
              </table>
            </div>
          </div>
          <div class="form-group text-right">
            <h4><strong>SubTotal:</strong> Rp <input type="text" size="9" readonly name="subtotal" value="0" style="text-align: right;" id="subTotal"></h4>
            <h4><strong>Discount:</strong> 
            <select name="discount" id="discountRate">
              <option value="0">0%</option>
              @foreach ($discounts as $discount)
                <option value="{{ $discount->discount_rate }}">{{ $discount->discount_rate }}%</option>
              @endforeach
            </select></h4>
            <h4><strong>Grand Total:</strong> Rp <input type="text" size="9" readonly name="grandtotal" value="0" style="text-align: right;" id="total"></h4>
          </div>
          <div class="text-right"><br><button type="button" class="btn btn-primary" onclick="validateSalesOrder()">Calculate</button>&nbsp;<button type="submit" class="btn btn-default" disabled>Submit</button></div>
        </form>
      </div>
    </div>
  </section>
  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Item Detail</h4>
      </div>
      <div class="modal-body">        
        <div class="form-group" id="storageProduct">
          <label>Product</label>
          <select class="form-control" id="salesProduct">          
            <option></option>
            @foreach ($products as $product)
            <option value="{{ $product->id }}">{{ $product->kode_produk }}</option>
            @endforeach
          </select>
        </div>
      </div>      
      <table id="itemList" class="table table-responsive">
      <!-- API CONTENT -->
      </table>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" id="btnAddSalesProduk">Add Product</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop