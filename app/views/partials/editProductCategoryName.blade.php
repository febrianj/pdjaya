@section('title')
  {{ $title }}
@stop

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Product Category Name
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Product Category Name > Edit</li>
      </ol>
    </section>

    <section class="content">
  @if(Session::has('message'))
    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
  @elseif($errors->any())
    <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
  @endif

      <form method="post" action="{{URL::route('updateProductCategoryName',$productCategoryName->id)}}">
      {{ Form::token() }}
        <div class="form-group">
          <label>Product Category Name</label>
          <input type="text" name="name" class="form-control" value="{{ $productCategoryName->name }}">
        </div>
        <div class="form-group">
          <label>Status</label>
          <select name="status" class="form-control">
            @if($productCategoryName->status == 'active')
              <option value="active">active</option>
              <option value="inactive">inactive</option>
            @elseif($productCategoryName->status == 'inactive')
              <option value="inactive">inactive</option>
              <option value="active">active</option>
            @endif
          </select>
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-default" name="submit" value="Submit">
        </div>
      </form>
  </section>
@stop