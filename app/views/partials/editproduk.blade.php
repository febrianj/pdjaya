@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Product
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product > Edit</li>
      </ol>
    </section>

    <section class="content">
	@if($errors->any())
		<h5 class="text-danger">{{ $errors->first() }}</h5>
	@endif
	<form method="POST" action="{{ URL::route('updateProduk',$items->prodid)}}">
	{{ Form::token() }}
	<div class="form-group">
		<label>Invoice No.</label>
		<input type="text" class="form-control" name="invoice_no" value="{{ $items->no_faktur }}" disabled="">
	</div>
	<div class="form-group">
		<label>Supplier</label>
		<input type="text" class="form-control" name="supplier" value="{{ $items->namasupplier }}" disabled="">
	</div>
	<div class="form-group">
		<label>Product Code</label>
		<input type="text" class="form-control" name="product_code" value="{{ $items->kode_produk }}" disabled="">
	</div>
	<div class="form-group">
		<label>Product Name</label>
		<input type="text" class="form-control" name="product_name" value="{{ $items->nama_produk }}" disabled="">
	</div>
	<div class="form-group">
		<label>Packing</label>
		<input type="text" class="form-control" name="packing" value="{{ $items->kategori_packing }}" disabled="">
	</div>
	<div class="form-group">
		<label>Storage</label>
		<input type="text" class="form-control" name="storage" value="{{ $items->no_gudang }}" disabled="">
	</div>
	<div class="form-group">
		<label>Invoice Date</label>
		<input type="text" class="form-control" name="invoice_date" value="{{ $items->tanggal_faktur }}" disabled="">
	</div>
	<div class="form-group">
		<label>Arrival Date</label>
		<input type="text" class="form-control" name="arrival_date" value="{{ $items->tanggal_tiba }}" disabled="">
	</div>
	<div class="form-group">
		<label>Tara</label>
		<input type="text" class="form-control" name="tara" value="{{ $items->tara_asli }}" >
	</div>
	<div class="form-group">
		<label>Bruto</label>
		<input type="text" class="form-control" name="bruto" value="{{ $items->bruto_asli }}" >
	</div>
	<div class="form-group">
		<label>Qty</label>
		<input type="text" class="form-control" name="qty" value="{{ $items->quantity }}" >
	</div>
	<div class="form-group">
		<label>Price</label>
		<input type="text" class="form-control" name="harga" value="{{ $items->harga_asli }}" >
	</div>




		</br><h5>Detail Item</h5>
		<div class="form-group">
		<label>Notes (if exists)</label>
		<textarea name="notes" class="form-control" cols="30" rows="10">{{ $items->keterangan }}</textarea>
		</div>

		<div class="form-group"><input type="submit" name="submit" class="btn btn-info" value="Ubah"></div>

	</div>
	</form>
	</section>
@stop