@section('title')
	{{ $title }}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Supplier
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Supplier > Edit</li>
      </ol>
    </section>

    <section class="content">
	@if($errors->any())
	<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    @endif
	<form method="POST" action="{{ URL::route('updateSupplier',$supplier->id) }}">
	{{ Form::token() }}
	<div class="form-group">
		<label>Supplier Code</label>
		<input type="text" name="Supplier_Code" class="form-control" value="{{ $supplier->kodesupplier }}">
	</div>
	<div class="form-group">
		<label>Supplier Name</label>
		<input type="text" name="Supplier_Name" class="form-control" value="{{ $supplier->namasupplier }}">
	</div>
	<div class="form-group">
		<label>Supplier Address</label>
		<textarea rows="5" name= "Supplier_Address" class="form-control">
		{{ $supplier->alamat }}</textarea>
	</div>
	<div class="form-group">
		<label>Bank Account</label>
		<textarea class="form-control" name="no_rek" rows="5">{{ $supplier->no_rek }}</textarea>
	</div>
	<div class="form-group">
		<label>Fax No</label>
		<input type="text" name="Supplier_Phone" class="form-control" value="{{ $supplier->fax_no }}">
	</div>
	<div class="form-group">
		<label>Supplier Phone</label>
		<input type="text" name="Supplier_Phone" class="form-control" value="{{ $supplier->notelepon }}">
	</div>	
	<div class="form-group">
		<label>Status</label>
		<select name="status" class='form-control'>
		@if($supplier->status == 'Active')
			<option value="Active">Active</option>
			<option value="Inactive">Inactive</option>
		@elseif($supplier->status == 'Inactive')
			<option value="Inactive">Inactive</option>
			<option value="Active">Active</option>
		@endif
		</select>
	</div>
	<div class="form-group"><input type="submit" name="submit" class="btn btn-default" value="Update"></div>
	</form>
	</section>
@stop