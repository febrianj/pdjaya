@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Report Detail
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reporting > Sales > Detail</li>
      </ol>
    </section>
    <section class="content">
	   <div class="table-responsive">
      <table class="table table-striped">
        <tr>
    			<th>Sales Order Id</th>
    			<th>Payment Method</th>
    			<th>Sales Total</th>
    			<th>Date</th>
        </tr>
        @foreach($salesOrder as $item)
      	<tr>
      		<td>{{ $item->so_id }}</td>
      		<td>{{ $item->payment_method }}</td>
      		<td>Rp {{ number_format($item->total) }}</td>
      		<td>{{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}</td>
      	</tr>
        @endforeach
      </table>
     </div>
	  </section>
@stop