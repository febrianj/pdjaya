@section('title')
	{{$title}}
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Discount
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Discount</li>
      </ol>
    </section>

    <section class="content">
	@if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Add</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="add">
    &nbsp;
    	<form method="post" action="{{URL::to('/master/discount/add')}}">
			{{ Form::token() }}
				<div class="form-group">
					<label>Discount Rate(%)</label>
					<input type="text" name="Discount_Rate" class="form-control" value="{{ Input::old('Discount_Rate') }}">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea rows="5" name= "Description" class="form-control">
					{{ Input::old('Description') }}</textarea>
				</div>	
	      <div class="form-group">
	        <input type="submit" class="btn btn-default" name="submit" value="Submit">
	      </div>
	      </form>
    </div>

    <div role="tabpanel" class="tab-pane active" id="view">
    	<div class="table-responsive">
    	&nbsp;
			<table class="table table-striped">
				<tr>
					<th>Discount Rate</th>
					<th>Description</th>
					<th>Status</th>
					<th>Action</th>

				</tr>
				@foreach($discount as $data)
				<tr>
					<td>{{ $data->discount_rate }} %</td>
					<td>{{ $data->keterangan }}</td>
					<td>{{ $data->status }}</td>
					<td><a href="{{  URL::route('editDiscount',$data->id) }}"><i class="fa fa-pencil">&nbsp;Edit</i></a></td>
				</tr>
				@endforeach
			</table>
			{{ $discount->links() }}
		</div>
    </div>
  </div>
	</section>
@stop
