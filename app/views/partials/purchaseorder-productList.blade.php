@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Purchase Order - Product List
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Activities > Purchase Order > Product List</li>
      </ol>
    </section>
    <section class="content">
    @if($errors->any())
	<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    @endif

    <form action="{{ URL::to('/activities/purchaseorder/add') }}" method="post">
    	{{ Form::token() }}
    	<?php 
    		$data = Faktur::Join('supplier','faktur.supplier_id','=','supplier.id')
    		->Join('detil_produk','faktur.no_faktur','=','detil_produk.no_faktur')
    		->Join('produk','detil_produk.meta_id','=','produk.prodid')
    		->where('faktur.supplier_id','=',Session::get('supplier_id'))
    		->where('detil_produk.no_faktur','=',Session::get('invoice_id'))
    		->groupBy('produk.bruto_faktur')
    		->groupBy('detil_produk.meta_id')
    		->orderBy('detil_produk.kode_produk','ASC')
    		->get();

    		$fetch = PurchaseOrder::where('po_id', 'like', date("Ymd").'%')->orderBy('po_id', 'DESC')->first();
	        $newId = date('Ymd').str_pad(1, 3, '0', STR_PAD_LEFT);
	        if($fetch) {
	        	$newId = date('Ymd').str_pad((int)substr($fetch->po_id, 8)+1, 3, '0', STR_PAD_LEFT);
	        }
        ?>
    	<h5>Data Item</h5>
		  <div class="form-horizontal">
		  	<div class="form-group">
		      <div class="col-sm-2 control-label"><label>No PO</label></div>
		      <div class="col-sm-10"><input type="text" name="no_po" readonly value="{{ $newId }}"></div>
		    </div>
		    <div class="form-group">
		      <div class="col-sm-2 control-label"><label>No Faktur</label></div>
		      <div class="col-sm-10"><input type="text" name="no_faktur" readonly value="{{ Session::get('invoice_id') }}"></div>
		    </div>
		    <div class="form-group">
		      <div class="col-sm-2 control-label"><label>Supplier</label></div>
		      <div class="col-sm-10"><input type="text" name="nama_supplier" readonly value="{{ $data[0]->namasupplier }}"></div>
		    </div>
		    <div class="form-group">
		      <div class="col-sm-2 control-label"><label>Date (yyyy-mm-dd)</label></div>
		      <div class="col-sm-10"><input type="text" readonly value="{{ $data[0]->tanggal_faktur }}"></div>
		    </div>
		  </div>
		  <h5>Detail Item</h5>
		  <div class="table-responsive">
		  <table border="1" width="100%">
		  <tr>
		    <th style="text-align:center;">Item</th>
		    <th style="text-align:center;">Weight</th>
		    <th style="text-align:center;">Tara</th>
		    <th style="text-align:center;">Qty</th>
		    <th style="text-align:center;">Netto x Qty x Price/kg (Rp) (e.g 50000)</th>
		  </tr>
		  @for($i=0; $i<count($data); $i++)
		  <tr align="center">
		    <td><input type="text" idx="{{ $i }}" name="item[]" size="25" readonly value="{{ $data[$i]->kode_produk }}"></td>
		    <td><input type="text" idx="{{ $i }}" name="weight[]" size="5" readonly value="{{ $data[$i]->bruto_asli }}"></td>
		    <td><input type="text" idx="{{ $i }}" name="tara[]" size="5" readonly value="{{ $data[$i]->tara_asli }}"></td>
		    <td><input type="text" idx="{{ $i }}" name="qty[]" class="max" size="5" readonly value="{{ $data[$i]->quantity }}"></td>
		    <td>
		    	<div class="priceRow" idx="{{ $i }}">
		    	</div>
		    	<div class="text-right"><button type="button" idx="{{ $i }}" class="priceRow_remove">-</button><button type="button" idx="{{ $i }}" class="priceRow_add">+</button></div>
		    </td>
		  </tr>
		  @endfor
		  </table>
			<br>
		  <div class="form-group">
		  	<label for="">Notes</label>
		  	<textarea name="notes" cols="30" rows="5" class="form-control"></textarea>
		  </div>
		  <div class="form-group">
		  	<h4>
		  		Additional Cost
				<button type="button" class="additional_remove">-</button>
		  		<button type="button" class="additional_add">+</button>
		  	</h4>
		  </div>
		  <div class="form-group">
		  	<div class="table-responsive">
		  		<table border="1" width="100%" class="additional">
		  			<tr>
		  				<th>Additional</th>
		  				<th>Price (Rp) (e.g 50000)</th>
		  			</tr>
		  		</table>
		  	</div>
		  </div>
		  <div class="form-group text-right">
		  	<h2><strong>Total:</strong> Rp <input type="text" size="9" readonly name="total" value="0" style="text-align: right;" id="totalCost"></h2>
		  </div>
		  <input type="hidden" id="totalArr" value="{{ count($data) }}">
			<div class="text-right"><br><button type="button" class="btn btn-primary" onclick="validateAll()">Calculate</button>&nbsp;<button type="submit" class="btn btn-default" disabled>Submit</button></div>
	</form>
	</section>
@stop