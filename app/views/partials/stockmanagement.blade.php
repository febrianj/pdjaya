@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Delivery Order
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Activities > Delivery Order</li>
      </ol>
    </section>
    <section class="content">
	@if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Add</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="add">
  	<form method="post" action="{{ URL::to('/activities/stockmanagement') }}">
			{{ Form::token() }}
			&nbsp;
			<div class="form-group">
				<label>Origin</label>
				<select id="origin" name="origin" class="form-control">
					<option value="">Choose here</option>
					@foreach($gudang as $item)
						<option value="{{ $item->id }}">{{ $item->no_gudang }} {{ $item->kamar_gudang }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Destination</label>
				<select id="destination" name="destination" class="form-control">
					<option value="">Choose here</option>
					@foreach($gudang as $item)
						<option value="{{ $item->id }}">{{ $item->no_gudang }} {{ $item->kamar_gudang }}</option>
					@endforeach
				</select>
			</div>
	<div class="form-group">
	<div class="text-right"><br><input type="submit" id="submit" class="btn btn-default" value="Next" disabled></div>
	</form>
	</div>
</div>
		<div role="tabpanel" class="tab-pane active" id="view">
		&nbsp;
		<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th>Delivery Form</th>
				<th>Created Date</th>
				<th>Action</th>
			</tr>
			@foreach($deliveryOrder as $item)
				<tr>
					<td>{{ $item->delivery_id }}</td>
					<td>{{ $item->created_at->toFormattedDateString() }}</td>
					<td><a href="{{ URL::route('printDeliveryForm',$item->delivery_id) }}" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-print"></i>&nbsp;Print</a></td>
				</tr>
			@endforeach
			</table>
		</div>
		{{ $deliveryOrder->links() }}
		</div>
		</div>					
	</section>
@stop