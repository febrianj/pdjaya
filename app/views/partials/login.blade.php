<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login</title>
    <script src="{{ asset('packages/modernizr.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('packages/css/reset.css') }}">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="{{ asset('packages/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
  </head>

  <body>
    <form class="login" method="POST" action="{{ URL::to('/login') }}">
    {{ Form::token() }}
    @if($errors->any())
    <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    @endif
    @if(Session::has('message'))
    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
    @endif
    <fieldset>
  	<legend class="legend">Login</legend>
    <div class="input">
    	<input type="text" placeholder="Username" name="username" />
      <span><i class="fa fa-envelope-o"></i></span>
    </div>
    <div class="input">
    	<input type="password" placeholder="Password" name="password" />
      <span><i class="fa fa-lock"></i></span>
    </div>
    <button type="submit" class="submit" name="submit"><i class="fa fa-long-arrow-right"></i></button>
    </fieldset>
  </form>
    <script src='{{ asset('packages/plugins/jQuery/jQuery-2.1.4.min.js') }}'></script>
    <script src="{{ asset('packages/js/index.js') }}"></script>
  </body>
</html>
