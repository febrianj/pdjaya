@section('title')
  {{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Purchase Order Detail
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Activities > Purchase Order Detail</li>
    </ol>
  </section>
  <section class="content">  
    <div class="form-horizontal">
      <div class="form-group">
        <div class="col-sm-2">
          <label for="">Purchase Order Id</label>
        </div>
        <div class="col-sm-10">
          {{ $purchaseOrder->po_id }}
        </div>
      </div>
    </div>
    <div class="">
      <table class="table table-striped">
        <tr>
          <th>Product Code</th>
          <th>Weight</th>
          <th>Tara</th>
          <th>Quantity</th>
          <th>Detail (Netto x Qty x Price)</th>
          <th>Total</th>
        </tr>
        @foreach($purchaseOrderItem as $item)
        <tr>
          <td>{{ $item->kode_produk }}</td>
          <td>{{ $item->weight }}</td>
          <td>{{ $item->tara }}</td>
          <td>{{ $item->quantity }}</td>
          <td>
            @foreach($purchaseOrderItemDtl[$item->id] as $dtl)
             {{ $dtl->netto }} x {{ $dtl->quantity }} x {{ $dtl->price }} <br>
            @endforeach
          </td>
          <td>
            @foreach($purchaseOrderItemDtl[$item->id] as $dtl)
              {{ $dtl->netto * $dtl->quantity * $dtl->price }} <br>
            @endforeach
          </td>
        </tr>          
        @endforeach
        <tr>
          <th colspan="4"></th>
          <th>Subtotal</th>
          <th>Rp {{ number_format($itemSubtotal) }}</th>
        </tr>
      </table>
      <div class="row">
        <div class="col-sm-5 col-sm-offset-7">
          <table class="table table-striped">
            <tr>
              <th>Description</th>
              <th>Price</th>
            </tr>
            @foreach($additionalCost as $item)
            <tr>
              <td>{{ $item->desc }}</td>
              <td>Rp {{ number_format($item->price) }}</td>
            </tr>
            @endforeach
          </table>
          <table class="table table-striped">
            <tr>
              <th class="col-sm-7">Total</th>
              <th class="col-sm-5">Rp {{ number_format($purchaseOrder->total) }}</th>
            </tr>
          </table>
        </div>
      </div>

    </div>
  </section>
@stop