@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Report
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reporting > Sales</li>
      </ol>
    </section>
    <section class="content">
	   <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Year</th>
          <th>Month</th>
          <th>Cash Sales</th>
          <th>Credit Sales</th>
          <th>Sales Total</th>
          <th>Detail</th>
        </tr>
        @foreach($report as $item)
        <tr>
          <td>{{ $item->year }}</td>
          <td>{{ $item->month }}</td>
          <td>Rp {{ number_format($item->salescash) }}</td>
          <td>Rp {{ number_format($item->salescredit) }}</td>
          <td>Rp {{ number_format($item->salestotal) }}</td>
          <td><a href="{{ URL::route('getSalesReportDetail', array($item->year, Carbon\Carbon::parse($item->created_at)->format('m'), 'a')) }}" class="button btn-sm">All</a>
              <a href="{{ URL::route('getSalesReportDetail', array($item->year, Carbon\Carbon::parse($item->created_at)->format('m'), 'Cash')) }}" class="button btn-sm">Cash</a>
              <a href="{{ URL::route('getSalesReportDetail', array($item->year, Carbon\Carbon::parse($item->created_at)->format('m'), 'Credit')) }}" class="button btn-sm">Credit</a>
          </td>
        </tr>
        @endforeach
      </table>
     </div>
	  </section>
@stop