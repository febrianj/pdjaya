@section('title')
	{{ $title }}
@stop

@section('content')
	<section class="content-header">
      <h1>
        Profile
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

	<section class="content">
		@if(Session::has('message'))
			<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
		@endif
		<div class="panel panel-default">
			<div class="panel-body">
			<p><a href="{{ URL::to('/profile/edit') }}"><i class="fa fa-pencil">&nbsp;Edit</i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="{{ URL::to('/profile/change-password') }}"><i class="fa fa-lock">&nbsp;Change Password</i></a></p>
				<div class="form-group">
					<label>Name</label>
					<p>{{ $users->userfirstname }} {{ $users->userlastname }}</p>
				</div>
				<div class="form-group">
					<label>Phone</label>
					<p>{{ $users->userphone }}</p>
				</div>
				<div class="form-group">
					<label>Email</label>
					<p>{{ $users->useremail }}</p>
				</div>
				<div class="form-group">
					<label>Username</label>
					<p>{{ $users->username }}</p>
				</div>
				<div class="form-group">
					<label>Role</label>
					<p>{{ $users->role }}</p>
				</div>
			</div>
		</div>
	</section>
@stop