@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Inventory Stock
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reporting > Inventory Stock</li>
      </ol>
    </section>
    <section class="content">
    <div class="row">
      <form class="form-inline" role='search' action="{{ URL::route('searchInventoryStock') }}" method="POST">
        <div class="form-group">
        
          <select id="inventoryFilter" name="filter">
            <option value="">Choose Option</option>
            <option value="1">Supplier Name</option>
            <option value="2">Date</option>
            <option value="3">Storage</option>
            <option value="4">Supplier Code</option>
          </select>
          <span class="filterField"></span>
          <input type="submit" class="btn btn-sm btn-danger" value="Search">
        
      </div>
      </form>
    </div>
      <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Product Code</th>          
          <th>Bruto (kg)</th>
          <th>Tara (kg)</th>
          <th>Qty</th>
          <th>Netto</th>
          <th>Supplier Code</th>
          <th>Supplier Name</th>
          <th>Storage</th>
        </tr>
        @foreach ($items as $item)
        @if($item->quantity_sisa == 0)
        <tr style="color:white; background: crimson">        
        @else 
        <tr>
        @endif
          <td>{{ $item->kode_produk }}</td>
          <td>{{ $item->bruto_asli }}</td>
          <td>{{ $item->tara_asli }}</td>
          <td>{{ $item->quantity_sisa }}</td>
          <td>{{ $item->netto_asli }}</td>
          <td>{{ $item->kodesupplier }}</td>
          <td>{{ $item->namasupplier   }}</td>
          <td>{{ $item->gudang }}</td>     
        </tr>
        @endforeach
      </table>
      {{ $items->links() }}
      </div>
	   

    </section>
@stop