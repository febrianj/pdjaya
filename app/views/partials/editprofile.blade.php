@section('title')
	{{$title}}
@stop

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Profile
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile > Edit</li>
      </ol>
    </section>
    <section class="content">
    	@if($errors->any())
    		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
    	@endif
    	<div class="panel panel-default">
    		<div class="panel-body">
    			<form action="{{ URL::to('/profile/edit') }}" method="POST">
    				{{ Form::token() }}
    				<div class="form-group">
    					<label>Firstname</label>
    					<input type="text" name="firstname" class="form-control" value="{{ $user->userfirstname }}">
    				</div>
    				<div class="form-group">
    					<label>Lastname</label>
    					<input type="text" name="lastname" class="form-control" value="{{ $user->userlastname }}">
    				</div>
    				<div class="form-group">
    					<label>Phone</label>
    					<input type="tel" name="phone" class="form-control" value="{{ $user->userphone }}">
    				</div>
    				<div class="form-group">
    					<label>Email</label>
    					<input type="email" name="email" class="form-control" value="{{ $user->useremail }}">
    				</div>
    				<div class="form-group">
    					<label>Username</label>
    					<input type="text" name="username" class="form-control" value="{{ $user->username }}">
    				</div>
    				<div class="form-group">
    					<input type="submit" name="submit" class="btn btn-default" value="Update">
    				</div>
    			</form>
    		</div>
    	</div>
	</section>
@stop