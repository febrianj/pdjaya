<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit Account</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="container">
	<div class="well">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
		@endif
		<form action="{{ URL::route('updateAccount',$user->id) }}" method="POST">
			{{ Form::token() }}
			<div class="form-group">
				<label>Firstname *</label>
				<input type="text" name="firstname" class="form-control" value="{{ $user->userfirstname }}">
			</div>
			<div class="form-group">
				<label>Lastname</label>
				<input type="text" name="lastname" class="form-control" value="{{ $user->userlastname }}">
			</div>
			<div class="form-group">
				<label>Phone</label>
				<input type="tel" name="phone" class="form-control" value="{{ $user->userphone }}">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" name="email" class="form-control" value="{{ $user->useremail }}">
			</div>
			<div class="form-group">
				<label>Username *</label>
				<input type="text" name="username" class="form-control" value="{{ $user->username }}">
			</div>
			<div class="form-group">
				<label>Role *</label>
				<select name="role" class="form-control">
				@if($user->role == 'Developer')
					<option value="Developer">Developer</option>
					<option value="Front Store">Front Store</option>
					<option value="Finance">Finance</option>
					<option value="Cashier">Cashier</option>
					<option value="Storage">Storage</option>
					<option value="Owner">Owner</option>
				@elseif($user->role == 'Front Store')
					<option value="Front Store">Front Store</option>
					<option value="Developer">Developer</option>
					<option value="Finance">Finance</option>
					<option value="Cashier">Cashier</option>
					<option value="Storage">Storage</option>
					<option value="Owner">Owner</option>
				@elseif($user->role == 'Finance')
					<option value="Finance">Finance</option>
					<option value="Front Store">Front Store</option>
					<option value="Developer">Developer</option>
					<option value="Cashier">Cashier</option>
					<option value="Storage">Storage</option>
					<option value="Owner">Owner</option>
				@elseif($user->role == 'Cashier')
					<option value="Cashier">Cashier</option>
					<option value="Front Store">Front Store</option>
					<option value="Developer">Developer</option>
					<option value="Finance">Finance</option>
					<option value="Storage">Storage</option>
					<option value="Owner">Owner</option>
				@elseif($user->role == 'Storage')
					<option value="Storage">Storage</option>
					<option value="Front Store">Front Store</option>
					<option value="Developer">Developer</option>
					<option value="Finance">Finance</option>
					<option value="Cashier">Cashier</option>
					<option value="Owner">Owner</option>
				@elseif($user->role == 'Owner')
					<option value="Owner">Owner</option>
					<option value="Front Store">Front Store</option>
					<option value="Developer">Developer</option>
					<option value="Finance">Finance</option>
					<option value="Cashier">Cashier</option>
					<option value="Storage">Storage</option>
				@endif
				</select>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select name="status" class="form-control">
					@if($user->status == 'ACTIVE')
					<option value="ACTIVE">ACTIVE</option>
					<option value="INACTIVE">INACTIVE</option>
					@else
					<option value="INACTIVE">INACTIVE</option>
					<option value="ACTIVE">ACTIVE</option>
					@endif
				</select>
			</div>
			<p>* required</p>
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-default">
			</div>
		</form>
	</div>
	</div>
</body>
<!-- jQuery 2.1.4 -->
<script src="{{ asset('packages/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('packages/bootstrap/js/bootstrap.min.js') }}"></script>
</html>