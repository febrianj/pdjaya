@section('title')
  {{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Delivery Order
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Activities > Delivery Order > Delivery Form</li>
      </ol>
    </section>
&nbsp;    
    <section class="content">
    <form method="post" class="doform" action="{{ URL::to('/activities/createdeliveryform') }}">
      {{ Form::token() }}
        <div class="form-inline">
        <div class="form-group">
        <?php $gudang=Gudang::where('id','=',Session::get('origin'))->first(); ?> 
        <label>Origin: </label>
        <span><input type="text" id="origin" readonly name="origin" value="{{ $gudang->no_gudang }} {{ $gudang->kamar_gudang }}"></span>
      </div>
      &nbsp;
       <div class="form-group">
        <?php $gudang=Gudang::where('id','=',Session::get('destination'))->first(); ?> 
        <label>Destination</label>
        <span><input type="text" readonly name="destination" value="{{ $gudang->no_gudang }} {{ $gudang->kamar_gudang }}"></span>
      </div> 
      </div>
      &nbsp;
      <div class="form-inline">
       <div class="form-group">
       <?php
        $data = deliveryOrder::where('delivery_id', 'like', date("Ymd").'%')->orderBy('delivery_id', 'DESC')->first();
        $newId = date('Ymd').str_pad(1, 3, '0', STR_PAD_LEFT);
        if($data)
        {
          $new = (int)substr($data->delivery_id, 8)+1;
          $newId = date('Ymd').str_pad($new, 3, '0', STR_PAD_LEFT);
        }
        ?>
        <label>Delivery No.</label>
        <span><input type="text" readonly name='delivery_id' value="{{ $newId }}"></span>
      </div>
      &nbsp;
      <div class="form-group">
        <label>Date</label>
        {{ Carbon\Carbon::now()->format('d M Y') }}
      </div>      
      </div>

      <br><br>
      
      <div class="table-responsive">
        <table class="table table-striped">
          <tr>
            <th>Product</th>
            <th>Weight & Stock Available</th>
            <th>Quantity</th>
          </tr>
          
          @for($i = 0; $i < 10; $i++)
          <tr>
            <td>
                <select name="product[]" class="product" index="{{ $i }}">
                <option value="">Choose Here</option>
                @foreach($item as $items)
                    <option value="{{ $items->kode_produk }}">{{ $items->kode_produk }}</option>
                  @endforeach
                </select>
            </td>
            <td class="weight" index="{{ $i }}"></td>
            <td class="qty" index="{{ $i }}"></td>
          </tr>
          @endfor
          
        </table>
      </div>                 
      <div class="form-group">
      <div class="text-left"><br><input type="submit" class="btn btn-default" value="Submit"></div>
      </div>
      </form>
  </section>
@stop