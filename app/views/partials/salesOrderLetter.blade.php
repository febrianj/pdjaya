<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $title }}</title>
  <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
  <style>
    body{margin: 30px; border:1px solid #ddd; padding:15px;}
  </style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-3">
				<label>Sales Order No.</label>
				<p>{{ $salesOrder->so_id }}</p>
			</div>
			<div class="col-xs-2">
				<label>Payment</label>
				<p>{{ $salesOrder->payment_method }}</p>
			</div>
			<div class="col-xs-2">
				<label>Customer Name</label>
				<p>{{ $salesOrder->name }}</p>
			</div>
			<div class="col-xs-3">
				<label>Address</label>
				<p>{{ $salesOrder->address }}</p>
			</div>
			<div class="col-xs-2">
				<label>Order Date</label>
				<p>{{ $salesOrder->created_at->toFormattedDateString() }}</p>
			</div>
		</div>
		<div class="row">
			<table class="table table-striped">
				@foreach($salesOrderItem as $item)
				<tr>
					<th class="text-center" colspan="6">{{ $item->product_code }}</th>
				</tr>
				<tr>
					<th class="col-xs-3">Bruto</th>
					<th class="col-xs-3">Tara</th>
					<th class="col-xs-3">Quantity</th>
					<th class="col-xs-3">Netto</th>
				</tr>
					@foreach($salesOrderItemDtl[$item->id] as $dtl)
					<tr>
						<td>{{ $dtl->bruto }}</td>
						<td>{{ $dtl->tara }}</td>
						<td>{{ $dtl->quantity }}</td>
						<td>{{ $dtl->netto }}</td>
					</tr>
					@endforeach
				@endforeach
			</table>
		</div>
	</div>
</body>
</html>