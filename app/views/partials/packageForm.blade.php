@section('title')
  {{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Package Form
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Package</li>
      </ol>
    </section>
&nbsp;    
    <section class="content">
     @if(Session::has('message'))
        <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
      @elseif($errors->any())
        <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
      @endif
    <form method="post" action="{{ URL::to('/master/packages/form') }}">
      {{ Form::token() }}
        <div class="form-inline">
          <div class="form-group">
          <?php $produk=Produk::where('prodid','=',Session::get('produk1'))->first(); ?> 
          <label>Products</label>        
            <?php
                  $totalPrice = $totalNetto = 0;
                  $DUS = Session::get('dus');
            ?>
            @foreach (Session::get('id') as $id)
              <?php 
                $produk = Produk::Join('detil_produk', 'produk.prodid', '=', 'detil_produk.meta_id')
                ->where('detil_produk.id', '=', $id)
                ->get();

                $qty = Session::get('qty')[$id]; 

                $price = Session::get('price')[$id];
              ?>
              <ul>
              @foreach($produk as $i)
                <li>{{ $i->kode_produk }} ({{ $i->quantity_sisa }} items) = {{ $i->netto_asli }} kg x {{ $qty }} items x Rp{{ number_format($price) }} </li>
                <?php $totalPrice += (int)$qty * (double)$i->netto_asli * (int)$price;
                      $totalNetto += $i->netto_asli * $qty;
                    ?>
              @endforeach
              </ul>
             @endforeach
            </div>
      </div>

      <div class="form-group">
          Total Price = Rp{{ number_format($totalPrice) }} <br>
          @ Box Price = Rp{{ number_format($DUS) }} <br>
          Total Netto = {{ $totalNetto }} kg
          <input type="hidden" name="totalPrice" value="{{ $totalPrice }}">
          <input type="hidden" name="dus" value="{{ $DUS }}">
      </div>

      &nbsp;
      <div class="form-inline">
        <div class="form-group">
         <?php
          $data = packages::where('package_id', 'like', 'PJY'.date('y').'%')->orderBy('package_id', 'DESC')->first();
          $newId =  date('y').chr((date('m')-1) + 65).str_pad(1, 4, '0', STR_PAD_LEFT);
          if($data)
          {
            $x = substr($data->package_id, 6);          
            $kode = (int)$x + 1;
            $newId =  date('y').chr((date('m')-1) + 65).str_pad($kode, 4, '0', STR_PAD_LEFT);
          }
          ?>
          <label>Package ID</label>
          <span><input type="text" name='package_id' value="PJY{{ $newId }}" readonly></span>
        </div>
      &nbsp;
        <div class="form-group">
          <label>Date</label>
          {{ Carbon\Carbon::now()->format('d M Y') }}
        </div>      
      </div>

      <div class="form-group">
      	<?php $packing = kategoriPacking::where('status', '=', 'active')->get(); ?>
      	<label>Packing</label>
    		<select name="packing" id="" class="form-control">    			
    			@foreach($packing as $item)
    				<option value="{{ $item->id }}"> {{ $item->packing }}</option>
    			@endforeach
    		</select>
      </div>

	  
      <div class="form-group">
      	<?php $gudang = gudang::where('status', '=', 'active')->get(); ?>
      	<label>Storage</label>
    		<select name="storage" id="" class="form-control">    			
    			@foreach($gudang as $item)
    				<option value="{{ $item->id }}"> {{ $item->no_gudang }} {{ $item->kamar_gudang }}</option>
    			@endforeach
    		</select>
      </div>
		
      <br><br>
      
      <div class="table-responsive">
        <table class="table table-striped">
          <tr>
            <th>Netto (Kg)</th>
            <th>Quantity</th>
          </tr>

          <tr>
            <td>
                <input type="text" name="netto[]" value="0" index="0">
            </td>
            <td>
              	<input type="text" name="qty[]" value="0" index="0">
            </td>
          </tr>

          <tr>
            <td>
                <input type="text" name="netto[]" value="0" index="1">
            </td>
            <td>
              	<input type="text" name="qty[]" value="0" index="1">
            </td>
          </tr>

          <tr>
            <td>
                <input type="text" name="netto[]" value="0" index="2">
            </td>
            <td>
              	<input type="text" name="qty[]" value="0" index="2">
            </td>
          </tr>

          <tr>
            <td>
                <input type="text" name="netto[]" value="0" index="3">
            </td>
            <td>
                <input type="text" name="qty[]" value="0" index="3">
            </td>
          </tr>

          <tr>
            <td>
                <input type="text" name="netto[]" value="0" index="4">
            </td>
            <td>
                <input type="text" name="qty[]" value="0" index="4">
            </td>
          </tr>
        </table>
      </div>                 
      
      <div class="text-right">
        <h4>Total Netto <input id="totalNetto" type="text" value="0" size="10" readonly></h4>
        <input type="button" id="calculateNetto" class="btn btn-info" value="Calculate"> <input type="submit" class="btn btn-default" value="Submit" disabled>
      </div>
      
      </form>
  </section>
@stop