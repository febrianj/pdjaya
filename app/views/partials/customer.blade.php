@section('title')
	{{ $title }}
@stop

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Customer
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Customer</li>
      </ol>
    </section>

    <section class="content">
	@if(Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
	@elseif($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
    <!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
  		<li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    	<li role="presentation"><a href="#addnew" aria-controls="addnew" role="tab" data-toggle="tab">Add New</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="view">	
	    &nbsp;
			<div class="table-responsive">
				<table class="table table-striped">
					<tr>
						<th>Name</th>
						<th>Phone</th>
						<th>Address</th>
						<th>Action</th>
					</tr>
					@foreach ($customers as $customer)
					<tr>
						<td>{{ $customer->name }}</td>
						<td>{{ $customer->phone }}</td>
						<td>{{ $customer->address }}</td>
						<td><a href="{{ URL::route('getUpdateCustomer', $customer->id) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Update Data</a></td>
					</tr>
					@endforeach
				</table>
				{{ $customers->links() }}
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="addnew">
	    &nbsp;
	    	<form method="POST" action="{{ URL::route('addCustomer') }}">
	    		{{ Form::token() }}
	    		<div class="form-group">
	    			<label for="">Name</label>
	    			<input type="text" class="form-control" name="name" placeholder="Name">
	    		</div>
	    		<div class="form-group">
	    			<label for="">Phone No</label>
	    			<input type="text" class="form-control" name="phone" placeholder="Phone No">
	    		</div>
	    		<div class="form-group">
	    			<label for="">Address</label>
	    			<textarea name="address" id="" cols="30" rows="10" class="form-control" placeholder="Address"></textarea>
	    		</div>
	    		<div class="form-group">
	    			<input type="submit" class="btn btn-default" value="Submit">
	    		</div>
	    	</form>
	    </div>
	</section>
@stop