@section('title')
	{{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Storage
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Storage > Edit</li>
      </ol>
    </section>

    <section class="content">
	@if($errors->any())
		<div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
	@endif
	<form method="POST" action="{{ URL::route('updateGudang',$gudang->id)}}">
	{{ Form::token() }}
	<div class="form-group">
		<label>Storage No.</label>
		<input type="text" name="Storage_Number" class="form-control" value="{{ $gudang->no_gudang }}">
	</div>
	<div class="form-group">
		<label>Storage Room</label>
		<input type="text" name="Storage_Room" class="form-control" value="{{ $gudang->kamar_gudang }}">
	</div>
	<div class="form-group">
		<label>PIC</label>
		<input type="text" name="Storage_PIC" class="form-control" value="{{ $gudang->pic_gudang }}">
	</div>

	<div class="form-group">
		<label>Status</label>
		<select name="status" class='form-control'>
		@if($gudang->status == 'Active')
			<option value="Active">Active</option>
			<option value="Inactive">Inactive</option>
		@elseif($gudang->status == 'Inactive')
			<option value="Inactive">Inactive</option>
			<option value="Active">Active</option>
		@endif
		</select>
	</div>
	<div class="form-group"><input type="submit" name="submit" class="btn btn-default" value="Update"></div>
	</form>
	</section>
@stop