<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Print Delivery Order</title>
  <link rel="stylesheet" href="{{ asset('packages/bootstrap/css/bootstrap.min.css') }}">
  <style>
    body{margin: 30px; border:1px solid #ddd; padding:15px;}
  </style>
</head>
<body>
  <?php 
    $data = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
    ->Join('delivery_order','detil_produk.id','=','delivery_order.meta_id')
    ->where('delivery_order.delivery_id','=',$delivery_id)
    ->groupBy('detil_produk.kode_produk')
    ->get(array('delivery_order.delivery_id','delivery_order.origin','delivery_order.destination','delivery_order.created_at','detil_produk.kode_produk','produk.bruto_asli','delivery_order.quantity','delivery_order.meta_id'));
  ?>
    <div class="container-fluid">
      <div class="row form-inline">
        <div class="col-xs-3 form-group">
          <label>DO No.</label>
          <p>{{ $data[0]->delivery_id }}</p>
        </div>
        <div class="col-xs-3 form-group">
          <label>Origin</label>
          <p>{{ $data[0]->origin }}</p>
        </div>
        <div class="col-xs-3 form-group">
          <label>Destination</label>
          <p>{{ $data[0]->destination }}</p>
        </div>
        <div class="col-xs-3 form-group">
          <label>Date</label>
          <p>{{ $data[0]->created_at->toFormattedDateString() }}</p>
        </div>
      </div>

      <div class="row">
        <table class="table table-striped">
          <tr>
            <th>Product</th>
            <th>KG</th>
            <th>Quantity</th>
            <th>T</th>
            <th>Netto</th>
            <th>Total Qty</th>
          </tr>
          <br><br>
          @foreach($data as $item)
          <tr>
            <td>{{ $item->kode_produk }}</td>
            <td>
              <?php 
              $gudang = explode(' ',$data[0]->origin);

              //get Gudang ID
              $getGudang = Gudang::where('no_gudang','=',$gudang[0])
              ->where('kamar_gudang','=',$gudang[1])->first();

              $items = Produk::Join('detil_produk', 'produk.prodid', '=', 'detil_produk.meta_id')
              ->Join('delivery_order','detil_produk.id','=','delivery_order.meta_id')
              ->where('detil_produk.gudang_id','=',$getGudang->id)
              ->where('detil_produk.kode_produk','=',$item->kode_produk)
              ->where('delivery_order.delivery_id','=',$delivery_id)
              ->orderBy('produk.bruto_asli','ASC')
              ->get(array('produk.bruto_asli','delivery_order.quantity','produk.tara_asli'));
              
              $bruto = array();
              $qty = array();
              $tara = array();
              $netto = array();
              ?>
              
              @foreach($items as $item)
                <?php array_push($bruto, $item->bruto_asli); ?>
              @endforeach

              {{ implode(', ',$bruto) }}
            </td>
            
            <td>
              @foreach($items as $item)
                <?php array_push($qty, $item->quantity); ?>
              @endforeach

              {{ implode(', ',$qty) }}
            </td>

            <td>
              @foreach($items as $item)
                <?php array_push($tara, $item->tara_asli); ?>
              @endforeach

              {{ implode(', ',$tara) }}
            </td>
            
            <td>
              <?php 
                for($i=0;$i<count($bruto);$i++){
                  $net = ((double)$bruto[$i] * (int)$qty[$i]) - ((double)$tara[$i] * (int)$qty[$i]);
                  array_push($netto, $net);
                }
              ?>

              {{ implode(', ',$netto) }}
            </td>
            <td>
              <?php 
              $totalqty = 0;
              for($i = 0; $i < count($qty); $i++)
                $totalqty += $qty[$i];
              ?>
              {{ $totalqty }}
            </td>
          </tr>
          @endforeach
        </table>  
      </div>
    </div>
</body>
</html>