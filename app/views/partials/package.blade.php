@section('title')
  {{$title}}
@stop

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Package Form
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Master > Package</li>
      </ol>
    </section>
    <section class="content">
  @if(Session::has('message'))
    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
  @elseif($errors->any())
    <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
  @endif
<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a></li>
    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Add</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="add">
    <form method="post" action="{{ URL::to('/master/packages') }}">
      {{ Form::token() }}
      &nbsp;
        <div class="form-group">
          <label for="">Packing Cost</label>
          <input type="text" class="form-control" name="packing_cost">
        </div>
        <div class="table-responsive">
        <table class="table table-striped">
        <tr>
          <th>Product Id</th>
          <th>Product Name</th>
          <th>Weight (kg)</th>
          <th>Storage</th>
          <th>Stock Available</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
            @for($i=0; $i<count($items); $i++)
              <?php $gudang = Gudang::where('id','=',$items[$i]->gudang_id)->first(); ?>
            <tr>
              <td><input type="checkbox" value="{{ $items[$i]->id }}" name="id[{{ $items[$i]->id }}]"> {{ $items[$i]->kode_produk }} </td>
              <td>{{ $items[$i]->nama_produk }}</td>
              <td>{{ $items[$i]->bruto_asli }}</td>
              <td>{{$gudang->no_gudang }} {{ $gudang->kamar_gudang }}</td>
              <td>{{ $items[$i]->quantity_sisa }}</td>
              <td><input type="text" name="qty[{{ $items[$i]->id }}]" value="0"></td>
              <td><input type="text" name="price[{{ $items[$i]->id }}]" value="{{ $items[$i]->harga_asli }}" @if($items[$i]->harga_asli != 0) readonly @endif></td>
            </tr>
            @endfor
        </table>
        </div>
          
  <div class="form-group">
  <div class="text-right"><br><input type="submit" class="btn btn-info" value="Next"></div>
  </form>
  </div>
      </div>
      <div role="tabpanel" class="tab-pane active" id="view">
        &nbsp;
        <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Package ID</th>
          <th>Price (Asset)</th>
          <th>Created Date</th>
          <th>Action</th>
        </tr>
        <?php $packages = packages::Join('detil_produk','packages.id','=','detil_produk.meta_id')->orderBy('packages.package_id','DESC')->groupBy('packages.package_id')->Paginate(20); ?>
        @foreach($packages as $package)
          <tr>
            <td>{{ $package->package_id }}</td>
            <td>Rp{{ number_format($package->harga_asli) }} / kg</td>
            <td>{{ $package->created_at->toFormattedDateString() }}</td>
            <td><a href="{{ URL::route('printPackage', $package->package_id) }}" target="_blank" class="btn btn-sm btn-default">
            <i class="fa fa-print"></i>&nbsp;View & Print</a></td>
          </tr>
        @endforeach
        </table>
        {{ $packages->links() }}
        </div>
        </div>
      </div>          

  </section>
@stop