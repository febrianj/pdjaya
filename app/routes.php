<?php

//-------------------------------------API-------------------------------------------------//

Route::get("/api/v1/getProductCode", array('uses' => 'ApiController@getProductCode'));
Route::post("/api/v1/getProductDetail", array('uses' => 'ApiController@getProductDetail'));
Route::get('/api/v1/invoiceList', array('uses' => 'ApiController@getInvoiceList'));
Route::get('/api/v1/getSalesOrderId', array('uses' => 'ApiController@getSalesOrderId'));
Route::post('/api/v1/getSalesProduct', array('uses' => 'ApiController@getSalesOrderProduct'));

//------------------------------------------------------------------------------------------//

Route::get('/', array('uses' => 'AuthController@getLogin'));
Route::post('/login',array('before'=>'csrf', 'uses'=> 'AuthController@doLogin'));
Route::get('/logout', array('uses'=>'AuthController@getLogout'));

Route::get('/profile', array('uses' => 'AccountController@getProfile'));
Route::get('/profile/edit', array('uses' => 'AccountController@getEditProfile'));
Route::post('/profile/edit', array('uses' => 'AccountController@doEditProfile'));
Route::get('/profile/change-password', array('uses' => 'AccountController@getChangePassword'));
Route::post('/profile/change-password', array('uses' => 'AccountController@doChangePassword'));

Route::get('/account', array('uses' => 'AccountController@getAccount'));
Route::post('/account/add', array('before'=>'csrf','uses'=>'AccountController@doAddAccount'));
Route::get('/account/{id}/edit', array('as' => 'editAccount', 'uses' => 'AccountController@getEditAccount'));
Route::post('/account/{id}/edit', array('as' => 'updateAccount', 'uses' => 'AccountController@doEditAccount'));

Route::get('/dashboard', array('uses' => 'DashController@getDashboard'));

Route::get('/master/customer', array('as' => 'getCustomer', 'uses' => 'MasterController@getCustomer'));
Route::post('/master/customer', array('as' => 'addCustomer', 'before' => 'csrf', 'uses' => 'MasterController@addCustomer'));
Route::get('/master/customer/{id}/edit', array('as' => 'getUpdateCustomer', 'uses' => 'MasterController@getUpdateCustomer'));
Route::post('/master/customer/{id}/edit', array('as' => 'updateCustomer', 'uses' => 'MasterController@doUpdateCustomer'));

Route::get('/master/product', array('uses' => 'MasterController@getProduk'));
Route::post('/master/product/add-new', array('before' => 'csrf' , 'uses' => 'MasterController@doAddProdukBaru'));
Route::post('/master/product/add-existing', array('before' => 'csrf' , 'uses' => 'MasterController@doAddProdukExisting'));
Route::get('/master/product/{prodid}/edit', array('as' => 'editProduk','uses' => 'MasterController@getUpdateProduk' ));
Route::get('/master/product/{prodid}/delete', array('as' => 'deleteProduk','uses' => 'MasterController@doDeleteProduk' ));
Route::post('/master/product/{prodid}/edit', array('as' => 'updateProduk' , 'before' => 'csrf' , 'uses' => 'MasterController@doUpdateProduk' ));
Route::post('/master/product/search/{param}', array('as'=>'search','uses' => 'MasterController@searchProduct'));

Route::get('/master/supplier', array('uses' =>'MasterController@getSupplier' ));
Route::get('/master/supplier/{id}/edit', array('as' => 'editSupplier' , 'uses' => 'MasterController@getUpdateSupplier' ));
Route::post('/master/supplier/add', array('before' => 'csrf' , 'uses' => 'MasterController@doAddSupplier'));
Route::post('/master/supplier/{id}/edit', array('as' => 'updateSupplier' , 'before' => 'csrf' , 'uses' => 'MasterController@doUpdateSupplier' ));
Route::post('/master/supplier/search', array('as' => 'searchSupplier','uses' => 'MasterController@searchSupplier'));

Route::get('/master/discount', array('uses' =>'MasterController@getDiscount' ));
Route::get('/master/discount/{id}/edit', array('as' => 'editDiscount' , 'uses' => 'MasterController@getUpdateDiscount' ));
Route::post('/master/discount/add', array('before' => 'csrf' , 'uses' => 'MasterController@doAddDiscount'));
Route::post('/master/discount/{id}/edit', array('as' => 'updateDiscount' , 'before' => 'csrf' , 'uses' => 'MasterController@doUpdateDiscount' ));

Route::get('/master/product-category', array('uses' => 'MasterController@getKategoriBarang'));
Route::get('/master/product-category/{id}/edit', array('as' => 'editKategoriBarang', 'uses' => 'MasterController@getUpdateKategoriBarang'));
Route::post('/master/product-category/add', array('before' => 'csrf', 'uses' => 'MasterController@doAddKategoriBarang'));
Route::post('/master/product-category/{id}/edit', array('as' => 'updateKategoriBarang', 'before' => 'csrf', 'uses' => 'MasterController@doUpdateKategoriBarang'));

Route::get('/master/product-category-name', array('uses' => 'MasterController@getProductCategoryName'));
Route::get('/master/product-category-name/{id}/edit', array('as' => 'editProductCategoryName', 'uses' => 'MasterController@getEditProductCategoryName'));
Route::post('/master/product-category-name/add', array('before' => 'csrf', 'uses' => 'MasterController@doAddProductCategoryName'));
Route::post('/master/product-category-name/{id}/edit', array('as' => 'updateProductCategoryName', 'before' => 'csrf', 'uses' => 'MasterController@doUpdateProductCategoryName'));

Route::get('/master/packing-category', array('uses' => 'MasterController@getKategoriPacking'));
Route::get('/master/packing-category/{id}/edit', array('as' => 'editKategoriPacking','uses' => 'MasterController@getUpdateKategoriPacking'));
Route::post('/master/packing-category/add', array('before' => 'csrf', 'uses' => 'MasterController@doAddKategoriPacking'));
Route::post('/master/packing-category/{id}/edit', array('as' => 'updateKategoriPacking', 'before' => 'csrf', 'uses' => 'MasterController@doUpdateKategoriPacking'));

Route::get('/master/storage', array('uses' => 'MasterController@getGudang' ));
Route::get('/master/storage/{id}/edit', array('as' => 'editGudang','uses' => 'MasterController@getUpdateGudang'));
Route::post('/master/storage/add', array('before' => 'csrf' , 'uses' => 'MasterController@doAddGudang'));
Route::post('/master/storage/{id}/edit', array('as' => 'updateGudang' , 'before' => 'csrf' , 'uses' => 'MasterController@doUpdateGudang' ));

Route::get('/master/packages', array('uses' => 'MasterController@getPackageForm'));
Route::post('master/packages', array('before' => 'csrf', 'uses' => 'MasterController@doCreatePackage'));
Route::get('/master/packages/form', array('uses' => 'MasterController@getPackages'));
Route::post('/master/packages/form', array('uses' => 'MasterController@addPackage'));
Route::get('/master/packages/{package_id}', array('as' => 'printPackage', 'uses' => 'MasterController@getPrintPackage'));

Route::get('/activities/stockmanagement', array('uses' =>'ActivitiesController@getStockManagement' ));
Route::post('/activities/stockmanagement', array('before'=>'csrf','uses' =>'ActivitiesController@doStockManagement' ));

Route::get('/activities/createdeliveryform', array('uses' =>'ActivitiesController@getCreateDeliveryForm' ));
Route::post('/activities/createdeliveryform', array('uses' =>'ActivitiesController@doCreateDeliveryForm' ));
Route::get('/activities/deliveryform/{delivery_id}', array('as' => 'printDeliveryForm', 'uses' =>'ActivitiesController@getPrintDeliveryForm' ));

Route::get('/activities/purchaseorder', array('uses' =>'ActivitiesController@getPurchaseOrder'));
Route::post('/activities/purchaseorder', array('uses' => 'ActivitiesController@doPurchaseOrder'));
Route::get('/activities/purchaseorder/productList', array('uses' => 'ActivitiesController@getProductList'));
Route::post('/activities/purchaseorder/add', array('before' => 'csrf', 'uses' => 'ActivitiesController@addPurchaseOrder'));
Route::get('/activities/purchaseorder/{id}', array('as' => 'getPurchaseOrderDetail', 'uses' => 'ActivitiesController@getPurchaseOrderDetail'));
Route::get('/activities/purchaseorder/{id}/doPay', array('as' => 'payPurchaseOrder', 'uses' => 'ActivitiesController@doPayPurchaseOrder'));

Route::get('/activities/salesorder', array('uses' =>'ActivitiesController@getSalesOrder' ));
Route::post('/activities/salesorder', array('before' => 'csrf', 'uses' => 'ActivitiesController@addSalesOrder'));
Route::get('/activities/salesorder/{id}/{type}', array('as' => 'printSalesOrderDetail', 'uses' =>'ActivitiesController@getSalesOrderDetail' ));

Route::get('/activities/adjustment', array('uses' => 'ActivitiesController@getAdjustmentData'));
Route::get('/activities/adjustment/{prodid}/edit', array('as' => 'editAdjustment', 'uses' => 'ActivitiesController@getEditAdjustment'));
Route::post('/activities/adjustment/{prodid}/edit', array('as' => 'updateAdjustment', 'uses' => 'ActivitiesController@doEditAdjustment'));

Route::get('/reporting/inventorystock', array('uses' =>'ReportController@getInventoryStock' ));
Route::post('/reporting/inventorystock', array('as' => 'searchInventoryStock','uses' => 'ReportController@searchInventoryStock'));
Route::get('/reporting/purchasereport', array('uses' =>'ReportController@getPurchaseReport' ));
Route::get('/reporting/purchasereport/{year}/{month}', array('as' => 'getPurchaseReportDetail', 'uses' =>'ReportController@getPurchaseReportDetail' ));
Route::get('/reporting/salesreport', array('uses' =>'ReportController@getSalesReport' ));
Route::get('/reporting/salesreport/{year}/{month}/{type}', array('as' => 'getSalesReportDetail', 'uses' =>'ReportController@getSalesReportDetail' ));
