<?php
class AuthController extends BaseController{
	public $restful = true;
	protected $layout = 'layout.master';

	public function getLogin()
	{
		return View::make('partials.login');
	}

	public function doLogin()
	{
		$rules = array(
			'username' => 'required',
			'password' => 'required'
			);

		$val = Validator::make(Input::all(), $rules);

		if ($val->fails()) {
		return Redirect::back()
        ->withErrors($val) // send back all errors to the login form
        ->withInput(); // send back the input
		} 
		else {

		$credentials = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'status' => 'ACTIVE'
			);

    	// attempt to do the login
    	if(Auth::attempt($credentials)) 
    	{
          // validation successful!
          //get userid from DB
          $user_id = Auth::user()->id;
    	  $user_fN = Auth::user()->userfirstname;
    	  $user_lN = Auth::user()->userlastname;
    	  $user_role = Auth::user()->role;
    	  $user_lastlogin = Auth::user()->created_at;
          //store to session
          Session::put('user_id',$user_id);
          Session::put('user_name', $user_fN." ".$user_lN);
          Session::put('user_role', $user_role);
          Session::put('user_lastlogin', $user_lastlogin);

          return Redirect::to('/dashboard');

   		}
   		else 
   		{        
        // validation not successful, send back to form 
        return Redirect::back()
        ->withErrors("Email & Password did not match. Please try again.")
        ->withInput();
    	}
	 }
	}

	public function getLogout()
	{
	  Auth::logout(); // log the user out of our application
      Session::flush();
      return Redirect::to('/')->withErrors('You are logged out.'); // redirect the user to the login screen
	}
}