<?php

class DashController extends BaseController {

	public $restful = true;
	protected $layout = 'layout.master';

	public function getDashboard()
	{
		if (Session::has('user_id'))
		{
			$this->layout->content = View::make('partials.dashboard')->withTitle('Dashboard');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
}
