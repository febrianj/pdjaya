<?php
class ApiController extends Controller {
	public $restful = true;
	public function getProductCode() {
		$keyword = $_GET['keyword'];
		
		$id = Produk::where('kode_produk', 'like', $keyword.date('y').'%')
		->orderBy('kode_produk', 'DESC')->groupBy('kode_produk')->first();
		
		if($id == null) {
			$newId =  date('y').chr((date('m')-1) + 65).str_pad(1, 4, '0', STR_PAD_LEFT);
		}
		else{
			$x = substr($id->kode_produk, 6);
			$kode = (int)$x + 1;
			$newId =  date('y').chr((date('m')-1) + 65).str_pad($kode, 4, '0', STR_PAD_LEFT);
		}

		return Response::json(array('id' => $newId));
	}

	public function getProductDetail() {
		$keyword = Request::input('keyword');
		$origin = explode(' ',Request::input('origin'));

		$gudang = Gudang::where('no_gudang','=',$origin[0])->where('kamar_gudang','=',$origin[1])->first();

		$produk = DB::table('produk')
			->Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
			->select('produk.prodid','produk.kode_produk','produk.bruto_asli','detil_produk.quantity_sisa')
			->where('produk.kode_produk','=',$keyword)
			->where('detil_produk.quantity_sisa','!=',0)
			->where('detil_produk.gudang_id','=',$gudang->id);

		$package = DB::table('packages')
			->Join('detil_produk','detil_produk.meta_id','=','packages.id')
			->Join('gudang','detil_produk.gudang_id','=','gudang.id')
			->select('packages.id','packages.package_id','packages.netto','detil_produk.quantity_sisa')
			->where('packages.package_id','=',$keyword)
			->where('detil_produk.quantity_sisa','!=',0)
			->where('detil_produk.gudang_id','=',$gudang->id);

		$data = $produk->union($package)->get();
		
		return Response::json(array('data' => $data));
	}

	public function getInvoiceList() {
		$supplier_id = $_GET['supplier_id'];

		$invoice = Faktur::where('invoice_status','=','Not Paid')
		->where('supplier_id','=',$supplier_id)
		->groupBy('no_faktur')
		->get();

		$paid = Faktur::where('invoice_status','=','Paid')
		->where('supplier_id','=',$supplier_id)
		->groupBy('no_faktur')
		->get();		
		return Response::json(array('data' => $invoice, 'paid' => $paid));
	}

	public function getSalesOrderId() {
		$payment = $_GET['payment'];
		$id = SalesOrder::where('so_id', 'like', 'SO'.$payment.date("Ymd").'%')
						->orderBy('so_id', 'DESC')->first();

		$newId = date('Ymd').str_pad(1, 3, '0', STR_PAD_LEFT);
        if($id) {
        	$newId = date('Ymd').str_pad((int)substr($id->so_id, 12)+1, 3, '0', STR_PAD_LEFT);
        }
        return Response::json(array('id' => 'SO'.$payment.$newId));
	}

	public function getSalesOrderProduct() {
		$productCode = Request::input('productCode');
		$produk = DB::table('produk')
				->Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
				->Join('gudang','detil_produk.gudang_id','=','gudang.id')
				->where('produk.kode_produk','=',$productCode)
				->where('detil_produk.quantity_sisa','!=',0)
				->select('produk.prodid','produk.kode_produk','produk.bruto_asli', 'produk.tara_asli','detil_produk.quantity_sisa', 'gudang.no_gudang', 'gudang.kamar_gudang', 'detil_produk.id')
				->orderBy('3', 'asc');

		$package = DB::table('packages')
				->Join('detil_produk','detil_produk.meta_id','=','packages.id')
				->Join('gudang','detil_produk.gudang_id','=','gudang.id')
				->where('packages.package_id','=',$productCode)
				->where('detil_produk.quantity_sisa','!=',0)
				->select('packages.id','packages.package_id',DB::raw('packages.netto AS bruto_asli'), DB::raw('"0" AS tara_asli'), 'detil_produk.quantity_sisa', 'gudang.no_gudang', 'gudang.kamar_gudang', 'detil_produk.id');
		$data = $produk->union($package);
		$data = $data->orderBy('bruto_asli', 'asc')->get();
		return Response::json(array('data' => $data));
	}
}