<?php
class MasterController extends BaseController{
	public $restful = true;
	protected $layout = 'layout.master';

	public function getCustomer() {
		if (Session::has('user_id'))
		{
			$customers = Customer::paginate(20);
			$this->layout->content = View::make('partials.customer', compact('customers'))->withTitle('Master Customer');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function addCustomer() {
		if (Session::has('user_id'))
		{
			$mustBeFilled = array('name' => 'required');
			$validate = Validator::make(Input::all(), $mustBeFilled);
			if ($validate->fails()) {
				return Redirect::to('/master/customer')->withErrors($validate);
			}
			$customer = new Customer;
			$customer->name = Input::get('name');
			$customer->phone = Input::get('phone');
			$customer->address = Input::get('address');
			$customer->save();

			return Redirect::to('/master/customer')->with('message','New Customer Added.');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getUpdateCustomer($id) {
		if (Session::has('user_id')) {
			$customer = Customer::whereId($id)->first();
			$this->layout->content = View::make('partials.customerEdit', compact('customer'))->withTitle('Edit Master Customer');
		} else {
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateCustomer($id) {
		if (Session::has('user_id')) {
			$customer = Customer::whereId($id)->first();
			$customer->name = Input::get('name');
			$customer->phone = Input::get('phone');
			$customer->address = Input::get('address');
			$customer->save();
			return Redirect::to('/master/customer')->with('message','Customer data updated');
		} else {
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getProduk()
	{
		if (Session::has('user_id'))
		{
			//VIEW TABLE
		    //get items
		    $items = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','produk.gudang','=','gudang.id')
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->Paginate(20);

		    $today = date('Y-m-d');


		    $recents = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','produk.gudang','=','gudang.id')
		    ->where('produk.created_at', '>=', $today)
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->paginate(20);
		    
		    //INPUT SECTION
		    $supplier = Supplier::where('status','=','active')->orderBy('namasupplier','ASC')->get();
		    $gudang = Gudang::where('status','=','active')->orderBy('no_gudang','ASC')->get();
		    $kategoriBarang = kategoriBarang::where('status','=','active')->orderBy('kode_kategori_barang','ASC')->get();
		    $kategoriPacking = kategoriPacking::where('status','=','active')->orderBy('packing','ASC')->get();

			$this->layout->content = View::make('partials.produk', array(
				'items' => $items,
				'supplier' => $supplier,
				'gudang' => $gudang,
				'kategoriBarang' => $kategoriBarang,
				'kategoriPacking' => $kategoriPacking,
				'recents' => $recents
			))->withTitle('Product');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function searchProduct($param) {
		if (Session::has('user_id'))
		{
			//VIEW TABLE
		    //get items

		    
		    $search = Input::get('search');
		    $keyword = Input::get('keyword');

	    	$items = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','detil_produk.gudang_id','=','gudang.id')
		    ->where($search, 'like', '%'.$keyword.'%')
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->Get();
		    //INPUT SECTION
		    $supplier = Supplier::where('status','=','active')->orderBy('namasupplier','ASC')->get();
		    $gudang = Gudang::where('status','=','active')->orderBy('no_gudang','ASC')->get();
		    $kategoriBarang = kategoriBarang::where('status','=','active')->orderBy('kode_kategori_barang','ASC')->get();
		    $kategoriPacking = kategoriPacking::where('status','=','active')->orderBy('packing','ASC')->get();

		    if ($param == 'product') {
				$this->layout->content = View::make('partials.search-product', array(
					'items' => $items,
					'supplier' => $supplier,
					'gudang' => $gudang,
					'kategoriBarang' => $kategoriBarang,
					'kategoriPacking' => $kategoriPacking,
				))->withTitle('Product');
			}else if($param == 'adjustment') {
				$this->layout->content = View::make('partials.search-adjustment', array(
					'items' => $items,
					'supplier' => $supplier,
					'gudang' => $gudang,
					'kategoriBarang' => $kategoriBarang,
					'kategoriPacking' => $kategoriPacking,
				))->withTitle('Adjustment');
			} else if($param == 'faktur.tanggal_tiba') {
				$items = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
							    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
							    ->Join('supplier','faktur.supplier_id','=','supplier.id')
							    ->Join('gudang','detil_produk.gudang_id','=','gudang.id')
							    ->whereBetween($search, Input::get('from'), Input::get('to'))
							    ->groupBy('produk.prodid')
							    ->orderBy($search, 'ASC')
							    ->Get();
				$this->layout->content = View::make('partials.search-adjustment', array(
					'items' => $items,
					'supplier' => $supplier,
					'gudang' => $gudang,
					'kategoriBarang' => $kategoriBarang,
					'kategoriPacking' => $kategoriPacking,
				))->withTitle('Adjustment');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

    public function doAddProdukBaru() 
	{
		if (Session::has('user_id'))
		{
			$rules = array(
				'invoice_no' => 'required',
				'supplier' => 'required',
				'product_code' => 'required',
				'product_name' => 'required',
				'packing' => 'required',
				'storage' => 'required',
				'invoice_date' => 'required',
				'arrival_date' => 'required'
			);

			$validation = Validator::make(Input::all(),$rules);
			if ($validation->fails()) {
				return Redirect::to('/master/product')->withErrors($validation);
			}
			else
			{
				$faktur = new Faktur;
				$supplier = explode('|', Input::get('supplier'));
				$faktur->no_faktur = Input::get('invoice_no');
				$faktur->tanggal_faktur = Input::get('invoice_date');
				$faktur->tanggal_tiba = Input::get('arrival_date');
				$faktur->supplier_id = $supplier[0];
				$faktur->save();

				for($i = 0; $i < count(Input::get('bruto')); $i++)
				{
					$total_bruto[$i] = (double)Input::get('bruto')[$i] * (int)Input::get('qty')[$i];
					$total_tara[$i] = (double)Input::get('tara') * (int)Input::get('qty')[$i];
					$netto[$i] = (double)$total_bruto[$i] - (double)$total_tara[$i];

					$produk = new Produk;
					$produk->kode_produk = Input::get('product_code');
					$produk->nama_produk = Input::get('product_name');
					$produk->kategori_packing = Input::get('packing');
					$produk->bruto_faktur = Input::get('bruto')[$i];
					$produk->bruto_asli = Input::get('bruto')[$i];
					$produk->tara_faktur = Input::get('tara');
					$produk->tara_asli = Input::get('tara');
					$produk->total_bruto_faktur = $total_bruto[$i];
					$produk->total_bruto_asli = $total_bruto[$i];
					$produk->total_tara_faktur = $total_tara[$i];
					$produk->total_tara_asli = $total_tara[$i];
					$produk->penyusutan = '0';
					$produk->penambahan = '0';
					$produk->netto_faktur = $netto[$i];
					$produk->netto_asli = $netto[$i];
					$produk->gudang = Input::get('storage');
					$produk->keterangan = Input::get('notes');
					$produk->save();

					$meta = produk::orderBy('prodid','DESC')->first();

					$detil_produk = new detilProduk;
					$detil_produk->no_faktur = Input::get('invoice_no');
					$detil_produk->meta_id = $meta->prodid;
					$detil_produk->kode_produk = Input::get('product_code');
					$detil_produk->quantity = Input::get('qty')[$i];
					$detil_produk->quantity_faktur = Input::get('qty')[$i];
					$detil_produk->quantity_sisa = Input::get('qty')[$i];
					$detil_produk->gudang_id = Input::get('storage');
					$detil_produk->harga_faktur = Input::get('harga')[$i];
					$detil_produk->harga_asli = Input::get('harga')[$i];
					$detil_produk->save();
				}

				return Redirect::to('/master/product')->with('message','New Product Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}

	}

	public function doAddProdukExisting() 
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'invoice_no' => 'required',
			'supplier' => 'required',
			'product_code' => 'required',
			'product_name' => 'required',
			'packing' => 'required',
			'storage' => 'required',
			'invoice_date' => 'required',
			'arrival_date' => 'required'
		);

		$validation = Validator::make(Input::all(),$rules);
		if ($validation->fails()) {
			return Redirect::to('/master/product')->withErrors($validation);
		}
		else
		{
			$faktur = new Faktur;
			$faktur->no_faktur = Input::get('invoice_no');
			$faktur->tanggal_faktur = Input::get('invoice_date');
			$faktur->tanggal_tiba = Input::get('arrival_date');
			$faktur->supplier_id = Input::get('supplier');
			$faktur->save();

			for($i = 0; $i < count(Input::get('bruto')); $i++)
			{
				$total_bruto[$i] = (double)Input::get('bruto')[$i] * (int)Input::get('qty')[$i];
				$total_tara[$i] = (double)Input::get('tara') * (int)Input::get('qty')[$i];
				$netto[$i] = (double)$total_bruto[$i] - (double)$total_tara[$i];

				$produk = new Produk;
				$produk->kode_produk = Input::get('product_code');
				$produk->nama_produk = Input::get('product_name');
				$produk->kategori_packing = Input::get('packing');
				$produk->bruto_faktur = Input::get('bruto')[$i];
				$produk->bruto_asli = Input::get('bruto')[$i];
				$produk->tara_faktur = Input::get('tara');
				$produk->tara_asli = Input::get('tara');
				$produk->total_bruto_faktur = $total_bruto[$i];
				$produk->total_bruto_asli = $total_bruto[$i];
				$produk->total_tara_faktur = $total_tara[$i];
				$produk->total_tara_asli = $total_tara[$i];
				$produk->penyusutan = '0';
				$produk->penambahan = '0';
				$produk->netto_faktur = $netto[$i];
				$produk->netto_asli = $netto[$i];
				$produk->gudang = Input::get('storage');
				$produk->keterangan = Input::get('notes');
				$produk->save();

				$meta = produk::orderBy('prodid','DESC')->first();

				$detil_produk = new detilProduk;
				$detil_produk->no_faktur = Input::get('invoice_no');
				$detil_produk->meta_id = $meta->prodid;
				$detil_produk->kode_produk = Input::get('product_code');
				$detil_produk->quantity = Input::get('qty')[$i];
				$detil_produk->quantity_faktur = Input::get('qty')[$i];
				$detil_produk->quantity_sisa = Input::get('qty')[$i];
				$detil_produk->harga_faktur = Input::get('harga')[$i];
				$detil_produk->harga_asli = Input::get('harga')[$i];
				$detil_produk->gudang_id = Input::get('storage');
				$detil_produk->save();
			}
			return Redirect::to('/master/product')->with('message','New Product Added.');
		}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}

	}

	public function getUpdateProduk($prodid)
	{
		if (Session::has('user_id'))
		{
			//get items
		    $items = detilProduk::Join('produk','detil_produk.kode_produk','=','produk.kode_produk')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','detil_produk.gudang_id','=','gudang.id')
		    ->where('detil_produk.meta_id', '=' ,$prodid)
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->first();


			$this->layout->content = View::make('partials.editproduk', array(
				'items' => $items
			))->withTitle('Edit Product');

		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doDeleteProduk($id)
	{
		if (Session::has('user_id'))
		{
			// Delete Product
			$product = DB::table('produk')->where('prodid', '=', $id)
						->delete();

			// Get productDetail to be deleted
			$productDtl = DB::table('detil_produk')
							->where('meta_id', '=', $id)
							->where('no_faktur', '!=', '0000');		
			$no_faktur =  $productDtl->first()->no_faktur;
			$productDtl->delete();

			// Check detil_produk with the same faktur
			$faktur = DB::table('detil_produk')
						->where('no_faktur', '=', $no_faktur)
						->get();

			if(!$faktur) {
				// Delete if faktur is Null
				$faktur = DB::table('faktur')
							->where('faktur.no_faktur', '=', $no_faktur)
							->delete();
			}

			return Redirect::to('/master/product')->with('message','Product succesfully Removed.');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateProduk($prodid)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'tara' => 'required|numeric',
			'bruto' => 'required|numeric',
			'qty' => 'required|numeric'
			);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				$total_bruto = (double)Input::get('bruto') * (int)Input::get('qty');
				$total_tara = (double)Input::get('tara') * (int)Input::get('qty');
				$netto = (double)$total_bruto - (double)$total_tara;

				$produk = produk::where('prodid','=',$prodid)->update(array(
					'tara_asli' => Input::get('tara'),
					'bruto_asli' => Input::get('bruto'),
					'total_tara_asli' => $total_tara,
					'total_bruto_asli' => $total_bruto,
					'netto_asli' => $netto,
					'keterangan' => nl2br(Input::get('notes'))
					));

				$data = detilProduk::where('meta_id','=',$prodid)
				->where('kode_produk','!=','PJY%')
				->first();

				if ($data->quantity_sisa != $data->quantity) {
					return Redirect::back()->withErrors('You cannot change the quantity, because quantity in stock management has changed');
				}
				else{
				
				$detil_produk = detilProduk::where('meta_id','=',$prodid)
				->where('kode_produk','!=','PJY%')
				->update(array(
					'quantity' => Input::get('qty'),
					'quantity_faktur' => Input::get('qty'),
					'quantity_sisa' => Input::get('qty'),
					'harga_faktur' => Input::get('harga'),
					'harga_asli' => Input::get('harga')
					));
				}

				return Redirect::to('/master/product')->with('message','Successfully Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getProductCategoryName() {
		if (Session::has('user_id'))
		{
			$data = productCategoryName::orderBy('name','ASC')->Paginate(10);
			$this->layout->content = View::make('partials.productcategoryname',compact('data'))->withTitle('Product Category Name');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doAddProductCategoryName() {
		if (Session::has('user_id'))
		{
			$rules = array(
				'name' => 'required|unique:product_category_name'
			);

			$validator = Validator::make(Input::all(),$rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				//save ke DB
				$productCategoryName = new productCategoryName;
				$productCategoryName->name = Input::get('name');
				$productCategoryName->save();

				return Redirect::to('/master/product-category-name')->with('message','New Product Category Name Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getEditProductCategoryName($id){
		if (Session::has('user_id'))
		{
			$productCategoryName = productCategoryName::findorFail($id);
			$this->layout->content = View::make('partials.editProductCategoryName',compact('productCategoryName'))
			->withTitle('Update Product Category Name');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
	public function doUpdateProductCategoryName($id){
		if (Session::has('user_id'))
		{
			$rules = array('name' => 'required');
			$val = Validator::make(Input::all(), $rules);
			if ($val->fails()) {
				return Redirect::back()->withErrors($val);
			}
			else {
				$i = productCategoryName::findorFail($id);
				$i->name = Input::get('name');
				$i->status = Input::get('status');
				$i->save();

				return Redirect::to('/master/product-category-name')->with('message','New Product Category Name has been edited.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getSupplier()
	{	
		if (Session::has('user_id'))
		{
			$supplier = Supplier::orderBy('kodesupplier','ASC')->Paginate(10);
			$this->layout->content = View::make('partials.supplier',compact('supplier'))->withTitle('Data Supplier');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getUpdateSupplier($id)
	{
		if (Session::has('user_id'))
		{
			$supplier = Supplier::findorFail($id);
			$this->layout->content = View::make('partials.editsupplier',compact('supplier', array('id' => $supplier)))
			->withTitle('Update Supplier');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateSupplier($id)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Supplier_Code' => 'required|alpha',
			'Supplier_Name' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				$supplier = Supplier::findorFail($id);
				$supplier->kodesupplier = Input::get('Supplier_Code');
				$supplier->namasupplier = Input::get('Supplier_Name');
				$supplier->alamat = Input::get('Supplier_Address');
				$supplier->no_rek = Input::get('no_rek');
				$supplier->notelepon = Input::get('Supplier_Phone');
				$supplier->status = Input::get('status');
				$supplier->save();

				return Redirect::to('/master/supplier')->with('message','Successfully Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doAddSupplier() 
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Supplier_Code' => 'required|alpha',
			'Supplier_Name' => 'required'
			);

			$validator = Validator::make(Input::all(),$rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				//save ke DB
				$supplier = new Supplier;
				$supplier->kodesupplier = Input::get('Supplier_Code');
				$supplier->namasupplier = Input::get('Supplier_Name');
				$supplier->alamat = Input::get('Supplier_Address');
				$supplier->no_rek = Input::get('no_rek');
				$supplier->fax_no = Input::get('Supplier_Fax');
				$supplier->notelepon = Input::get('Supplier_Phone');
				$supplier->save();

				return Redirect::to('/master/supplier')->with('message','New Supplier Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function searchSupplier() {
		if (Session::has('user_id'))
		{
		    $search = Input::get('search');
		    $keyword = Input::get('keyword');
	    	$supplier = Supplier::where($search, 'like', '%'.$keyword.'%')
		    			->orderBy('kodesupplier','ASC')->Paginate(10);

			$this->layout->content = View::make('partials.supplier', compact('supplier'))->withTitle('Search Result: Supplier');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getPackageForm() {
		if (Session::has('user_id'))
		{
			//VIEW TABLE
		    //get items
		    $items = detilProduk::Join('produk','produk.prodid','=','detil_produk.meta_id')
		    ->where('detil_produk.quantity_sisa','!=',0)
		    ->orderBy('nama_produk', 'ASC')
			->get();

		    $this->layout->content = View::make('partials.package', array(
				'items' => $items
			))->withTitle('Create Package Form');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doCreatePackage() {
		if (Session::has('user_id'))
		{
			$rules = array('packing_cost' => 'required|numeric');
			$val = Validator::make(Input::all(),$rules);
			if ($val->fails()) {
				return Redirect::back()->withErrors($val);
			}
			else {
				$id = Input::get('id');
				$dus = Input::get('packing_cost');
				$qty = array();
				$price = array();

				foreach ($id as $value) {
					if(Input::get('qty')[$value] != 0 && Input::get('price')[$value] != 0) {
						$qty[$value] = Input::get('qty')[$value];
						$price[$value] = Input::get('price')[$value];
	
						//Cek inputan quantity, jika lebih dari yg tersedia di stok -> show Error
						$fetch = detilProduk::where('id', '=', $value)->first();
						if ($qty[$value] > $fetch->quantity_sisa) {
							return Redirect::back()
							->withErrors('Quantity input should be lower than Stock Available.');
						}
						else if($qty[$value] <= 0) {
							return Redirect::back()
							->withErrors('Quantity input must be greater than 0');
						}
					}
					else {
						continue;
					}
				}

				//save ke session
				Session::put("id", $id);
				Session::put("dus", $dus);
				Session::put("qty", $qty);
				Session::put("price", $price);
				
				return Redirect::to('/master/packages/form');
			}	
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getPackages() {
		if (Session::has('user_id'))
		{
			$this->layout->content = View::make('partials.packageForm')->withTitle('Create Package Form');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function addPackage() {
		if (Session::has('user_id'))
		{	
			
			$data = Session::get("id");
			
			foreach ($data as $id) {
				$fetch = detilProduk::where('id', '=', $id)
				->where('kode_produk','!=','PJY%')
				->first();
				
				$newQty = (int)$fetch->quantity_sisa - (int)Session::get('qty')[$id];

				//update quantity sisa
				$fetchUpdate = detilProduk::where('id', '=', $id)
				->where('kode_produk','!=','PJY%')
				->update(array('quantity_sisa' => $newQty));
			}
			
			$totalNettoQty = 0; $boxPrice = 0;
			for ($i=0; $i < count(Input::get('netto')); $i++) { 
				if(Input::get('netto')[$i] != '') {
					if(is_numeric(Input::get('netto')[$i]) && is_numeric(Input::get('qty')[$i])) {
						$totalNettoQty += Input::get('netto')[$i] * Input::get('qty')[$i];
						$boxPrice += (int)Input::get('dus') * (int) Input::get('qty')[$i];
					}
				}				
			}
			$hargaModal = intval(ceil((Input::get('totalPrice') + $boxPrice) / $totalNettoQty));
			for($i = 0; $i < count(Input::get('netto')); $i++)
			{
				if(Input::get('netto')[$i] != ''){
					if(is_numeric(Input::get('netto')[$i]) && is_numeric(Input::get('qty')[$i]) ){
						$newPackage = new packages();
						$newPackage->package_id = Input::get('package_id');
						$newPackage->netto = Input::get('netto')[$i];
						$newPackage->packing_id = Input::get('packing');
						$newPackage->save();	

						$package = packages::where('package_id', '=', Input::get('package_id'))
									->orderBy('id', 'DESC')
									->first();

						$newDetilProduk = new detilProduk();
						$newDetilProduk->meta_id = $package->id;
						$newDetilProduk->no_faktur = '0000';
						$newDetilProduk->kode_produk = Input::get('package_id');
						$newDetilProduk->quantity = Input::get('qty')[$i];
						$newDetilProduk->quantity_faktur = Input::get('qty')[$i];
						$newDetilProduk->quantity_sisa = Input::get('qty')[$i];
						$newDetilProduk->harga_faktur = $hargaModal;
						$newDetilProduk->harga_asli = $hargaModal;
						$newDetilProduk->gudang_id = Input::get('storage');
						$newDetilProduk->save();
					}
				}
			}

			return Redirect::to('/master/packages')->with('message','Package '.Input::get('package_id').' created.');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getPrintPackage($package_id){
		if (Session::has('user_id'))
		{
			return View::make('partials.printPackage',array('package_id' => $package_id));
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getDiscount()
	{
		if (Session::has('user_id'))
		{
			$discount = Discount::orderBy('discount_rate','ASC')->Paginate(10);
			$this->layout->content = View::make('partials.discount',compact('discount'))->withTitle('Data Discount');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getUpdateDiscount($id)
	{
		if (Session::has('user_id'))
		{
			$discount = Discount::findorFail($id);
			$this->layout->content = View::make('partials.editdiscount',compact('discount', array('id' => $discount)))
			->withTitle('Update Discount');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateDiscount($id)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Discount_Rate' => 'required|numeric',
			'Description' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				$discount = Discount::findorFail($id);
				$discount->discount_rate = Input::get('Discount_Rate');
				$discount->keterangan = nl2br(Input::get('Description'));
				$discount->status = Input::get('status');
				$discount->save();

				return Redirect::to('/master/discount')->with('message','Successfully Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doAddDiscount() 
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Discount_Rate' => 'required|numeric',
			'Description' => 'required' 
			);

			$validator = Validator::make(Input::all(),$rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				//save ke DB
				$discount = new Discount;
				$discount->discount_rate = Input::get('Discount_Rate');
				$discount->keterangan = nl2br(Input::get('Description'));
				$discount->save();

				return Redirect::to('/master/discount')->with('message','New Discount Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getKategoriBarang()
	{
		if (Session::has('user_id'))
		{
			$kategoriBarang = kategoriBarang::orderBy('kode_kategori_barang','ASC')->Paginate(10);
			$this->layout->content = View::make('partials.kategoriBarang',compact('kategoriBarang'))
			->withTitle('Data Kategori Barang');
			}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getUpdateKategoriBarang($id)
	{	
		if (Session::has('user_id'))
		{
			$kategoriBarang = kategoriBarang::findorFail($id);
			$productCategoryNames = productCategoryName::where('status', '=', 'active')
									->orderBy('name')
									->get();
			$this->layout->content = View::make('partials.editKategoriBarang',compact('kategoriBarang', 'productCategoryNames'))
			->withTitle('Update Kategori Barang');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateKategoriBarang($id)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Product_Category_Code' => 'required',
			'Product_Category_Name' => 'required',
			'status' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator);
			}
			else
			{
				$kategoriBarang = kategoriBarang::findorFail($id);
				$kategoriBarang->kode_kategori_barang = Input::get('Product_Category_Code');
				$kategoriBarang->kategori_barang =  Input::get('Product_Category_Name');
				$kategoriBarang->status = Input::get('status');
				$kategoriBarang->save();

				return Redirect::to('/master/product-category')
				->with('message','Successfully Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doAddKategoriBarang()
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Product_Category_Code' => 'required',
			'Product_Category_Name' => 'required'
			);

			$validator =  Validator::make(Input::all(),$rules);

			if ($validator->fails()) {
				return Redirect::to('/master/product-category')->withErrors($validator)->withInput();
			}
			else
			{
				$data = new kategoriBarang;
				$data->kode_kategori_barang = Input::get('Product_Category_Code');
				$data->kategori_barang = Input::get('Product_Category_Name');
				$data->save();

				return Redirect::to('/master/product-category')->with('message','New Product Category Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getKategoriPacking()
	{
		if (Session::has('user_id'))
		{
			$kategoriPacking = kategoriPacking::orderBy('packing','ASC')->Paginate(10);
			$this->layout->content = View::make('partials.kategoriPacking',compact('kategoriPacking'))
			->withTitle('Data Kategori Packing');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getUpdateKategoriPacking($id)
	{
		if (Session::has('user_id'))
		{
			$kategoriPacking = kategoriPacking::findorFail($id);
			$this->layout->content = View::make('partials.editKategoriPacking',compact('kategoriPacking'))
			->withTitle('Update Kategori Packing');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateKategoriPacking($id)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Packing_Category' => 'required',
			'status' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator);
			}
			else
			{
				$kategoriPacking = kategoriPacking::findorFail($id);
				$kategoriPacking->packing =  Input::get('Packing_Category');
				$kategoriPacking->status = Input::get('status');
				$kategoriPacking->save();

				return Redirect::to('/master/packing-category')
				->with('message','Successfully Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doAddKategoriPacking()
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Packing_Category' => 'required'
			);

			$validator = Validator::make(Input::all(),$rules);

			if ($validator->fails()) {
				return Redirect::to('/master/packing-category')->withErrors($validator)->withInput();
			}
			else
			{
				$data = new kategoriPacking;
				$data->packing = Input::get('Packing_Category');
				$data->save();

				return Redirect::to('/master/packing-category')->with('message','New Packing Category Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getGudang()
	{
		if (Session::has('user_id'))
		{
			$gudang = Gudang::orderBy('no_gudang','ASC')->Paginate(10);
			$this->layout->content = View::make('partials.gudang',compact('gudang'))->withTitle('Data Gudang');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getUpdateGudang($id)
	{
		if (Session::has('user_id'))
		{
			$gudang = Gudang::findorFail($id);
			$this->layout->content = View::make('partials.editgudang',compact('gudang', array('id' => $gudang)))->withTitle('Update Gudang');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doUpdateGudang($id)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Storage_Number' => 'required',
			'Storage_Room' => 'required',
			'Storage_PIC' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				$gudang = Gudang::findorFail($id);
				$gudang->no_gudang = Input::get('Storage_Number');
				$gudang->kamar_gudang = Input::get('Storage_Room');
				$gudang->pic_gudang = Input::get('Storage_PIC');
				$gudang->status = Input::get('status');
				$gudang->save();

				return Redirect::to('/master/storage')->with('message','Successfully Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}


	public function doAddGudang() 
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'Storage_Number' => 'required',
			'Storage_Room' => 'required',
			'Storage_PIC' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {

				return Redirect::to('/master/storage')->withErrors($validator)->withInput();
			}
			else
			{
				//save ke DB
				$gudang = new Gudang;
				$gudang->no_gudang = Input::get('Storage_Number');
				$gudang->kamar_gudang = Input::get('Storage_Room');
				$gudang->pic_gudang = Input::get('Storage_PIC');
				$gudang->save();

				return Redirect::to('/master/storage')->with('message','New Storage Added.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
}