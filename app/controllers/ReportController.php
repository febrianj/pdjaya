<?php

class ReportController extends BaseController{
	public $restful = true;
	protected $layout = 'layout.master';

	public function getInventoryStock()
	{
		if (Session::has('user_id'))
		{
			$fetch = DB::table('ViewInventoryStock')
						->paginate(20);

			$this->layout->content = View::make('partials.inventorystock', array('items' => $fetch))->withTitle('Inventory Stock');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function searchInventoryStock() {
		if (Session::has('user_id'))
		{	

			$rules = array('filter' => 'required');
			$validation = Validator::make(Input::all(),$rules);

			if($validation->fails()) {
				return Redirect::to('/reporting/inventorystock')->withErrors($validation);
			}
			
			$column = '';
			switch (Input::get('filter')) {
				case '1':
					$column = 'namasupplier';
					break;
				case '2':
					$column = 'created_at';
					break;
				case '3':
					$column = 'gudang';
					break;
				case '4':
					$column = 'kodesupplier';
					break;
			}
			$fetch = DB::table('ViewInventoryStock')
						->where($column, 'LIKE', '%'.Input::get('value').'%')
						->paginate(20);

			if($column == 'created_at') {
				$fetch = DB::table('ViewInventoryStock')
							->whereBetween($column, array(Input::get('from'), Input::get('to')))
							->paginate(20);
			}

			$this->layout->content = View::make('partials.inventorystock', array('items' => $fetch))->withTitle('Inventory Stock');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getPurchaseReport()
	{
		if (Session::has('user_id'))
		{
			$report = PurchaseOrderReport::paginate(20);
			$this->layout->content = View::make('partials.purchasereport', array('report' => $report))->withTitle('Purchase Report');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getPurchaseReportDetail($year, $month) {
		if (Session::has('user_id'))
		{
			$purchaseOrder = PurchaseOrder::whereYear('created_at', '=', $year)
											->whereMonth('created_at', '=', $month)
											->paginate(20);
			$this->layout->content = View::make('partials.purchasereportdetail', array('purchaseOrder' => $purchaseOrder))->withTitle('Purchase Report Detail');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getSalesReport()
	{
		if (Session::has('user_id'))
		{
			$report = SalesOrderReport::paginate(20);
			$this->layout->content = View::make('partials.salesreport', array('report' => $report))->withTitle('Sales Report');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getSalesReportDetail($year, $month, $type)
	{
		if (Session::has('user_id'))
		{	
			$type = ($type == 'a') ? $type = '' : $type;
			$salesOrder = SalesOrder::where('payment_method', 'like', '%'.$type.'%')
									->whereMonth('created_at', '=', $month)
									->whereYear('created_at', '=', $year)
									->paginate(20);
			$this->layout->content = View::make('partials.salesreportdetail', array('salesOrder' => $salesOrder))->withTitle('Sales Report Detail');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
}