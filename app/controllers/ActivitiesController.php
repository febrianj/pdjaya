<?php
class ActivitiesController extends BaseController{
	public $restful = true;
	protected $layout = 'layout.master';

	public function getStockManagement()
	{
		if (Session::has('user_id'))
		{
			$gudang = Gudang::where('status','=','active')->orderBy('no_gudang','ASC')->get();
			$deliveryOrder = deliveryOrder::orderBy('delivery_id', 'DESC')
			->groupBy('delivery_id')
			->Paginate(20);
			
			$this->layout->content = View::make('partials.stockmanagement', array(
				'gudang' => $gudang,
				'deliveryOrder' => $deliveryOrder
			))->withTitle('Stock Management');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doStockManagement() 
	{
		if (Session::has('user_id'))
		{
				//save ke session
			Session::put("origin",Input::get('origin'));
			Session::put("destination", Input::get('destination'));

			return Redirect::to('/activities/createdeliveryform');
			
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getCreateDeliveryForm()
	{
		if (Session::has('user_id'))
		{
			$produk = DB::table('produk')
					->Join('detil_produk','produk.prodid','=','detil_produk.meta_id')
					->where('detil_produk.gudang_id','=',Session::get('origin'))
					->where('detil_produk.quantity_sisa','!=',0)
					->select('produk.kode_produk');

			$package = DB::table('packages')
					->Join('detil_produk','packages.id','=','detil_produk.meta_id')
					->where('detil_produk.gudang_id','=',Session::get('origin'))
					->where('detil_produk.quantity_sisa','!=',0)
					->select('packages.package_id');

			$item = $produk->union($package)->get();

			$this->layout->content = View::make('partials.createdeliveryform', array(
				'item' => $item
			))->withTitle('Create Delivery Form');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doCreateDeliveryForm() {
		if (Session::has('user_id'))
		{	
			for($i = 0; $i < count(Input::get('product')); $i++)
			{	
				if(Input::get('product')[$i] != '') {
					foreach (Input::get('weight')[$i] as $key) {						
						$data = detilProduk::where('meta_id', '=', $key)
						->where('kode_produk', '=', Input::get('product')[$i])
						->where('gudang_id', '=', Session::get('origin'))
						->first();
						$quantity_new = (int)$data->quantity_sisa - (int)Input::get('qty')[$i][$key];
						$fetch = detilProduk::where('gudang_id', '=', Session::get('destination'))
							->where('meta_id','=', $key)
							->where('kode_produk','=',Input::get('product')[$i])
							->first();

						// kalo uda ada di gudang tambahin, kalau ga ada buat baru
						if ($quantity_new != 0) {
							$detilProduk = detilProduk::where('meta_id', '=', $key)
							->where('kode_produk', '=', Input::get('product')[$i])
							->where('gudang_id','=',Session::get('origin'))
							->update(array(
								'quantity_sisa' => $quantity_new
							));
						} else {
							$detilProduk = detilProduk::where('meta_id', '=', $key)
							->where('kode_produk', '=', Input::get('product')[$i])
							->where('gudang_id','=',Session::get('origin'))
							->update(array(
								'quantity_sisa' => 0
							));
						}
						if($fetch == null) { 
							// data baru di destination
							$newDetilProduk = new detilProduk;
							$newDetilProduk->meta_id 		= $key;
							$newDetilProduk->no_faktur 		= $data->no_faktur;
							$newDetilProduk->kode_produk 	= $data->kode_produk;
							$newDetilProduk->quantity 		= $data->quantity;
							$newDetilProduk->quantity_faktur= $data->quantity_faktur;
							$newDetilProduk->quantity_sisa 	= Input::get('qty')[$i][$key];
							$newDetilProduk->harga_faktur 	= $data->harga_faktur;
							$newDetilProduk->harga_asli 	= $data->harga_asli;
							$newDetilProduk->gudang_id 		= Session::get('destination');
							$newDetilProduk->save();
						} else {
							// update data destination
							$fetchqty = (int)$fetch->quantity_sisa + (int)Input::get('qty')[$i][$key];
							$fetchUpdate = detilProduk::where('gudang_id', '=', Session::get('destination'))
							->where('meta_id','=', $key)
							->where('kode_produk','=',Input::get('product')[$i])
							->update(array('quantity_sisa' => $fetchqty));
						}

						$deliveryOrder = new deliveryOrder;
						$deliveryOrder->delivery_id = Input::get('delivery_id');
						$deliveryOrder->origin 		= Input::get('origin');
						$deliveryOrder->destination = Input::get('destination');
						$deliveryOrder->meta_id 	= $data->id;
						$deliveryOrder->quantity 	= Input::get('qty')[$i][$key];
						$deliveryOrder->save();
					}
				}
			}

			return Redirect::to('/activities/stockmanagement')->with('message','Delivery Order created.');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}	

	public function getPrintDeliveryForm($delivery_id)
	{
		if (Session::has('user_id'))
		{
			return View::make('partials.deliveryform',compact('delivery_id'))->withTitle('Delivery Form');		
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getPurchaseOrder()
	{
		if (Session::has('user_id'))
		{
			$this->layout->content = View::make('partials.purchaseorder')->withTitle('Purchase Order');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
	public function getPurchaseOrderDetail($id) 
	{
		if (Session::has('user_id'))
		{
			$purchaseOrder 		  = PurchaseOrder::whereId($id)
									->first();
			$purchaseOrderItem 	  = PurchaseOrderItem::where('purchase_order_id', '=', $purchaseOrder->id)
									->get();
			$itemId 			  = PurchaseOrderItem::where('purchase_order_id', '=', $purchaseOrder->id)
									->lists('id');
			$purchaseOrderItemDtl = PurchaseOrderItemDetail::whereIn('purchase_order_item_id', $itemId)
									->get();
			$additionalCost 	  = AdditionalCost::where('ref_id', '=', $purchaseOrder->po_id)->get();
			$itemSubtotal = 0;
			$fetchDtl = [];

			foreach ($purchaseOrderItem as $item) {
				$fetchDtl[$item->id] = [];
				foreach ($purchaseOrderItemDtl as $dtl) {
					if($dtl->purchase_order_item_id == $item->id) {
						array_push($fetchDtl[$item->id], $dtl);
						$itemSubtotal += $dtl->netto * $dtl->quantity *$dtl->price;
					}
				}
			}
			$this->layout->content = View::make('partials.purchaseorderdetail', 
				array(	'purchaseOrder'		   => $purchaseOrder, 
						'purchaseOrderItem'    => $purchaseOrderItem, 
						'purchaseOrderItemDtl' => $fetchDtl, 
						'itemSubtotal'		   => $itemSubtotal,
						'additionalCost' 	   => $additionalCost)
				)->withTitle('Purchase Order');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doPurchaseOrder()
	{
		if (Session::has('user_id'))
		{
			$rules = array('supplier' => 'required','invoice_id' => 'required');
			$val = Validator::make(Input::all(), $rules);

			if ($val->fails()) {
				return Redirect::back()->withErrors($val);
			}
			else {
				Session::put('supplier_id',Input::get('supplier'));
				Session::put('invoice_id',Input::get('invoice_id'));

				return Redirect::to('/activities/purchaseorder/productList');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	// INSERT PURCHASE ORDER
	public function addPurchaseOrder() {
		if (Session::has('user_id')) {
			$no_po 		   = Input::get('no_po');
			$no_faktur 	   = Input::get('no_faktur');
			$nama_supplier = Input::get('nama_supplier');
			$notes 		   = Input::get('notes');
			
			$faktur = Faktur::where('no_faktur', '=', $no_faktur)->first();
			$faktur->invoice_status = 'Paid';
			$faktur->save();

			$newPO = new PurchaseOrder;
			$newPO->po_id 		 = $no_po;
			$newPO->no_faktur 	 = $no_faktur;
			$newPO->namasupplier = $nama_supplier;
			$newPO->notes 		 = $notes;
			$newPO->total 		 = Input::get('total');
			$newPO->save();

			// INSERT PO ITEMS
			for($i = 0; $i < count(Input::get('item')); $i++) {
				$item 	= Input::get('item')[$i];
				$weight = Input::get('weight')[$i];
				$tara 	= Input::get('tara')[$i];
				$qty 	= Input::get('qty')[$i];
				
				$newPOItem = new PurchaseOrderItem;
				$newPOItem->purchase_order_id = $newPO->id;
				$newPOItem->kode_produk = $item;
				$newPOItem->weight 		= $weight;
				$newPOItem->tara 		= $tara;
				$newPOItem->quantity 	= $qty;
				$newPOItem->save();

				// INSERT PO ITEM DETAILS
				for($j = 0; $j < count(Input::get('qty_input')[$i]); $j++) {
					$qty_input = Input::get('qty_input')[$i][$j];
					$price     = Input::get('price')[$i][$j];
					
					$newPOItemDtl = new PurchaseOrderItemDetail;
					$newPOItemDtl->purchase_order_item_id = $newPOItem->id;
					$newPOItemDtl->netto 	= $weight-$tara;
					$newPOItemDtl->quantity = $qty_input;
					$newPOItemDtl->price 	= $price;
					$newPOItemDtl->save();
				}

			}
			
			// INSERT ADDITIONAL PRICE DETAIL
			for($i = 0; $i < count(Input::get('additional_desc')); $i++) {
				$add_desc = Input::get('additional_desc')[$i];
				$add_price= Input::get('additional_price')[$i];
				
				$addCost = new AdditionalCost;
				$addCost->ref_id = $newPO->po_id;
				$addCost->desc = $add_desc;
				$addCost->price = $add_price;
				$addCost->save();
			}
			return Redirect::to('/activities/purchaseorder')->with('message','Purchase Order created.');
		} else {
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
	public function doPayPurchaseOrder($id) {
		if (Session::has('user_id')) {
			$purchaseOrder = PurchaseOrder::whereId($id)->first();
			$purchaseOrder->status = "Paid";
			$purchaseOrder->save();
			return Redirect::to('/activities/purchaseorder')->with('message','Purchase Order Payment Updated.');
		} else {
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getProductList()
	{
		if (Session::has('user_id'))
		{
			$this->layout->content = View::make('partials.purchaseorder-productList')->withTitle('Purchase Order');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	// Get Sales Order Page
	public function getSalesOrder()
	{
		if (Session::has('user_id'))
		{
			$products = DB::table('detil_produk')
					->where('quantity_sisa', '!=', '0')
					->select('id', 'kode_produk')
					->groupBy('kode_produk')
					->get();				
			$discounts = DB::table('discount')
						->where('status', '=', 'Active')
						->select('id', 'discount_rate')
						->get();
			$salesOrder = DB::table('sales_order')
							->paginate(20);
			$customers = Customer::orderBy('name')
							->get();
			$this->layout->content = View::make('partials.salesorder', array('salesOrder' => $salesOrder,'products' => $products, 'discounts' => $discounts, 'customers' => $customers))->withTitle('Sales Order');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	// Insert Sales Order
	public function addSalesOrder() {
		if (Session::has('user_id'))
		{
			$rules = array(
				'payment' => 'required',
				'cust_id' => 'required'
				);
			$validation = Validator::make(Input::all(), $rules);
			if ($validation->fails()) {
				return Redirect::to('/activities/salesorder')->withErrors($validation);
			}
			// Sales Order Header
			$dtlId 		= Input::get('dtlid');
			$so_id 		= Input::get('no_so');
			$payment 	= 'Cash';
			$customer_id 	= Input::get('cust_id');
			$address 	= Input::get('address');
			$subtotal 	= Input::get('subtotal');
			$discount 	= Input::get('discount');
			$grandtotal	= Input::get('grandtotal');

			// Sales Order Item
			$prod_codes	= Input::get('prod_code');
			
			// Sales Order Item Detail
			$brutos		= Input::get('bruto');
			$taras 		= Input::get('tara');
			$qtys 		= Input::get('qty');
			$nettos		= Input::get('netto');
			$prices		= Input::get('price');
			$totals		= Input::get('total');
			
			// AdditionalCost
			$addDesc 	= Input::get('additional_desc');
			$addPrice 	= Input::get('additional_price');
			

			if (Input::get('payment') == 'CR') 
				$payment = 'Credit';

			// INSERT SALES ORDER
			$salesOrder = new SalesOrder;
			$salesOrder->so_id 			= $so_id;
			$salesOrder->payment_method = $payment;
			$salesOrder->customer_id	= $customer_id;
			$salesOrder->subtotal 		= $subtotal;
			$salesOrder->discount 		= $discount;
			$salesOrder->total 			= $grandtotal;
			$salesOrder->save();

			// INSERT SALES ORDER ITEM
			foreach ($prod_codes as $key => $prod_code) {				
				$salesOrderItem = new SalesOrderItem;
				$salesOrderItem->sales_order_id = $salesOrder->id;
				$salesOrderItem->product_code 	= $prod_code;
				$salesOrderItem->save();

				// INSERT SALESORDERITEMDETAIL
				for($i = 0; $i < count($brutos[$key]); $i++) {

					$dtlItem = detilProduk::findorFail($dtlId[$key][$i]);

					$dtlItem->quantity_sisa = $dtlItem->quantity_sisa - $qtys[$key][$i];
					$dtlItem->save();

					$salesOrderItemDtl = new SalesOrderItemDetail;
					$salesOrderItemDtl->sales_order_item_id = $salesOrderItem->id;
					$salesOrderItemDtl->bruto 	 = $brutos[$key][$i];
					$salesOrderItemDtl->tara 	 = $taras[$key][$i];
					$salesOrderItemDtl->quantity = $qtys[$key][$i];
					$salesOrderItemDtl->netto 	 = $nettos[$key][$i];
					$salesOrderItemDtl->price 	 = $prices[$key][$i];
					$salesOrderItemDtl->total 	 = $totals[$key][$i];
					$salesOrderItemDtl->save();
				}
			}

			for ($i=0; $i < count($addDesc); $i++) { 
				$additionalCost = new AdditionalCost;
				$additionalCost->ref_id = $so_id;
				$additionalCost->desc 	= $addDesc[$i];
				$additionalCost->price 	= $addPrice[$i];
				$additionalCost->save();
			}
			return Redirect::to('/activities/salesorder')->with('message','Sales Order created.');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getSalesOrderDetail($id, $type) {
		if(Session::has('user_id'))
		{
			$page = 'salesOrder'.$type;
			$salesOrder = SalesOrder::Join('customer', 'customer.id', '=', 'sales_order.customer_id')
							->where('sales_order.id', '=', $id)
							->select('so_id', 'customer.name', 'payment_method', 'subtotal', 'total', 'discount', 'sales_order.created_at', 'customer.address')
							->first();
			$additionalCost = AdditionalCost::where('ref_id', '=', $salesOrder->so_id)
								->get();
			$salesOrderItem = SalesOrderItem::where('sales_order_id', '=', $id)
								->get();
			$itemId = SalesOrderItem::where('sales_order_id', '=', $id)
						->lists('id');
			$salesOrderItemDtl = SalesOrderItemDetail::whereIn('sales_order_item_id', $itemId)
								->get();
			$fetchDtl = [];
			foreach ($salesOrderItem as $item) {
				$fetchDtl[$item->id] = [];
				foreach ($salesOrderItemDtl as $dtl) {
					if($dtl->sales_order_item_id == $item->id) {
						array_push($fetchDtl[$item->id], $dtl);
					}
				}
			}
			return View::make("partials.".$page, array('salesOrder' => $salesOrder, 'additionalCost' => $additionalCost, 'salesOrderItem' => $salesOrderItem, 'salesOrderItemDtl' => $fetchDtl))->withTitle('Sales Order '.$type);
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getAdjustmentData() 
	{
		if(Session::has('user_id'))
		{
			$items = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','detil_produk.gudang_id','=','gudang.id')
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->Paginate(20);

		    $today = date('Y-m-d');

		    $recents = Produk::Join('detil_produk','detil_produk.meta_id','=','produk.prodid')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','detil_produk.gudang_id','=','gudang.id')
		    ->where('produk.created_at', '>=', $today)
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->Paginate(20);
			$this->layout->content = View::make("partials.adjustment", compact('items', 'recents'))->withTitle('Adjustment');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getEditAdjustment($prodid) {
		if(Session::has('user_id'))
		{
			$items = detilProduk::Join('produk','detil_produk.kode_produk','=','produk.kode_produk')
		    ->Join('faktur','detil_produk.no_faktur','=','faktur.no_faktur')
		    ->Join('supplier','faktur.supplier_id','=','supplier.id')
		    ->Join('gudang','detil_produk.gudang_id','=','gudang.id')
		    ->where('detil_produk.meta_id', '=' ,$prodid)
		    ->groupBy('produk.prodid')
		    ->orderBy('produk.prodid','DESC')
		    ->first();


			$this->layout->content = View::make('partials.editAdjustment', array(
				'items' => $items
			))->withTitle('Edit Adjustment Data');		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doEditAdjustment($prodid)
	{
		if (Session::has('user_id'))
		{
			$rules = array(
			'tara' => 'required|numeric',
			'bruto' => 'required|numeric',
			'qty' => 'required|numeric',
			'depreciation' => 'required|numeric',
			'addition' => 'required|numeric'
			);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else
			{
				$total_bruto = (double)Input::get('bruto') * (int)Input::get('qty');
				$total_tara = (double)Input::get('tara') * (int)Input::get('qty');
				$netto = (double)$total_bruto - (double)$total_tara - (double)Input::get('depreciation') + (double)Input::get('addition');
				$produk = produk::where('prodid','=',$prodid)->update(array(
					'tara_faktur' => Input::get('tara'),
					'bruto_faktur' => Input::get('bruto'),
					'total_tara_faktur' => $total_tara,
					'total_bruto_faktur' => $total_bruto,
					'netto_faktur' =>$netto,
					'penyusutan' => Input::get('depreciation'),
					'penambahan' => Input::get('addition'),
					'keterangan' => nl2br(Input::get('notes'))
					));

				$data = detilProduk::where('meta_id','=',$prodid)
				->where('kode_produk','!=','PJY%')
				->first();

				if ($data->quantity_sisa != $data->quantity) {
					return Redirect::back()->withErrors('You cannot change the quantity, because quantity in stock management has changed');
				}
				else {
					$detil_produk = detilProduk::where('meta_id','=',$prodid)
					->where('kode_produk','!=','PJY%')
					->update(array(
						'quantity_faktur' => Input::get('qty'),
						'harga_faktur' => Input::get('harga_faktur')
					));
				}
				return Redirect::to('/activities/adjustment')->with('message','Successfully Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
}