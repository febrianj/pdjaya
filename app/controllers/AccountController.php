<?php
class AccountController extends BaseController{
	public $restful = true;
	protected $layout = 'layout.master';

	public function getAccount()
	{
		$users = User::orderBy('userfirstname','ASC')->Paginate(10);
		return View::make('partials.account',compact('users'));
	}

	public function doAddAccount()
	{
		$rules = array(
			'firstname' => 'required',
			'phone' => 'numeric',
			'username' => 'required',
			'password' => 'required',
			'role' => 'required'
			);

		$val = Validator::make(Input::all(), $rules);
		if ($val->fails()) {
			return Redirect::back()->withErrors($val);
		}
		else
		{
			$user = new User;
			$user->userfirstname = Input::get('firstname');
			$user->userlastname = Input::get('lastname');
			$user->userphone = Input::get('phone');
			$user->useremail = Input::get('email');
			$user->username = Input::get('username');
			$user->password = Hash::make(Input::get('password'));
			$user->role = Input::get('role');
			$user->save();

			return Redirect::back()->with('message','New Account added.');
		}
	}

	public function getEditAccount($id)
	{
		$user = User::findorFail($id);
		return View::make('partials.editaccount',compact('user'));
	}

	public function doEditAccount($id)
	{
		$rules = array(
			'firstname' => 'required',
			'phone' => 'numeric',
			'username' => 'required',
			'role' => 'required'
			);

		$val = Validator::make(Input::all(),$rules);
		if ($val->fails()) {
			return Redirect::back()->withErrors($val);
		}
		else
		{
			$user = User::findorFail($id);
			$user->userfirstname = Input::get('firstname');
			$user->userlastname = Input::get('lastname');
			$user->userphone = Input::get('phone');
			$user->useremail = Input::get('email');
			$user->username = Input::get('username');
			$user->role = Input::get('role');
			$user->status = Input::get('status');
			$user->save();

			return Redirect::to('/account')->with('message', 'Successfully Updated.');
		}
	}

	public function getProfile()
	{
		if (Session::has('user_id'))
		{
			$users = User::where('id','=',Session::get('user_id'))->first();
			$this->layout->content = View::make('partials.profile',compact('users'))->withTitle("Profile");
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getEditProfile()
	{
		if (Session::has('user_id'))
		{
			$user = User::findorFail(Session::get('user_id'));
			$this->layout->content = View::make('partials.editprofile',compact('user'))->withTitle('Edit Profile');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function doEditProfile()
	{
		if (Session::has('user_id'))
		{
			$rules = array(
				'firstname' => 'required',
				'phone' => 'numeric',
				'username' => 'required'
				);

			$val = Validator::make(Input::all(), $rules);

			if ($val->fails()) {
				return Redirect::back()->withErrors($val);
			}
			else
			{
				$user = User::findorFail(Session::get('user_id'));
				$user->userfirstname = Input::get('firstname');
				$user->userlastname = Input::get('lastname');
				$user->userphone = Input::get('phone');
				$user->useremail = Input::get('email');
				$user->username = Input::get('username');
				$user->save();

				return Redirect::to('/profile')->with('message','Profile Updated.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}

	public function getChangePassword()
	{
		if (Session::has('user_id'))
		{
			$user = User::findorFail(Session::get('user_id'));
			$this->layout->content = View::make('partials.changepassword',compact('user'))->withTitle('Change Password');
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}	

	public function doChangePassword()
	{
		if (Session::has('user_id'))
		{
			$rules = array(
				'new_password' => 'required|min:6',
				'confirm_password' => 'required|same:new_password'
				);
			$val = Validator::make(Input::all(),$rules);
			if ($val->fails()) {
				return Redirect::back()->withErrors($val);
			}
			else
			{
				$user = User::findorFail(Session::get('user_id'));
				$user->password = Hash::make(Input::get('new_password'));
				$user->save();

				return Redirect::to('/')->with('message','Your Password has changed. Please relogin.');
			}
		}
		else
		{
			return Redirect::to('/')->withErrors('Your session might be expired or No session was found. Try to login.');
		}
	}
}